# This code load the DIC (after outlier removing) for each grain of multigrain experiments and detect voids before computing their characteristics


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
from scipy import ndimage as nd
import math as m
from matplotlib.path import Path
import cv2
import yaml
from PIL import Image

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Particule geometry [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Parameter for observable computing:
### volume discretisation of matrix of inertia [px]:
dxyz=data_input['analysis']['discrete volume']


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Detect void between particles:

## Storage folder:
os.system('mkdir video/void')
os.system('mkdir result/void')

## Loading tracking data:
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Vector to store image size:
vec_im_siz=np.zeros(nb_part)

## Loop over the pictures:
for it_stp in range(nb_img):
    
    ### Display:
    print('step: '+str(it_stp))
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ### Prepare empty matrix and coordinates:
    if it_stp==0:
        dxy=dxyz
        XM=np.max(J_cur0)+diam_lg/1.5
        Ym=-np.max(I_cur0)-diam_lg/1.5
        xv,yv=np.meshgrid(np.linspace(0,XM,int(XM/dxy)),np.linspace(0,-Ym,int(-Ym/dxy)),indexing='xy')
        matter0=np.zeros(xv.shape)
        xv=xv.flatten()
        yv=yv.flatten()
        matter1=matter0.flatten()
    
    matter=matter1.copy()
    
    ### Detect particle matter:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Get image size:
        if it_stp==0:
            pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(it_stp).zfill(4)+'.png'))
            vec_im_siz[it_p]=pict1.shape[0]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        ##### Compute back in Cartesian coordinates:
        Y_edg=I_c-r_cur_edg_smth*np.sin(a_cur_edg)
        X_edg=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        
        #### Detect matter:
        path=Path(np.vstack((X_edg,Y_edg)).T)
        I_in=np.where(path.contains_points(np.vstack((xv,yv)).T)>0.5)[0]
        matter[I_in]=1
    
    ### Get matter matrix:
    matter=np.reshape(matter,matter0.shape)
    
    # !! Debug !!:
    #~ plt.matshow(matter)
    #~ plt.show()
    #~ plt.close()
    
    ### Dilate particles:
    matter=nd.binary_dilation(nd.binary_dilation(matter))
    
    ### Extract connected regions:
    label_mat,num_void=nd.measurements.label(1-matter)
    
    # !! Debug !!:
    #~ plt.matshow(label_mat)
    #~ plt.show()
    #~ plt.close()
    
    ### Initialize father matrix:
    if it_stp==0:
        father_old=np.zeros(label_mat.shape)
    
    father_new=np.zeros(label_mat.shape)
    cmpt_void=1
    
    ### Loop over voids:
    for it_v in range(2,num_void+1):
        I,J=np.where(label_mat==it_v)
        if len(I)>50:
            #### Create void pattern:
            void_cur=matter0.copy(); void_cur[I,J]=255
            #### Add in father matrix:
            father_new[I,J]=cmpt_void
            cmpt_void+=1
            #### Detect contour:
            contours,hierarchy=cv2.findContours(void_cur.astype('uint8'),1,2)
            cnt=contours[0]
            # ~ #!!Debug!!
            # ~ plt.imshow(void_cur)
            # ~ plt.plot(cnt[:,0][:,0],cnt[:,0][:,1],'o-r')
            # ~ plt.show()
            # ~ plt.close()
            #### Compute the area:
            Area=cv2.contourArea(cnt)*dxy*dxy
            #### Compute perimeter:
            Perimeter=cv2.arcLength(cnt,True)*dxy
            #### Compute asphericity:
            Asphericity=Perimeter*Perimeter/(4*m.pi*Area)
            #### Compute the solidity (ratio of contour area to its convex hull area):
            area=cv2.contourArea(cnt)
            hull=cv2.convexHull(cnt)
            hull_area=cv2.contourArea(hull)
            Solidity=float(area)/hull_area
            #### Compute the main orientation:
            if len(cnt)>4:
                (x,y),(MA,ma),Angle=cv2.fitEllipse(cnt)
            else:
                Angle=float('nan')
            
            #### Get father void if any:
            I_g=int(np.mean(I)); J_g=int(np.mean(J)) 
            Father=father_old[I_g,J_g]
            #### Store data:
            if cmpt_void==2:
                data_void=np.array([Father,Area,Perimeter,Asphericity,Solidity,Angle])
            else:
                data_void=np.vstack((data_void,np.array([Father,Area,Perimeter,Asphericity,Solidity,Angle])))
    
    ### Save data if any:
    if cmpt_void>1:
        np.savetxt('result/void/void_stp_'+'%03d'%(it_stp)+'.txt',data_void)
    
    ### Plot void evolution:
    plt.matshow(father_new)
    plt.axis('off')
    plt.savefig('video/void/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()
    
    father_old=father_new

## Make movie:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/void/%03d.png -qscale 1 video/void/void.avi')



