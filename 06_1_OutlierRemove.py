# This code load the DIC and tracking results for each grain of multigrain experiments  and
# - remove outliers
# - detect particle edges


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
from random import randint
import yaml
from scipy.spatial import Delaunay

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Particule diameter [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Parameters for the outlier removing:
### Filter range of interaction: 
filt_rg=data_input['filtering']['interaction range']
### Filter intensity:
filt_int=data_input['filtering']['intensity']
### Maximum relative displacement of cell for rigid particles:
filt_max_disp=data_input['filtering']['maximum displacement rigid']

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Concave Hull parameter:
### Alpha parameter for concave Hull detection of edges [px]:
alpha_hull=data_input['picture handling']['alpha Hull']

## Correlation parameter:
### Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Remove outliers:

## Load the particle position, size and nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Loop over particles:
for it_p in range(nb_part):
    
    ### Display:
    print('Outlier removing - particle: '+'%03d'%it_p)
    
    ### Load data:
    #### Cell positions:
    I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
    J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
    #### Cell displacements:
    mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
    mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
    
    ### Centering:
    I0=I0-np.mean(I0)
    J0=J0-np.mean(J0)
    
    ### Compute total displacement matrix:
    mat_dist=np.sqrt(mat_dI0**2.+mat_dJ0**2.)
    
    ### Initialize outlier position matrix:
    mat_outl=np.zeros(mat_dI0.shape)
    
    ### Loop over the frames:
    for itImg in range(nb_img):
        
        #### Extract current displacements:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        
        #### Compute current position:
        I_pos_cur=I0+dI_cur
        J_pos_cur=J0+dJ_cur
        
        #### Prepare storage matrices:
        vec_del=np.array([]).astype('int')
        
        #### Loop over the correlation cells:
        vec_val=np.zeros(len(I0))
        for it_c in range(len(I0)):
            ##### Extract cells around:
            I_cur=I0[it_c]
            J_cur=J0[it_c]
            d_cur=np.sqrt((I0-I_cur)**2.+(J0-J_cur)**2.)
            II_1=np.where(d_cur<filt_rg*dd_0)[0]
            II_cur=np.where(II_1==it_c)[0][0]
            ##### Compute relative position to the particle center:
            rel_pos=np.sqrt(I_pos_cur[it_c]**2.+J_pos_cur[it_c]**2.)/(IJSN[it_p,2]*diam_lg/2.+float(not(IJSN[it_p,2]))*diam_sm/2.)
            ##### check if the point is not isolated:
            if (len(II_1)<2) and (rel_pos>1.2):
                vec_del=np.append(vec_del,it_c)
            else:
                ##### check if it is an outlier:
                ###### Case of soft particles: 
                dd_cur=np.abs(mat_dist[II_1,itImg]-np.median(mat_dist[II_1,itImg]))
                if IJSN[it_p,3]==0:
                    m_cur=np.median(dd_cur)
                    if (m_cur>0): 
                        # ~ s_cur=dd_cur/m_cur
                        # ~ if m_cur<0.1: ...
                        s_cur=dd_cur
                        vec_val[it_c]=s_cur[II_cur]
                        if s_cur[II_cur]>filt_int:
                            vec_del=np.append(vec_del,it_c)
                
                ###### Case of rigid particles:
                else:
                    II_2=np.where(dd_cur>filt_max_disp)[0]
                    vec_val[it_c]=dd_cur[II_cur]
                    if dd_cur[II_cur]>filt_max_disp:
                        vec_del=np.append(vec_del,it_c)
        
        #!DEBUG!
        # ~ plt.scatter(J_pos_cur,I_pos_cur,c=vec_val,s=50,vmin=0,vmax=40)
        # ~ plt.jet()
        # ~ plt.colorbar()
        # ~ plt.plot(np.delete(J_pos_cur,vec_del),np.delete(I_pos_cur,vec_del),'+g')
        # ~ plt.axis('equal')
        # ~ plt.savefig(str(itImg).zfill(4)+'.png')
        # ~ #plt.show()
        # ~ plt.close()
        
        #### Update outlier matrix:
        if len(vec_del)>0:
            mat_outl[vec_del,itImg]=1
    
    ### Save outlier matrix:
    np.savetxt('result/'+'%03d'%it_p+'/outlier.txt',mat_outl)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Edge detection from deformation:

## Define concave contour detection (concave hull) functions:
def add_edge(edges, i, j):
    if (i, j) in edges or (j, i) in edges:
        assert (j, i) in edges
        edges.remove((j, i))
        return
    edges.add((i, j))

def alpha_shape(points, alpha):
    tri = Delaunay(points)
    edges = set()
    for ia, ib, ic in tri.vertices:
        pa = points[ia]
        pb = points[ib]
        pc = points[ic]
        a = np.sqrt((pa[0] - pb[0]) ** 2 + (pa[1] - pb[1]) ** 2)
        b = np.sqrt((pb[0] - pc[0]) ** 2 + (pb[1] - pc[1]) ** 2)
        c = np.sqrt((pc[0] - pa[0]) ** 2 + (pc[1] - pa[1]) ** 2)
        s = (a + b + c) / 2.0
        area = np.sqrt(s * (s - a) * (s - b) * (s - c))
        circum_r = a * b * c / (4.0 * area)
        if circum_r < alpha:
            add_edge(edges, ia, ib)
            add_edge(edges, ib, ic)
            add_edge(edges, ic, ia)
    return edges


## Loop over particles:
for it_p in range(nb_part):
    
    ### Display:
    print('Edge detection - particle: '+'%03d'%it_p)
    
    ### Load data:
    I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
    J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
    
    ### Cell displacements:
    mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
    mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
    
    ### Outlier matrix:
    mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
    
    ### Make storage folder:
    os.system('mkdir '+'result/'+'%03d'%it_p+'/edge')
    
    ### Loop over the frames:
    for itImg in range(nb_img):
        #### Extract current displacements:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        
        #### Index of valid cells:
        I_val=np.where(mat_outl[:,itImg]==0)[0]
        
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        
        #### Computation of the Hull concave cell:
        ##### Get edges:
        edge=np.array(list(alpha_shape(np.array([I_cur[I_val],J_cur[I_val]]).T,alpha_hull)))[:,0]
        ##### Sort them:
        theta=np.arctan2(I_cur[I_val][edge]-np.mean(I_cur[I_val]),J_cur[I_val][edge]-np.mean(J_cur[I_val]))
        edge=edge[np.argsort(theta)]
        
        #### Change index coordinate:
        edge = I_val[edge]
        
        ## ~DEBUG~ ##
        # ~ plt.plot(I_cur[I_val],J_cur[I_val],'ob')
        # ~ plt.plot(I_cur,J_cur,'.g')
        # ~ plt.plot(I_cur[edge],J_cur[edge],'-r')
        # ~ plt.axis('equal')
        # ~ plt.savefig(str(itImg).zfill(4)+'.png')
        # ~ #plt.show()
        # ~ plt.close()
        
        ### Save data:
        np.savetxt('result/'+'%03d'%it_p+'/edge/'+str(itImg).zfill(4)+'.txt',edge)























