# This code permits to analyse the geometry of the strain field


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import math as m
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Global inputs:
### Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

### Numbering:
#### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
#### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

### Particule diameter [px]:
#### Large:
diam_lg=data_input['general']['particle diameter']['large']
#### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Local inputs:
### Colormap for plotting:
cm=plt.cm.get_cmap('jet')
### Packing fraction binarisation:
t_bin=50


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Von Mises strain average geometry:

## Make storage folders:
os.system('mkdir video')
os.system('mkdir video/averageVM')
os.system('mkdir video/averageVM/small_soft')
os.system('mkdir video/averageVM/large_soft')
os.system('mkdir video/averageVM/small_rigid')
os.system('mkdir video/averageVM/large_rigid')
os.system('mkdir video/VMshape')
os.system('mkdir video/VMshape/small_soft')
os.system('mkdir video/VMshape/large_soft')
os.system('mkdir video/VMshape/small_rigid')
os.system('mkdir video/VMshape/large_rigid')

## Load particle nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Loop over packing fractions:
K_lg_sft=[]
K_sm_sft=[]
K_lg_rgd=[]
K_sm_rgd=[]
flag_lg=True
flag_sm=True
for it_stp in list(reversed(range(nb_img))):
    ### Current steps:
    I_cur=np.array(range(max(0,it_stp-2),min(it_stp+2,nb_img-1)))
    ### Loop over current steps to collect data:
    Q_lg_sft=[]
    Q_sm_sft=[]
    Q_lg_rgd=[]
    Q_sm_rgd=[]
    for it_i in I_cur:
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                vec_e1=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+'%03d'%it_i+'_eigenvalue_1.txt')
                vec_e2=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+'%03d'%it_i+'_eigenvalue_2.txt')
            
            if IJSN[it_p,3]==1:
                vec_e1=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+'%03d'%it_i+'_eigenvalue_1.txt')
                vec_e2=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+'%03d'%it_i+'_eigenvalue_2.txt')
            
            vec_VM=np.sqrt(vec_e1*vec_e1+vec_e2*vec_e2-vec_e1*vec_e2)
            VM_med=np.nanmedian(vec_VM)
            vec_VM[((vec_VM<0.8*VM_med)+(vec_VM>1.2*VM_med))>0.5]=np.float('nan')
            
            if IJSN[it_p,2]==1 and flag_lg:
                flag_lg=False
                grid_I_lg=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_I.txt')
                grid_J_lg=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_J.txt')
                dd_lg=np.nanmax(grid_I_lg)
                grid_r_lg=np.sqrt(grid_I_lg**2.+grid_J_lg**2.)
                r_lg=np.linspace(0,dd_lg,30)
            
            if IJSN[it_p,2]==0 and flag_sm:
                flag_sm=False
                grid_I_sm=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_I.txt')
                grid_J_sm=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_J.txt')
                dd_sm=np.nanmax(grid_I_sm)
                grid_r_sm=np.sqrt(grid_I_sm**2.+grid_J_sm**2.)
                r_sm=np.linspace(0,dd_sm,30)
            
            if IJSN[it_p,2]==1 and IJSN[it_p,3]==0:
                Q_lg_sft.append(vec_VM)
            elif IJSN[it_p,2]==1 and IJSN[it_p,3]==1:
                Q_lg_rgd.append(vec_VM)
            elif IJSN[it_p,2]==0 and IJSN[it_p,3]==0:
                Q_sm_sft.append(vec_VM)
            elif IJSN[it_p,2]==0 and IJSN[it_p,3]==1:
                Q_sm_rgd.append(vec_VM)
    
    ### Correst size
    d_lg_sft=float('inf')
    for it in range(len(Q_lg_sft)):
        if Q_lg_sft[it].shape[0]<d_lg_sft:
            d_lg_sft=Q_lg_sft[it].shape[0]
    
    for it in range(len(Q_lg_sft)):
        Q_lg_sft[it]=Q_lg_sft[it][0:d_lg_sft,0:d_lg_sft]
    
    d_lg_rgd=float('inf')
    for it in range(len(Q_lg_rgd)):
        if Q_lg_rgd[it].shape[0]<d_lg_rgd:
            d_lg_rgd=Q_lg_rgd[it].shape[0]
    
    for it in range(len(Q_lg_rgd)):
        Q_lg_rgd[it]=Q_lg_rgd[it][0:d_lg_rgd,0:d_lg_rgd]
    
    d_sm_sft=float('inf')
    for it in range(len(Q_sm_sft)):
        if Q_sm_sft[it].shape[0]<d_sm_sft:
            d_sm_sft=Q_sm_sft[it].shape[0]
    
    for it in range(len(Q_sm_sft)):
        Q_sm_sft[it]=Q_sm_sft[it][0:d_sm_sft,0:d_sm_sft]
    
    d_sm_rgd=float('inf')
    for it in range(len(Q_sm_rgd)):
        if Q_sm_rgd[it].shape[0]<d_sm_rgd:
            d_sm_rgd=Q_sm_rgd[it].shape[0]
    
    for it in range(len(Q_sm_rgd)):
        Q_sm_rgd[it]=Q_sm_rgd[it][0:d_sm_rgd,0:d_sm_rgd]
    
    ### Compute the average strain
    avg_lg_sft=np.nanmean(np.dstack(Q_lg_sft),axis=2)
    avg_sm_sft=np.nanmean(np.dstack(Q_sm_sft),axis=2)
    avg_lg_rgd=np.nanmean(np.dstack(Q_lg_rgd),axis=2)
    avg_sm_rgd=np.nanmean(np.dstack(Q_sm_rgd),axis=2)
    
    ### Get the position of the maximum strain:
    max_lg_sft=avg_lg_sft>np.nanmean(avg_lg_sft)
    max_sm_sft=avg_sm_sft>np.nanmean(avg_sm_sft)
    max_lg_rgd=avg_lg_rgd>np.nanmean(avg_lg_rgd)
    max_sm_rgd=avg_sm_rgd>np.nanmean(avg_sm_rgd)
    
    if (it_stp==nb_img-1):
        VM_lg_sft_m=np.nanmin(avg_lg_sft)
        VM_lg_sft_M=np.nanmax(avg_lg_sft)
        VM_sm_sft_m=np.nanmin(avg_sm_sft)
        VM_sm_sft_M=np.nanmax(avg_sm_sft)
        VM_lg_rgd_m=np.nanmin(avg_lg_rgd)
        VM_lg_rgd_M=np.nanmax(avg_lg_rgd)
        VM_sm_rgd_m=np.nanmin(avg_sm_rgd)
        VM_sm_rgd_M=np.nanmax(avg_sm_rgd)
    
    ### Plot results:
    plt.imshow(avg_lg_sft,vmin=VM_lg_sft_m,vmax=VM_lg_sft_M,cmap=cm)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/averageVM/large_soft/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    plt.imshow(avg_sm_sft,vmin=VM_sm_sft_m,vmax=VM_sm_sft_M,cmap=cm)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/averageVM/small_soft/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    plt.imshow(avg_lg_rgd,vmin=VM_lg_rgd_m,vmax=VM_lg_rgd_M,cmap=cm)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/averageVM/large_rigid/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    plt.imshow(avg_sm_rgd,vmin=VM_sm_rgd_m,vmax=VM_sm_rgd_M,cmap=cm)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/averageVM/small_rigid/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    
    plt.imshow(max_lg_sft,vmin=0,vmax=1,cmap=plt.cm.binary)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/VMshape/large_soft/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    plt.imshow(max_sm_sft,vmin=0,vmax=1,cmap=plt.cm.binary)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/VMshape/small_soft/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    plt.imshow(max_lg_rgd,vmin=0,vmax=1,cmap=plt.cm.binary)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/VMshape/large_rigid/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    plt.imshow(max_sm_rgd,vmin=0,vmax=1,cmap=plt.cm.binary)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/VMshape/small_rigid/'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()


## Make movies:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/averageVM/large_rigid/%03d.png -qscale 1 video/averageVM/large_rigid/vonMisesAverage_large_rigid.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/averageVM/small_rigid/%03d.png -qscale 1 video/averageVM/small_rigid/vonMisesAverage_small_rigid.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/averageVM/large_soft/%03d.png -qscale 1 video/averageVM/large_soft/vonMisesAverage_large_soft.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/averageVM/small_soft/%03d.png -qscale 1 video/averageVM/small_soft/vonMisesAverage_small_soft.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/VMshape/large_rigid/%03d.png -qscale 1 video/VMshape/large_rigid/vonMisesShape_large_rigid.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/VMshape/small_rigid/%03d.png -qscale 1 video/VMshape/small_rigid/vonMisesShape_small_rigid.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/VMshape/large_soft/%03d.png -qscale 1 video/VMshape/large_soft/vonMisesShape_large_soft.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/VMshape/small_soft/%03d.png -qscale 1 video/VMshape/small_soft/vonMisesShape_small_soft.avi')









