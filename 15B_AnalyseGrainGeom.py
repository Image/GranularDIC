# This code permits to individually analyze grain shape


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import fnmatch
import math as m
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Global inputs:
### Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

### Numbering:
#### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
#### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

### Particule diameter [px]:
#### Large:
diam_lg=data_input['general']['particle diameter']['large']
#### Large:
diam_sm=data_input['general']['particle diameter']['small']

## Local inputs:
### Colormap for plotting:
cm=plt.cm.get_cmap('jet')
### Binarisation number for PDF:
n_bin=10
### Packing fraction binarisation:
t_bin=10


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Function to compute linear pdfs:

def hist_pdf_lin(Q,X):
    P=np.zeros(len(X)-1)
    rP=np.zeros(len(X)-1)
    Q_len=float(len(Q))
    for it in range(len(X)-1):
        I=np.where((X[it]<=Q)*(Q<X[it+1]))[0]
        P[it]=float(len(I))/Q_len/(X[it+1]-X[it])
        rP[it]=np.std(Q[I])/m.sqrt(len(I))/(X[it+1]-X[it])/(X[it+1]-X[it])
    
    X=0.5*(X[0:len(X)-1]+X[1:len(X)])
    return [X,P,rP]


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Gather data in common matrices:

## Initialise storage:
### Perimeter:
mat_peri=np.zeros((nb_part,nb_img))
### Area:
mat_area=np.zeros((nb_part,nb_img))
### Asphericity:
mat_asph=np.zeros((nb_part,nb_img))
### Major direction:
mat_Mdir=np.zeros((nb_part,nb_img))
### Minor direction:
mat_mdir=np.zeros((nb_part,nb_img))
### Anisotropy:
mat_anis=np.zeros((nb_part,nb_img))

## Loops over particles to collect data:
for it_p in range(nb_part):
    mat_peri[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/perimeter.txt')
    mat_area[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/area.txt')
    mat_asph[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/asphericity.txt')
    mat_Mdir[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/major_direction.txt')
    mat_mdir[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/minor_direction.txt')
    mat_anis[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/anisotropy.txt')

## Load particle nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Grain asphericity PDF :

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

## Loop over packing fractions:
if np.min(IJSN[:,3])==0:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            if IJSN[it_i,3]==0:
                lst_Q.append(mat_asph[:,it_i])
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        print(len(vec_Q))
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xlim(1.0,1.2)
    # ~ plt.ylim(0.,300.)
    plt.xlabel('grain asphericity')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFgrainAsphericity_soft.png')
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Grain anisotropy PDF :

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
 
## Loop over packing fractions:
if np.min(IJSN[:,3])==0:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            if IJSN[it_i,3]==0:
                lst_Q.append(mat_anis[:,it_i])
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xlim(0.75,1)
    # ~ plt.ylim(0.,130.)
    plt.xlabel('grain anisotropy')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFgrainAnisotropy_soft.png')
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Grain major direction PDF :

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
 
## Loop over packing fractions:
if np.min(IJSN[:,3])==0:
    fig=plt.figure()
    ax=fig.add_subplot(111, polar=True)
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        lst_P=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            if IJSN[it_i,3]==0:
                lst_Q.append(mat_Mdir[:,it_i])
                lst_P.append(mat_anis[:,it_i])
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        vec_P=np.hstack(lst_P)
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),int(1.5*n_bin)))
        ### Clean PDF:
        I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Convert into degrees:
        X=np.hstack((X,180+X))/180.*m.pi; X=np.append(X,X[0])
        Y=np.hstack((P,P)); Y=(1-np.mean(vec_P))*np.append(Y,Y[0])
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        ax.plot(X,Y,'o-',color=cm(flt_col),linewidth=1,markersize=3)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.savefig('figure/PDFgrainMajorDirection_soft.png')
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Grain minor direction PDF :

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
 
## Loop over packing fractions:
if np.min(IJSN[:,3])==0:
    fig=plt.figure()
    ax=fig.add_subplot(111, polar=True)
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        lst_P=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            if IJSN[it_i,3]==0:
                lst_Q.append(mat_mdir[:,it_i])
                lst_P.append(mat_anis[:,it_i])
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        vec_P=np.hstack(lst_P)
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),int(1.5*n_bin)))
        ### Clean PDF:
        I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Convert into degrees:
        X=np.hstack((X,180+X))/180.*m.pi; X=np.append(X,X[0])
        Y=np.hstack((P,P)); Y=(1-np.mean(vec_P))*np.append(Y,Y[0])
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        ax.plot(X,Y,'o-',color=cm(flt_col),linewidth=1,markersize=3)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.savefig('figure/PDFgrainMinorDirection_soft.png')
    plt.show()
    plt.close()









