# This code permits to write the YAML file with post-processing inputs


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Libraries:

##Commons ones:
import os
import yaml
import io
import math as m

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
## Parameters you can modify:
### Number of picture:
nb_pict=76
### Number of processor for parallelization:
nb_proc=6
### Particule diameter [px]:
#### Large:
diam_lg=2810
#### Small:
diam_sm=1860
### particle height:
height_part=1417

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
##Parameters for particule tracking:
### Correlation width for particle tracking from largest to smallest [px]:
#### Large:
cc_lg_M=int(diam_lg/(2*m.sqrt(2)))
cc_lg_m=200
#### Small:
cc_sm_M=int(diam_sm/(2*m.sqrt(2)))
cc_sm_m=150
#### Number of steps from large to small:
n_cc=2
#### Correlation goodness acceptability [1/8bit]:
corr_accpt=17.

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
##Parameters for image correlation:
### Maximum number of pixel per cells in line and row for soft particles [px]:  
cc_0_M_sft=200
### Minimum number of pixel per cells in line and row for soft particles [px]:  
cc_0_m_sft=40
### Number of cell size subdivision for soft particles :
nc_sft=3
### Maximum number of pixel per cells in line and row for rigid particles [px]:  
cc_0_M_rgd=80
### Minimum number of pixel per cells in line and row for rigid particles [px]:  
cc_0_m_rgd=80
### Number of cell size subdivision for rigid particles :
nc_rgd=1
### Maximum shift optimization (for optimization algorithm) [px<<cc_0_m]:
m_sht=4
### Space between cell centers in line and row [px]:
dd_0=60
### Image reduction factor for display: 
ImRat=0.30

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
## Parameters for the outlier removing:
### Filter range of interaction: 
filt_rg=5.
### Filter intensity:
filt_int=30.
### Maximum relative displacement of cell for rigid particles:
filt_max_disp=15.

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
## Parameters for picture handling
### Folder with pictures:
name_dir_im=os.path.join(os.getcwd(),'picture')
### Picture extension:
name_ext_im='.png'
### Image reduction factor for making small illustrative pictures:
ImRat_sml=0.03
###Image reduction for selection of the particle nature:
ImRat_med=0.20 
### Smothening filter size (for particle detection) [px]:
size_smh_filt=100
### Black border on the edges:
pp=400
### Alpha parameter for concave Hull detection of edges [px]:
alpha_hull=5*dd_0

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
## Parameter for results displaying:
### Grain scale displaying of results (0: none, 1: reduced, 2: full):
local_disp=1
### Grain scale displaying nature (I1|I2|eigen1_val|eigen2_val|eigen1_vec|eigen2_vec|VM, of C (soft) or E (rigid)):
local_nat_I1=1
local_nat_I2=1
local_nat_Eval1=1
local_nat_Eval2=1
local_nat_Evec1=1
local_nat_Evec2=1
local_nat_VM=1
### Displaying accuracy for global movies:
disp_acc_mov=1.5 
### Displaying accuracy for fancy pictures:
disp_acc_fancy=0.5 

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
## Parameter for observable computing:
### volume discretisation of matrix of inertia [px]:
dxyz=10

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
## Parameter for contact detection:
## Proximity criterion for potential contact [px]:
ctt_th=150
## Number of closest cells to consider for VM field measurement to test contact:
ctt_meas_th=10
## Distance threshold value for contact detection of rigid particles [px]:
ctt_d_th=75
## VM threshold value for contact detection of soft particles:
ctt_VM_th=0.015


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Save post-processing parameter in a YAML file:

## Make the dictionnary: 
data_input={
'general':           { 'number of picture':            nb_pict ,
                       'particle diameter':            { 'small': diam_sm ,
                                                         'large': diam_lg } ,
                       'number of processor':          nb_proc ,
                       'particle height':              height_part },
'picture handling':  { 'folder':                       name_dir_im ,
                       'extension':                    name_ext_im ,
                       'smoothening':                  size_smh_filt ,
                       'image border':                 pp ,
                       'small reduction ratio':        ImRat_sml,
                       'medium reduction ratio':       ImRat_med,
                       'alpha Hull':                   alpha_hull },
'particle tracking': { 'correlation cell size' :       { 'small': [cc_sm_m, cc_sm_M] ,
                                                         'large': [cc_lg_m, cc_lg_M] } ,
                       'number of step':               n_cc ,
                       'correlation acceptability':    corr_accpt } ,
'granular DIC':      { 'correlation cell size' :       { 'soft':  [cc_0_M_sft, cc_0_m_sft] ,
                                                         'rigid': [cc_0_M_rgd, cc_0_m_rgd] } ,
                       'number of step':               { 'soft':  nc_sft ,
                                                         'rigid': nc_rgd } ,
                       'inter-cell space':             dd_0 ,
                       'maximum shift':                m_sht ,
                       'image reduction':              ImRat } ,
'filtering':         { 'interaction range':            filt_rg ,
                       'intensity':                    filt_int ,
                       'maximum displacement rigid':   filt_max_disp } ,
'displaying':        { 'field accuracy thin':          disp_acc_fancy ,
                       'field accuracy coarse':        disp_acc_mov ,
                       'local displaying':             local_disp ,
                       'local field':                  { 'invariant 1':   local_nat_I1 ,
                                                         'invariant 2':   local_nat_I2 ,
                                                         'eigenvalue 1':  local_nat_Eval1 ,
                                                         'eigenvalue 2':  local_nat_Eval2 ,
                                                         'eigenvector 1': local_nat_Evec1 ,
                                                         'eigenvector 2': local_nat_Evec2 ,
                                                         'von Mises':     local_nat_VM }} ,
'analysis':          { 'discrete volume':              dxyz } ,
'contact':           { 'proximity threshold consider': ctt_th ,
                       'number close cells':           ctt_meas_th ,
                       'proximity threshold rigid':    ctt_d_th ,
                       'von Mises threshold':          ctt_VM_th}
}


## Save it in a YAML file:
with io.open('input_parameters.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data_input, outfile, default_flow_style=False, allow_unicode=True)





