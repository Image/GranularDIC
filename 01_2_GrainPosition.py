# This code permits to detect grains on the first picture
# Grains are detected automatically and then manually for a better positioning


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Libraries:

##Commons ones:
import os
import numpy as np
import math as m
from PIL import Image
from PIL import ImageOps
Image.MAX_IMAGE_PIXELS = 933120000
import cv2
from scipy import signal as sg
import yaml
import io
import sys
from matplotlib import pyplot as plt

##Display ones:
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QLabel, QSizePolicy, QSlider, QSpacerItem, QVBoxLayout, QWidget, QDialog, QPushButton, QGroupBox
import pyqtgraph as pg

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Particule diameter [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Parameters for picture handling
### Folder with pictures:
name_dir_im=data_input['picture handling']['folder']
### Picture extension:
name_ext_im=data_input['picture handling']['extension']
### Image reduction factor for making small illustrative pictures:
ImRat_sml=data_input['picture handling']['small reduction ratio']
### Black border on the edges:
pp=data_input['picture handling']['image border']
### Smothening filter size (for particle detection) [px]:
size_smh_filt=data_input['picture handling']['smoothening']

## Not stored input parameters:
### Threshold for particle detection [0,1]:
th_detect = 0.98

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Define classes for particle detection:

class Main(QWidget):
    
    def __init__(self):
        super(Main, self).__init__()
        self.initUI()
    
    def initUI(self):
        # create the initial window and show it
        self.win_1 = Widget1(self)
        self.win_1.show()

## Class to make a tuning parameter slider
class Slider(QWidget):
    def __init__(self, minimum, maximum, default, parent=None):
        super(Slider, self).__init__(parent=parent)
        
        self.horizontalLayout = QHBoxLayout(self)
        self.label = QLabel(self)
        self.horizontalLayout.addWidget(self.label)
        self.verticalLayout = QVBoxLayout()
        spacerItem = QSpacerItem(0, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem)
        self.slider = QSlider(self)
        self.slider.setOrientation(Qt.Horizontal)
        self.slider.setSliderPosition(default)
        self.horizontalLayout.addWidget(self.slider)
        spacerItem1 = QSpacerItem(0, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem1)

        self.minimum = minimum
        self.maximum = maximum
        self.slider.valueChanged.connect(self.setLabelValue)
        self.x = None
        self.setLabelValue(self.slider.value())

    def setLabelValue(self, value):
        self.x = self.minimum + (float(value) / (self.slider.maximum() - self.slider.minimum())) * (self.maximum - self.minimum)
        self.label.setText("{:3.2f}".format(self.x))

## Class to select the image threshold
class Widget1(QDialog):
    def __init__(self, parent=None):
        super(Widget1, self).__init__(parent=parent)
        
        self.initUI()
    
    def initUI(self):
        
        ### Load the initial image
        image_0 = Image.open(os.path.join(name_dir_im,'%04d'%0+name_ext_im))
        image_0 = ImageOps.expand(image_0,border=pp,fill='black')
        ### Resize picture
        size_img_0 = image_0.size
        image_0.thumbnail((int(size_img_0[0]*ImRat_sml),int(size_img_0[1]*ImRat_sml)),Image.ANTIALIAS)
        image_0 = np.array(image_0).astype('float')
        
        ## Smoothen the reduced picture
        ### Make the smoothening filter
        size_smh_filt_r = int(size_smh_filt*ImRat_sml)
        filt_0 = np.zeros((size_smh_filt_r,size_smh_filt_r))
        for it_i in range(size_smh_filt_r):
            for it_j in range(size_smh_filt_r):
                if m.sqrt( (it_i-size_smh_filt_r/2)**2.+(it_j-size_smh_filt_r/2)**2.)<size_smh_filt_r/2:
                    filt_0[it_i,it_j] = 1.
        
        filt_0 = filt_0.astype('float')/float(np.sum(filt_0))
        ### Apply filter:
        self.image_0_smh = sg.convolve2d(image_0,filt_0,mode='same')
        
        ### Make the window
        #### Add title
        self.setWindowTitle('Image threshold selection')
        #### Maximise window
        self.showMaximized()
        #### Add the slider 
        self.verticalLayout = QVBoxLayout(self)
        self.slid_1 = Slider(0, 20, 15/255*100)
        self.verticalLayout.addWidget(self.slid_1)
        #### Add the image
        self.disp_img = pg.ImageView()
        self.verticalLayout.addWidget(self.disp_img)
        #### Add validation button
        self.b0 = QPushButton('OK',self)
        self.b0.clicked.connect(self.function_button)
        self.verticalLayout.addWidget(self.b0)
        
        ### Display initial image
        self.slid_1.slider.valueChanged.connect(self.update_thrd)
        self.update_thrd()
    
    ### Function to update the thresold
    def update_thrd(self):
        self.thr_pict = self.slid_1.x
        image_0_th = (self.image_0_smh>self.thr_pict).astype('uint8')
        self.disp_img.setImage(image_0_th)
    
    ### Function to save thresold when closing and open the next window
    def function_button(self, event):
        #### Save image threshold in YAML file
        data_input['picture handling'].update({'image threshold':self.thr_pict})
        with io.open('input_parameters.yaml', 'w', encoding='utf8') as outfile:
            yaml.dump(data_input, outfile, default_flow_style=False, allow_unicode=True)
        #### Hide current window
        self.hide()
        ### Make the following window
        self.win_2 = Widget2(self)
        #### Open next window
        self.win_2.show()
        

## Class to roughly detect the position of particles
class Widget2(QDialog):
    def __init__(self, parent=None):
        super(Widget2, self).__init__(parent=parent)
        
        self.initUI()
    
    def initUI(self):
        
        ### Get thresholded image
        self.image_0 = Image.open(os.path.join(name_dir_im,'%04d'%0+name_ext_im))
        self.image_0 = ImageOps.expand(self.image_0,border=pp,fill='black')
        size_img_0 = self.image_0.size
        self.image_0.thumbnail((int(size_img_0[0]*ImRat_sml),int(size_img_0[1]*ImRat_sml)),Image.ANTIALIAS)
        self.image_0 = np.array(self.image_0).astype('float')
        size_smh_filt_r = int(size_smh_filt*ImRat_sml)
        filt_0 = np.zeros((size_smh_filt_r,size_smh_filt_r))
        for it_i in range(size_smh_filt_r):
            for it_j in range(size_smh_filt_r):
                if m.sqrt( (it_i-size_smh_filt_r/2)**2.+(it_j-size_smh_filt_r/2)**2.)<size_smh_filt_r/2:
                    filt_0[it_i,it_j] = 1.
        filt_0 = filt_0.astype('float')/float(np.sum(filt_0))
        image_0_smh = sg.convolve2d(self.image_0,filt_0,mode='same')
        with open('input_parameters.yaml', 'r') as stream:
            data_input=yaml.safe_load(stream)
        image_0_th = (image_0_smh>data_input['picture handling']['image threshold']).astype('uint8')
        
        ### Look for large particles:
        #### Make pattern particle for large particles detection
        diam_lg_r = int(diam_lg*ImRat_sml*1.01)
        filt_lg = np.zeros((diam_lg_r,diam_lg_r))
        for it_i in range(diam_lg_r):
            for it_j in range(diam_lg_r):
                if m.sqrt((it_i-diam_lg_r/2)**2.+(it_j-diam_lg_r/2)**2.)<diam_lg_r/2:
                    filt_lg[it_i,it_j]=diam_lg_r/2-m.sqrt((it_i-diam_lg_r/2)**2.+(it_j-diam_lg_r/2)**2.)
        filt_lg=filt_lg.astype('float')/float(np.sum(filt_lg))
        #### Convolution with the large particle filter and threshold:
        image_0_lg=sg.convolve2d(image_0_th,filt_lg,mode='same')
        image_0_lg_th=image_0_lg>th_detect
        #### Look for connected areas:
        output_bwl=cv2.connectedComponentsWithStats(np.uint8(image_0_lg_th),4) 
        lg_IJ_0=output_bwl[3][1:-1,:]
        lg_IJ_0=lg_IJ_0.astype('uint')
        #### Improvement of the detection of large particles:
        lg_IJ=np.zeros((len(lg_IJ_0),2))
        nn=int(diam_lg_r/8)
        for it in range(len(lg_IJ_0)):
            tmp_img=image_0_lg.copy()
            tmp_img[0:int(lg_IJ_0[it,1]-nn),:]=0
            tmp_img[int(lg_IJ_0[it,1]+nn):-1,:]=0
            tmp_img[:,0:int(lg_IJ_0[it,0]-nn)]=0
            tmp_img[:,int(lg_IJ_0[it,0]+nn):-1]=0
            I,J=np.where(tmp_img==np.max(tmp_img))
            lg_IJ[it,0]=I[0]
            lg_IJ[it,1]=J[0]
        
        ### Look for small particles:
        #### Remove large particles:
        image_0_th_crp=image_0_th.copy()
        nn=int(diam_lg_r/2/m.sqrt(2))
        for it in range(len(lg_IJ)):
            image_0_th_crp[int(lg_IJ[it,0]-nn):int(lg_IJ[it,0]+nn),int(lg_IJ[it,1]-nn):int(lg_IJ[it,1]+nn)]=0
        #### Make particle for small particles detection
        diam_sm_r=int(diam_sm*ImRat_sml*1.01)
        filt_sm=np.zeros((diam_sm_r,diam_sm_r))
        for it_i in range(diam_sm_r):
            for it_j in range(diam_sm_r):
                if m.sqrt((it_i-diam_sm_r/2)**2.+(it_j-diam_sm_r/2)**2.)<diam_sm_r/2:
                    filt_sm[it_i,it_j]=diam_sm_r/2-m.sqrt((it_i-diam_sm_r/2)**2.+(it_j-diam_sm_r/2)**2.)
        filt_sm=filt_sm.astype('float')/float(np.sum(filt_sm))
        #### Convolution with the small particle filter and threshold:
        image_0_sm=sg.convolve2d(image_0_th_crp,filt_sm,mode='same')
        image_0_sm_th=image_0_sm>th_detect
        #### Look for connected areas:
        output_bwl=cv2.connectedComponentsWithStats(np.uint8(image_0_sm_th),4) 
        sm_IJ_0=output_bwl[3][1:-1,:]
        sm_IJ_0=sm_IJ_0.astype('uint')
        ### Improvement of the detection of large particles:
        sm_IJ=np.zeros((len(sm_IJ_0),2))
        nn=int(diam_sm_r/8)
        for it in range(len(sm_IJ_0)):
            tmp_img=image_0_sm.copy()
            tmp_img[0:int(sm_IJ_0[it,1]-nn),:]=0
            tmp_img[int(sm_IJ_0[it,1]+nn):-1,:]=0
            tmp_img[:,0:int(sm_IJ_0[it,0]-nn)]=0
            tmp_img[:,int(sm_IJ_0[it,0]+nn):-1]=0
            I,J=np.where(tmp_img==np.max(tmp_img))
            sm_IJ[it,0]=I[0]
            sm_IJ[it,1]=J[0]
        
        
        ### Make the window
        #### Add title
        self.setWindowTitle('Particle selection')
        #### Maximise window
        self.showMaximized()
        #### Make button to add a large particle
        self.b_large = QPushButton('Large Particle',self)
        self.b_large.clicked.connect(self.add_large)
        #### Make button to add a small particle
        self.b_small = QPushButton('Small Particle',self)
        self.b_small.clicked.connect(self.add_small)
        #### Make button to finish
        self.b_OK = QPushButton('OK',self)
        self.b_OK.clicked.connect(self.save_and_next)
        #### Make explanations
        comment = QLabel(self)
        msg = 'Reposition detected particles if necessary\n'
        msg+= 'by draging them.\n'
        msg+= 'Add new small or large particles.\n'
        msg+= 'Right clic on them to remove.'
        comment.setText(msg)
        #### Make image view box
        roiLayout = pg.GraphicsLayoutWidget(self)
        self.viewBoxROI = roiLayout.addViewBox(lockAspect=True, invertY=True)
        self.pg_img     = pg.ImageItem(self.image_0)
        self.viewBoxROI.addItem( self.pg_img )
        self.viewBoxROI.disableAutoRange('xy')
        self.viewBoxROI.autoRange()
        #### Lay widgets
        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.addWidget(comment)
        self.verticalLayout.addWidget(self.b_large)
        self.verticalLayout.addWidget(self.b_small)
        self.verticalLayout.addWidget(self.b_OK)
        groupBox = QGroupBox()
        groupBox.setLayout(self.verticalLayout)
        self.horizontalLayout = QHBoxLayout(self)
        self.horizontalLayout.addWidget(roiLayout)
        self.horizontalLayout.addWidget(groupBox)
        
        
        ### Add aready detected particles
        #### Make storage variable
        self.list_roi = []
        #### Loop over already detected particles
        ##### Large particles
        for it_lrg in range(lg_IJ.shape[0]):
            self.list_roi.append(pg.CircleROI([lg_IJ[it_lrg,0]-diam_lg*ImRat_sml/2.,lg_IJ[it_lrg,1]-diam_lg*ImRat_sml/2.], [diam_lg*ImRat_sml, diam_lg*ImRat_sml], pen=(4,9), removable=True))
            self.viewBoxROI.addItem(self.list_roi[-1])
            self.list_roi[-1].sigRemoveRequested.connect(self.remove_particle)
        ##### small particles
        for it_sml in range(sm_IJ.shape[0]):
            self.list_roi.append(pg.CircleROI([sm_IJ[it_sml,0]-diam_sm*ImRat_sml/2.,sm_IJ[it_sml,1]-diam_sm*ImRat_sml/2.], [diam_sm*ImRat_sml, diam_sm*ImRat_sml], pen=(1,9), removable=True))
            self.viewBoxROI.addItem(self.list_roi[-1])
            self.list_roi[-1].sigRemoveRequested.connect(self.remove_particle)
    
    ### Function to add a large particle
    def add_large(self):
        #### Add particle in the list
        self.list_roi.append(pg.CircleROI([0.5*diam_lg*ImRat_sml, 0.5*diam_lg*ImRat_sml], [diam_lg*ImRat_sml, diam_lg*ImRat_sml], pen=(4,9), removable=True))
        #### Display new roi
        self.viewBoxROI.addItem(self.list_roi[-1])
        #### Make it removable
        self.list_roi[-1].sigRemoveRequested.connect(self.remove_particle)
    
    ### Function to add a small particle
    def add_small(self):
        #### Add particle in the list
        self.list_roi.append(pg.CircleROI([1.5*diam_sm*ImRat_sml, 1.5*diam_sm*ImRat_sml], [diam_sm*ImRat_sml, diam_sm*ImRat_sml], pen=(1,9), removable=True))
        #### Display new roi
        self.viewBoxROI.addItem(self.list_roi[-1])
        #### Make it removable
        self.list_roi[-1].sigRemoveRequested.connect(self.remove_particle)
    
    ### Function to remove a particle
    def remove_particle(self,roi):
        self.list_roi.remove(roi)
        self.viewBoxROI.removeItem(roi)
    
    ### Save get positions and open the next windows for accurate positionning
    def save_and_next(self):
        
        #### Get ROI positions
        part_pos = np.zeros((len(self.list_roi),3))
        for it_p in range(len(self.list_roi)):
            part_pos[it_p,0:2] = np.array(self.list_roi[it_p].pos())
            if 0.95*diam_lg*ImRat_sml<self.list_roi[it_p].getArrayRegion(self.image_0, self.pg_img).shape[0]<1.05*diam_lg*ImRat_sml:
                part_pos[it_p,2] = 1
                part_pos[it_p,0] += diam_lg*ImRat_sml/2.
                part_pos[it_p,1] += diam_lg*ImRat_sml/2.
            else:
                part_pos[it_p,0] += diam_sm*ImRat_sml/2.
                part_pos[it_p,1] += diam_sm*ImRat_sml/2.
        
        #### Scale positions
        part_pos[:,0:2] = part_pos[:,0:2]/ImRat_sml
        
        #### Hide current window
        self.hide()
        
        #### Make the following windowto select particle accurately 
        ##### Create the window
        win_3 = Widget3(parent=self,arg=part_pos)
        ##### Open it
        win_3.show()


## Class to accurately determine the position of particles
class Widget3(QDialog):
    def __init__(self, parent=None, arg=None):
        super(Widget3, self).__init__(parent=parent)
        
        self.part_pos = arg
        
        self.initUI()
    
    def initUI(self):
        
        ### Load full undeformed picture:
        self.image_0 = Image.open(os.path.join(name_dir_im,'%04d'%0+name_ext_im))
        self.image_0 = ImageOps.expand(self.image_0,border=pp,fill='black')
        self.image_0 = np.array(self.image_0).astype('float')
        
        ### Define area of observation:
        self.ray_lg_L = int(diam_lg*1.1/2.)
        self.ray_sm_L = int(diam_sm*1.1/2.)
        
        ### Initialize particle pointer:
        self.it_p = 0
        
        ### Make the window
        #### Add title
        self.setWindowTitle('Image threshold selection')
        #### Maximise window
        self.showMaximized()
        #### Make button to validate current particle
        self.b_Next = QPushButton('Next',self)
        self.b_Next.setEnabled(True)
        self.b_Next.clicked.connect(self.next_particle)
        #### Make explanations
        comment = QLabel(self)
        msg = 'Reposition exactly the particle\n'
        msg+= 'by draging it.\n'
        msg+= 'Click Next to validate.'
        comment.setText(msg)
        #### Make image view box
        roiLayout = pg.GraphicsLayoutWidget(self)
        self.viewBoxROI = roiLayout.addViewBox(lockAspect=True, invertY=True)
        
        #### Lay widgets
        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.addWidget(comment)
        self.verticalLayout.addWidget(self.b_Next)
        groupBox = QGroupBox()
        groupBox.setLayout(self.verticalLayout)
        self.horizontalLayout = QHBoxLayout(self)
        self.horizontalLayout.addWidget(roiLayout)
        self.horizontalLayout.addWidget(groupBox)
        
        self.next_particle()
    
    ### Function to display particle
    def next_particle(self):
        
        #### Get the accurate position
        if self.it_p>0:
            A = np.array(self.Roi_0.pos())
            if self.part_pos[self.it_p-1,2] == 1:
                A[0] = A[0]-(self.roi_0.shape[0]/2-diam_lg/2.)
                A[1] = A[1]-(self.roi_0.shape[1]/2-diam_lg/2.)
                self.part_pos[self.it_p-1,0:2] = self.part_pos[self.it_p-1,0:2]+A
            else:
                A[0] = A[0]-(self.roi_0.shape[0]/2-diam_sm/2.)
                A[1] = A[1]-(self.roi_0.shape[0]/2-diam_sm/2.)
                self.part_pos[self.it_p-1,0:2] = self.part_pos[self.it_p-1,0:2]+A
            print(A)
            #### Remove previous ROI
            self.viewBoxROI.removeItem(self.pg_img)
            self.viewBoxROI.removeItem(self.Roi_0)
        
        #### Load current position and size
        I_cur = self.part_pos[self.it_p,0]
        J_cur = self.part_pos[self.it_p,1]
        S_cur = self.part_pos[self.it_p,2]
        
        #### Extraction of the ROI
        if S_cur==1:
            ray_L = self.ray_lg_L
        else:
            ray_L = self.ray_sm_L
        I_roi_m = int(I_cur-ray_L)
        I_roi_M = int(I_cur+ray_L)
        J_roi_m = int(J_cur-ray_L)
        J_roi_M = int(J_cur+ray_L)
        self.roi_0 = self.image_0[I_roi_m:I_roi_M,J_roi_m:J_roi_M]
        
        #### Display image
        self.pg_img = pg.ImageItem(self.roi_0)
        self.viewBoxROI.addItem(self.pg_img)
        
        #### Display ROI
        if S_cur == 1:
            self.Roi_0 = pg.CircleROI([self.roi_0.shape[0]/2-diam_lg/2.,self.roi_0.shape[1]/2-diam_lg/2.], [diam_lg, diam_lg], pen=(4,9), removable=True)
        else:
            self.Roi_0 = pg.CircleROI([self.roi_0.shape[0]/2-diam_sm/2.,self.roi_0.shape[1]/2-diam_sm/2.], [diam_sm, diam_sm], pen=(1,9), removable=True)

        self.viewBoxROI.addItem(self.Roi_0)
        self.viewBoxROI.disableAutoRange('xy')
        self.viewBoxROI.autoRange()
        
        #### Increment porticle pointer
        self.it_p += 1
        
        print((self.part_pos.shape[0]-1))
        print(self.it_p)
        
        #### Test if last particle is reached
        if self.it_p == (self.part_pos.shape[0]):
            #### Disable button
            self.b_Next.setEnabled(False)
            
            #### Get the accurate position
            if self.it_p>0:
                A = np.array(self.Roi_0.pos())
                if self.part_pos[self.it_p-1,2] == 1:
                    A[0] = A[0]-(self.roi_0.shape[0]/2-diam_lg/2.)
                    A[1] = A[1]-(self.roi_0.shape[1]/2-diam_lg/2.)
                    self.part_pos[self.it_p-1,0:2] = self.part_pos[self.it_p-1,0:2]+A
                else:
                    A[0] = A[0]-(self.roi_0.shape[0]/2-diam_sm/2.)
                    A[1] = A[1]-(self.roi_0.shape[0]/2-diam_sm/2.)
                    self.part_pos[self.it_p-1,0:2] = self.part_pos[self.it_p-1,0:2]+A
            #### Remove previous ROI
            self.viewBoxROI.removeItem(self.pg_img)
            self.viewBoxROI.removeItem(self.Roi_0)
            ##### Save result
            os.system('mkdir '+name_dir_im+'/../'+'initialParticlePosition')
            np.savetxt(name_dir_im+'/../'+'initialParticlePosition/IJS_position.txt',self.part_pos,fmt='%d')
            
            ##### Plot result
            ###### Load and resize initial picture:
            ####### Load picture:
            image_0=Image.open(os.path.join(name_dir_im,'%04d'%0+name_ext_im))
            image_0=ImageOps.expand(image_0,border=pp,fill='black')
            ####### Resize picture:
            size_img_0=image_0.size
            image_0.thumbnail((int(size_img_0[0]*ImRat_sml),int(size_img_0[1]*ImRat_sml)),Image.ANTIALIAS)
            image_0 = np.array(image_0).astype('float')
            
            ###### Initial picture and grain number:
            ####### Plot picture:
            plt.imshow(image_0,cmap=plt.cm.Greys)
            plt.title('Grain positions & numbers')
            
            ####### Plot large grains:
            
            
            theta = np.linspace(0, 2*np.pi, 50)
            
            
            I=np.where(self.part_pos[:,2]==1)[0]
            for it in range(len(I)):
                I_cur=self.part_pos[I[it],0]*ImRat_sml
                J_cur=self.part_pos[I[it],1]*ImRat_sml
                plt.text(J_cur,I_cur,'%03d'%I[it],horizontalalignment='center',verticalalignment='center',fontsize=12, color='blue',weight='bold')
                r = diam_lg*ImRat_sml/2.
                x1 = r*np.cos(theta)+J_cur
                x2 = r*np.sin(theta)+I_cur
                plt.plot(x1,x2,'-b',linewidth=0.5)
            
            ####### Plot small grains:
            I=np.where(self.part_pos[:,2]==0)[0]
            for it in range(len(I)):
                I_cur=self.part_pos[I[it],0]*ImRat_sml
                J_cur=self.part_pos[I[it],1]*ImRat_sml
                plt.text(J_cur,I_cur,'%03d'%I[it],horizontalalignment='center',verticalalignment='center',fontsize=8, color='red',weight='bold')
                r = diam_sm*ImRat_sml/2.
                x1 = r*np.cos(theta)+J_cur
                x2 = r*np.sin(theta)+I_cur
                plt.plot(x1,x2,'-r',linewidth=0.5)
            
            plt.axis('off')
            
            plt.savefig(name_dir_im+'/../'+'initialParticlePosition/name_position.png',dpi=800)
            plt.close() 
            
            ##### Close window
            self.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Run particle detection:

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    win_0 = Main()
    
    if( app ):
        app.exec_()












