# This code compute all the global variables among which is the force for each step as well as the packing fraction


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import math as m
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Particle geometry [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Particle geometry [px]:
### Height:
height_part=data_input['general']['particle height']

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute force for each step from the force signal:

## Storage folder:
os.system('mkdir result/global')

## Load data:
## Load the force and corresponding times: 
f0=np.loadtxt('forceValue.txt')
t0=np.loadtxt('forceTime.txt')/60.
MM=min(len(t0),len(f0))
f0=f0[0:MM]
t0=t0[0:MM]

## Load the imaging times:
tImg=np.loadtxt('motionTime.txt')/60.

## Loop over steps:
f1=np.zeros(len(tImg)+1)
t1=np.zeros(len(tImg)+1)
errf1=np.zeros(len(tImg)+1)
for itStp in range(len(tImg)+1):
    if itStp==0:
        tm=0
        tM=tImg[itStp]
    elif itStp==len(tImg):
        tm=tImg[itStp-1]
        tM=np.max(t0)
    else:
        tm=tImg[itStp-1]
        tM=tImg[itStp]
    
    I=np.where((tm<t0)*(t0<tM))[0]
    if len(I)>0:
        t00=t0[I]
        f00=f0[I]
        
        I=np.array(range(int(len(t00)*0.3),int(len(t00)*0.9)))
        
        # !Debug!:
        #~ plt.plot(t00,f00,'-k')
        #~ plt.plot(t00[I],f00[I],'-b')
        #~ plt.show()
        #~ plt.close()
        
        f1[itStp]=np.mean(f00[I])
        t1[itStp]=np.mean(t00[I])
        errf1[itStp]=np.std(f00[I])/len(I)

## Remove force offset:
f0=f0-f1[0]
f1=f1-f1[0]

## Remove outliers:
f1=f1[0:nb_img]
errf1=errf1[0:nb_img]
t1=t1[0:nb_img]

## Save Data:
np.savetxt('result/global/force.txt',f1)
np.savetxt('result/global/error_force.txt',errf1)

## Graphical output:
plt.plot(t0,f0,'-k')
plt.plot(t1,f1,'ro-')
plt.ylim(0,np.max(f0))
plt.xlim(0,np.max(t0))
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the displacement for each step:

## Load data:
disp_stp_mm=float(np.loadtxt('input/DisplacementPerStep-mm'))
reso_scan=float(np.loadtxt('input/scannerResolution-DPI'))

## Convert in pixel:
disp_stp_px=disp_stp_mm/25.4*reso_scan

## Make vector:
c0=np.array(range(0,nb_img))*disp_stp_px

## Save Data:
np.savetxt('result/global/displacement.txt',c0)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the area, frame length and strain for each step:

## Load edge position:
vec_tl=np.loadtxt('result/box_edge/IJ_top_left.txt')
vec_tr=np.loadtxt('result/box_edge/IJ_top_right.txt')
vec_br=np.loadtxt('result/box_edge/IJ_bottom_right.txt')
vec_bl=np.loadtxt('result/box_edge/IJ_bottom_left.txt')
y_tl=vec_tl[:,0]; x_tl=vec_tl[:,1]
y_tr=vec_tr[:,0]; x_tr=vec_tr[:,1]
y_bl=vec_bl[:,0]; x_bl=vec_bl[:,1]
y_br=vec_br[:,0]; x_br=vec_br[:,1]

# ~ # !Debug!
# ~ plt.plot(x_tl,y_tl,'r.-')
# ~ plt.plot(x_tr,y_tr,'g.-')
# ~ plt.plot(x_br,y_br,'b.-')
# ~ plt.plot(x_bl,y_bl,'y.-')
# ~ plt.axis('equal')
# ~ plt.show()
# ~ plt.close()

## Compute the global strain evolution:
c0=np.loadtxt('result/global/displacement.txt')
eps0=c0/(0.5*(y_br+y_bl)-0.5*(y_tr+y_tl))

## Compute the frame length evolution:
l0=(0.5*(y_br+y_bl)-0.5*(y_tr+y_tl))-c0

## Compute area evolution
### Function to compute area:
def PolyArea(x,y):
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

### Loop over compression states:
a0=np.zeros(len(c0))
for it0 in range(len(c0)):
    #### Compute and store area:
    a0[it0]=PolyArea(np.array([x_tl[it0],x_tr[it0],x_br[it0],x_bl[it0]]),np.array([y_tl[it0],y_tr[it0],y_br[it0],y_bl[it0]]))

## Save Data:
np.savetxt('result/global/global_strain.txt',eps0)
np.savetxt('result/global/frame_length.txt',l0)
np.savetxt('result/global/frame_area.txt',a0)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the packing fraction for each step:

## Load the particle areas:
vec_area=np.zeros(len(c0))
for it_p in range(nb_part):
    vec_area_tmp=np.loadtxt('result/'+str(it_p).zfill(3)+'/area.txt')
    vec_area[0:len(vec_area_tmp)]+=vec_area_tmp

## Packing fraction (with initial measured particle area):
### Particle area:
a_part=vec_area[0]
### Ratio:
a0=np.loadtxt('result/global/frame_area.txt')
phi0=a_part/a0

## Packing fraction (with initial deduced particle area):
### Particle area:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')
a_part_b=len(np.where(IJSN[:,2]==0)[0])*(m.pi*((diam_sm/2)**2.))+len(np.where(IJSN[:,2]==1)[0])*(m.pi*((diam_lg/2)**2.))
### Ratio:
a0=np.loadtxt('result/global/frame_area.txt')
phi0_b=a_part_b/a0

## Packing fraction (with current measured particle area):
### Ratio:
phi1=vec_area/a0

## Save Data:
np.savetxt('result/global/packing_fraction_rough.txt',phi0)             # using particle area measured at time t=0 with with DIC boundaries
np.savetxt('result/global/packing_fraction_rough_better.txt',phi0_b)    # using initial particle area
np.savetxt('result/global/packing_fraction_corrected.txt',phi1)         # using particle area measured at time t with DIC boundaries


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the global stress for each step:

## Load the force:
f0=np.loadtxt('result/global/force.txt')
errf0=np.loadtxt('result/global/error_force.txt')

## Compute piston width:
pst_len=np.sqrt((x_tr-x_tl)**2.+(y_tr-y_tl)**2.)*0.0254/reso_scan

## Compute stress:
sig0=f0/pst_len/(height_part*0.0254/reso_scan)
sig0_err=errf0/pst_len/(height_part*0.0254/reso_scan)

## Save Data:
np.savetxt('result/global/global_stress.txt',sig0)
np.savetxt('result/global/error_global_stress.txt',sig0_err)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the coordination for each step:

### Loop over step:
vec_z=float('nan')*np.ones(nb_img)
for it_stp in range(nb_img):
    K_cnt=np.loadtxt('result/contact/ctt_stp_'+'%03d'%it_stp+'.txt')
    if K_cnt.ndim>1:
        vec_z[it_stp]=2*K_cnt.shape[0]/nb_part

## Save Data:
np.savetxt('result/global/coordination.txt',vec_z)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the specific contact length for each step:

### Loop over particles:
mat_scl=float('nan')*np.ones((nb_part,nb_img))
for it_p in range(nb_part):
    mat_scl[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/contact_length.txt')/np.loadtxt('result/'+str(it_p).zfill(3)+'/perimeter.txt')

### Compute specific contact length:
scl0=np.mean(mat_scl,axis=0)
err_scl0=np.std(mat_scl,axis=0)/m.sqrt(nb_part)

## Save Data:
np.savetxt('result/global/specific_contact_length.txt',scl0)
np.savetxt('result/global/error_specific_contact_length.txt',err_scl0)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the particle anisotropy for each step:

### Loop over particle:
mat_ani=np.zeros((nb_part,nb_img))
#### Loop over particles:
for it_p in range(nb_part):
    mat_ani[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/anisotropy.txt')

### Compute coordination:
ani0=np.mean(mat_ani,axis=0)
err_ani0=np.std(mat_ani,axis=0)/m.sqrt(nb_part)

## Save Data:
np.savetxt('result/global/particle_anisotropy.txt',ani0)
np.savetxt('result/global/error_particle_anisotropy.txt',err_ani0)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the particle asphericity for each step:

### Loop over particle:
mat_asp=np.zeros((nb_part,nb_img))
#### Loop over particles:
for it_p in range(nb_part):
    mat_asp[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/asphericity.txt')

### Compute coordination:
asp0=np.mean(mat_asp,axis=0)
err_asp0=np.std(mat_asp,axis=0)/m.sqrt(nb_part)

## Save Data:
np.savetxt('result/global/particle_asphericity.txt',asp0)
np.savetxt('result/global/error_particle_asphericity.txt',err_asp0)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the void area for each step:

### Loop over steps:
vec_va=float('nan')*np.ones(nb_img)
err_vec_va=float('nan')*np.ones(nb_img)
for it_stp in range(nb_img):
    if os.path.isfile('result/void/void_stp_'+str(it_stp).zfill(3)+'.txt'): 
        Q=np.loadtxt('result/void/void_stp_'+str(it_stp).zfill(3)+'.txt')
        if Q.ndim>1:
            vec_cur=Q[:,1]
            vec_va[it_stp]=np.sum(vec_cur)
            err_vec_va[it_stp]=np.std(vec_cur)/m.sqrt(len(vec_cur))

## Save Data:
np.savetxt('result/global/void_area.txt',vec_va)
np.savetxt('result/global/error_void_area.txt',err_vec_va)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the void solidity for each step:

### Loop over steps:
vec_vs=float('nan')*np.ones(nb_img)
err_vec_vs=float('nan')*np.ones(nb_img)
for it_stp in range(nb_img):
    if os.path.isfile('result/void/void_stp_'+str(it_stp).zfill(3)+'.txt'): 
        Q=np.loadtxt('result/void/void_stp_'+str(it_stp).zfill(3)+'.txt')
        if Q.ndim>1:
            vec_cur=Q[:,4]
            vec_vs[it_stp]=np.mean(vec_cur)
            err_vec_vs[it_stp]=np.std(vec_cur)/m.sqrt(len(vec_cur))

## Save Data:
np.savetxt('result/global/void_solidity.txt',vec_vs)
np.savetxt('result/global/error_void_solidity.txt',err_vec_vs)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the void asphecity for each step:

### Loop over steps:
vec_va=float('nan')*np.ones(nb_img)
err_vec_va=float('nan')*np.ones(nb_img)
for it_stp in range(nb_img):
    if os.path.isfile('result/void/void_stp_'+str(it_stp).zfill(3)+'.txt'): 
        Q=np.loadtxt('result/void/void_stp_'+str(it_stp).zfill(3)+'.txt')
        if Q.ndim>1:
            vec_cur=Q[:,3]
            vec_va[it_stp]=np.mean(vec_cur)
            err_vec_va[it_stp]=np.std(vec_cur)/m.sqrt(len(vec_cur))

## Save Data:
np.savetxt('result/global/void_asphecity.txt',vec_va)
np.savetxt('result/global/error_void_asphecity.txt',err_vec_va)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the first strain invariant for each step:

### Load particle nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

### Loop over steps for soft particles:
if np.min(IJSN[:,3])==0:
    vec_I1=np.zeros(nb_img)
    err_vec_I1=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_invariant_1.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_I1[it_stp]=np.mean(vec_cur0)
        err_vec_I1[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    ## Save Data:
    np.savetxt('result/global/invariant_1_soft.txt',vec_I1)
    np.savetxt('result/global/error_invariant_1_soft.txt',err_vec_I1)

### Loop over steps for rigid particles:
if np.max(IJSN[:,3])==1:
    vec_I1=np.zeros(nb_img)
    err_vec_I1=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==1:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_invariant_1.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_I1[it_stp]=np.mean(vec_cur0)
        err_vec_I1[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    ## Save Data:
    np.savetxt('result/global/invariant_1_rigid.txt',vec_I1)
    np.savetxt('result/global/error_invariant_1_rigid.txt',err_vec_I1)



#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the second strain invariant for each step:

### Loop over steps for soft particles:
if np.min(IJSN[:,3])==0:
    vec_I2=np.zeros(nb_img)
    err_vec_I2=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_invariant_2.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_I2[it_stp]=np.mean(vec_cur0)
        err_vec_I2[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_I2,0)
    err_vec_va=np.append(err_vec_I2,0)
    
    ## Save Data:
    np.savetxt('result/global/invariant_2_soft.txt',vec_I2)
    np.savetxt('result/global/error_invariant_2_soft.txt',err_vec_I2)

### Loop over steps for rigid particles:
if np.max(IJSN[:,3])==1:
    vec_I2=np.zeros(nb_img)
    err_vec_I2=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==1:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_invariant_2.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_I2[it_stp]=np.mean(vec_cur0)
        err_vec_I2[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_I2,0)
    err_vec_va=np.append(err_vec_I2,0)
    
    ## Save Data:
    np.savetxt('result/global/invariant_2_rigid.txt',vec_I2)
    np.savetxt('result/global/error_invariant_2_rigid.txt',err_vec_I2)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the major strain eigenvalue for each step:

### Loop over steps for soft particles:
if np.min(IJSN[:,3])==0:
    vec_e1=np.zeros(nb_img)
    err_vec_e1=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_eigenvalue_1.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_e1[it_stp]=np.mean(vec_cur0)
        err_vec_e1[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_e1,0)
    err_vec_va=np.append(err_vec_e1,0)
    
    ## Save Data:
    np.savetxt('result/global/eigenvalue_1_soft.txt',vec_e1)
    np.savetxt('result/global/error_eigenvalue_1_soft.txt',err_vec_e1)

### Loop over steps for rigid particles:
if np.max(IJSN[:,3])==1:
    vec_e1=np.zeros(nb_img)
    err_vec_e1=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==1:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_eigenvalue_1.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_e1[it_stp]=np.mean(vec_cur0)
        err_vec_e1[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_e1,0)
    err_vec_va=np.append(err_vec_e1,0)
    
    ## Save Data:
    np.savetxt('result/global/eigenvalue_1_rigid.txt',vec_e1)
    np.savetxt('result/global/error_eigenvalue_1_rigid.txt',err_vec_e1)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the minor strain eigenvalue for each step:

### Loop over steps for soft particles:
if np.min(IJSN[:,3])==0:
    vec_e2=np.zeros(nb_img)
    err_vec_e2=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_eigenvalue_2.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_e2[it_stp]=np.mean(vec_cur0)
        err_vec_e2[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_e2,0)
    err_vec_va=np.append(err_vec_e2,0)
    
    ## Save Data:
    np.savetxt('result/global/eigenvalue_2_soft.txt',vec_e2)
    np.savetxt('result/global/error_eigenvalue_2_soft.txt',err_vec_e2)

### Loop over steps for rigid particles:
if np.max(IJSN[:,3])==1:
    vec_e2=np.zeros(nb_img)
    err_vec_e2=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==1:
                vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_eigenvalue_2.txt')
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_e2[it_stp]=np.mean(vec_cur0)
        err_vec_e2[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_e2,0)
    err_vec_va=np.append(err_vec_e2,0)
    
    ## Save Data:
    np.savetxt('result/global/eigenvalue_2_rigid.txt',vec_e2)
    np.savetxt('result/global/error_eigenvalue_2_rigid.txt',err_vec_e2)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the von Mises C for each step:

### Loop over steps for soft particles:
if np.min(IJSN[:,3])==0:
    vec_VM=np.zeros(nb_img)
    err_vec_VM=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over large particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                vec_e1=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_eigenvalue_1.txt')
                vec_e2=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_eigenvalue_2.txt')
                vec_cur=np.sqrt(vec_e1*vec_e1+vec_e2*vec_e2-vec_e1*vec_e2)
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_cur0=vec_cur0[vec_cur0<1.001]
        vec_VM[it_stp]=np.mean(vec_cur0)
        err_vec_VM[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_VM,0)
    err_vec_va=np.append(err_vec_VM,0)
    
    ## Save Data:
    np.savetxt('result/global/vonMises_soft.txt',vec_VM)
    np.savetxt('result/global/error_vonMises_soft.txt',err_vec_VM)

### Loop over steps for rigid particles:
if np.max(IJSN[:,3])==1:
    vec_VM=np.zeros(nb_img)
    err_vec_VM=np.zeros(nb_img)
    for it_stp in range(nb_img):
        vec_cur0=[]
        #### Loop over large particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==1:
                vec_e1=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_eigenvalue_1.txt')
                vec_e2=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_eigenvalue_2.txt')
                vec_cur=np.sqrt(vec_e1*vec_e1+vec_e2*vec_e2-vec_e1*vec_e2)
                vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                vec_cur=vec_cur[~np.isnan(vec_cur)]
                vec_cur0.append(vec_cur)
        
        vec_cur0=np.hstack(vec_cur0)
        vec_cur0=vec_cur0[vec_cur0<1.001]
        vec_VM[it_stp]=np.mean(vec_cur0)
        err_vec_VM[it_stp]=np.std(vec_cur0)/m.sqrt(len(vec_cur0))
    
    vec_va=np.append(vec_VM,0)
    err_vec_va=np.append(err_vec_VM,0)
    
    ## Save Data:
    np.savetxt('result/global/vonMises_rigid.txt',vec_VM)
    np.savetxt('result/global/error_vonMises_rigid.txt',err_vec_VM)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the fraction of non rattlers for each step:

### Loop over particle:
mat_fnr=np.zeros((nb_part,nb_img))
#### Large particles:
for it_p in range(nb_part):
    mat_fnr[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/coordination.txt')>2

### Compute the fraction of non-attlers:
fnr=np.sum(mat_fnr,axis=0)/nb_part

## Save Data:
fnr=np.append(fnr,fnr[-1])
np.savetxt('result/global/fraction_non_rattlers.txt',fnr)







