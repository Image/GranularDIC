#This code permits to track the grain (position, rotation) all along the experiment and gives for each grain picture 
#corrected with solid rigid motion.
# on ajoute la decroissance des tailles de cellules pour la detection du deplacement avec un calcul de la correlation
# on range les taches par fonctions (pour la parallelisation qui ne fonctionne pas)
# on parallelise en deux
# on prend en compte les grandes rotations et grands deplacement (manuellement) et on enregistre le goodness of fit
# on parallelise totalement


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Libraries:

##Commons ones:
import os
import numpy as np
import math as m
from PIL import Image
Image.MAX_IMAGE_PIXELS = 933120000
from PIL import ImageOps
import cv2
import time
from scipy import ndimage as nd
from scipy.optimize import minimize
from multiprocessing import Pool, Array
import yaml
import imageio

##Display one:
from matplotlib import pyplot as plt


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Number of picture :
nb_pict=data_input['general']['number of picture']

## Folder with pictures:
name_dir_im=data_input['picture handling']['folder']

## Picture extension:
name_ext_im=data_input['picture handling']['extension']

## Particule diameter [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Correlation width for particle tracking from largest to smallest[px]:
### Large:
cc_lg_M=data_input['particle tracking']['correlation cell size']['large'][1]
cc_lg_m=data_input['particle tracking']['correlation cell size']['large'][0]
### Small:
cc_sm_M=data_input['particle tracking']['correlation cell size']['small'][1]
cc_sm_m=data_input['particle tracking']['correlation cell size']['small'][0]
### Number of steps from large to small:
n_cc=data_input['particle tracking']['number of step']

## Correlation goodness acceptability:
corr_accpt=data_input['particle tracking']['correlation acceptability']

## Black border on the edges:
pp=data_input['picture handling']['image border']

### Number of processor for parallelization:
nb_proc=data_input['general']['number of processor']

## Verbosity:
verbose=False


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Initialisation:

## Load initial grain position:
pos_IJ=np.loadtxt(name_dir_im+'/../'+'initialParticlePosition/IJS_position.txt')

## Prepare shared variables:
share_vec=Array('d',np.zeros(4*len(pos_IJ)))

## Not stored input parameters:
### image reduction in correlation computation[<1]:
rr=0.1

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Define correlation fonction for particle tracking:

def CorrelVal(X,roi_0,roi_1):
    global rr
    ## Extraction of the variables:
    dI=X[0]
    dJ=X[1]
    dA=X[2]
    ## Rotate the picture:
    roi_1_r=nd.interpolation.rotate(roi_1,dA,reshape=False,order=1,mode='constant',cval=0.,prefilter=False)
    ## Shift the picture:
    roi_1_rs=nd.interpolation.shift(roi_1_r,(dI,dJ),order=1,mode='constant',cval=0.,prefilter=False)
    ## Reduction to the center of the ROI:
    roi_0_r=roi_0[int(len(roi_0)*rr):int(len(roi_0)*(1.-rr)),int(len(roi_0)*rr):int(len(roi_0)*(1.-rr))]
    roi_1_rs_r=roi_1_rs[int(len(roi_0)*rr):int(len(roi_0)*(1.-rr)),int(len(roi_0)*rr):int(len(roi_0)*(1.-rr))]
    ## Correlation:
    s=np.sum(abs(roi_1_rs_r-roi_0_r))/(len(roi_0)**2)
    return s


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Main fonctions to detect motion for particles:

def Main(it,def_param=share_vec):
    ## Define global variables:
    global name_dir_im, vec_cc_sm , vec_cc_lg, image_0, image_1, itImg_var, corr_accpt, pos_IJ
    ## Load current position and orientation:
    I_cur=IJ_time[it,0,itImg_var]
    J_cur=IJ_time[it,1,itImg_var]
    A_cur=IJ_time[it,2,itImg_var]
    S_cur=int(pos_IJ[it,2])
    I_n=I_cur.copy(); J_n=J_cur.copy()
    ## Distinction between small and large pictures:
    if S_cur:
        vec_cc=vec_cc_lg
        diam=diam_lg
    else:
        vec_cc=vec_cc_sm
        diam=diam_sm
    ## Loop over the correlation cell lengths:
    for it_cc in range(len(vec_cc)):
        cc=vec_cc[it_cc]
        ### Extraction of the ROI:
        I_roi_m_0=int(I_cur-cc/2.); I_roi_M_0=int(I_cur+cc/2.); J_roi_m_0=int(J_cur-cc/2.); J_roi_M_0=int(J_cur+cc/2.)
        roi_0=image_0[I_roi_m_0:I_roi_M_0,J_roi_m_0:J_roi_M_0]
        I_roi_m_1=int(I_n-cc/2.); I_roi_M_1=int(I_n+cc/2.); J_roi_m_1=int(J_n-cc/2.); J_roi_M_1=int(J_n+cc/2.)
        roi_1=image_1[I_roi_m_1:I_roi_M_1,J_roi_m_1:J_roi_M_1]
        ### Measurement shift between picture from FFT:
        #### Compute Fourier transform:
        cell0FFT=np.fft.fft2(roi_0)
        cell1FFT=np.conjugate(np.fft.fft2(roi_1))
        #### Convolute:
        pictCCor=np.real(np.fft.ifft2((cell0FFT*cell1FFT)))
        #### Compute the shift: 
        pictCCorShift=np.fft.fftshift(pictCCor)
        I_sft,J_sft=np.where(pictCCorShift==np.max(pictCCorShift))
        I_sft=I_sft-int(len(roi_0)/2)
        J_sft=J_sft-int(len(roi_0)/2)
        ### Set the new approximate position:
        I_n=I_n-I_sft
        J_n=J_n-J_sft
    ## Extract the new ROI:
    I_roi_m_n=int(I_n-cc/2.); I_roi_M_n=int(I_n+cc/2.); J_roi_m_n=int(J_n-cc/2.); J_roi_M_n=int(J_n+cc/2.)
    roi_1=image_1[I_roi_m_n:I_roi_M_n,J_roi_m_n:J_roi_M_n]
    ## Optimisation of the correlation:
    ext_res=minimize(CorrelVal,np.array([0.1,0.1,0.1]),args=(roi_0,roi_1),method='Nelder-Mead',tol=1e-4,options={'maxiter':500,'maxfev':1000,'disp':verbose}) 
    ## Final and accurate position and rotation measurement: 
    dI_0=ext_res.x[0]
    dJ_0=ext_res.x[1]
    A_n=A_cur+ext_res.x[2]
    print('particle '+str(it)+': '+str(ext_res.fun))
    ## Test if it needs something more accurate:
    if (ext_res.fun>corr_accpt):
        ### Go back to the largest cells:
        cc=vec_cc[0]
        ### Extraction of the ROI:
        I_roi_m_0=int(I_cur-cc/2.); I_roi_M_0=int(I_cur+cc/2.); J_roi_m_0=int(J_cur-cc/2.); J_roi_M_0=int(J_cur+cc/2.)
        roi_0=image_0[I_roi_m_0:I_roi_M_0,J_roi_m_0:J_roi_M_0]
        I_roi_m_1=int(I_n-cc/2.); I_roi_M_1=int(I_n+cc/2.); J_roi_m_1=int(J_n-cc/2.); J_roi_M_1=int(J_n+cc/2.)
        roi_1=image_1[I_roi_m_1:I_roi_M_1,J_roi_m_1:J_roi_M_1]
        ### Segment the area of interest:
        roi_0_lft=roi_0[int(1.5*cc/4.):int(2.5*cc/4.),0:int(cc/4.)]
        roi_0_rgt=roi_0[int(1.5*cc/4.):int(2.5*cc/4.),int(3.*cc/4.):int(cc)]
        roi_1_lft=roi_1[int(1.5*cc/4.):int(2.5*cc/4.),0:int(cc/4.)]
        roi_1_rgt=roi_1[int(1.5*cc/4.):int(2.5*cc/4.),int(3.*cc/4.):int(cc)]
        ### Compute the shift for each part:
        #### Compute Fourier transform:
        cell0FFT_lft=np.fft.fft2(roi_0_lft)
        cell1FFT_lft=np.conjugate(np.fft.fft2(roi_1_lft))
        cell0FFT_rgt=np.fft.fft2(roi_0_rgt)
        cell1FFT_rgt=np.conjugate(np.fft.fft2(roi_1_rgt))
        #### Convolute:
        pictCCor_lft=np.real(np.fft.ifft2((cell0FFT_lft*cell1FFT_lft)))
        pictCCor_rgt=np.real(np.fft.ifft2((cell0FFT_rgt*cell1FFT_rgt)))
        #### Compute the shift: 
        pictCCorShift_lft=np.fft.fftshift(pictCCor_lft)
        I_sft_lft,J_sft_lft=np.where(pictCCorShift_lft==np.max(pictCCorShift_lft))
        I_sft_lft=I_sft_lft-int(len(roi_0_lft)/2)
        J_sft_lft=J_sft_lft-int(len(roi_0_lft)/2)
        pictCCorShift_rgt=np.fft.fftshift(pictCCor_rgt)
        I_sft_rgt,J_sft_rgt=np.where(pictCCorShift_rgt==np.max(pictCCorShift_rgt))
        I_sft_rgt=I_sft_rgt-int(len(roi_0_rgt)/2)
        J_sft_rgt=J_sft_rgt-int(len(roi_0_rgt)/2)
        ### Compute the global shift:
        I_sft_n=0.5*(I_sft_lft+I_sft_rgt)
        J_sft_n=0.5*(J_sft_lft+J_sft_rgt)
        ### Compute the global angle:
        dA_0=m.atan2(I_sft_lft-I_sft_rgt,J_sft_lft-J_sft_rgt+3.*cc/4.)*180./m.pi
        ## Extract the newer ROI:
        I_n=I_n-I_sft_n
        J_n=J_n-J_sft_n
        I_roi_m_nn=int(I_n-cc/2.); I_roi_M_nn=int(I_n+cc/2.); J_roi_m_nn=int(J_n-cc/2.); J_roi_M_nn=int(J_n+cc/2.)
        roi_1_n=image_1[I_roi_m_nn:I_roi_M_nn,J_roi_m_nn:J_roi_M_nn]
        ## Optimisation of the correlation:
        ext_res=minimize(CorrelVal,np.array([0.1,0.1,dA_0]),args=(roi_0,roi_1_n),method='Nelder-Mead',tol=1e-4,options={'maxiter':500,'maxfev':1000,'disp':verbose}) 
        ## Final and accurate position and rotation measurement: 
        dI_0=ext_res.x[0]
        dJ_0=ext_res.x[1]
        A_n=A_cur+ext_res.x[2]
        print('particle '+str(it)+': '+str(ext_res.fun)+'  !! Improved !!')
    ##Picture extraction:
    if (ext_res.fun<corr_accpt):
        ray_L=int(diam*1.2/2.) 
        if itImg_var==0:
            os.system('mkdir '+name_dir_im+'/../'+'particlePicture/'+'%03d'%it)
            I_roi_m=int(I_cur-ray_L); I_roi_M=int(I_cur+ray_L); J_roi_m=int(J_cur-ray_L); J_roi_M=int(J_cur+ray_L)
            roi_0=image_0[I_roi_m:I_roi_M,J_roi_m:J_roi_M]
            imageio.imwrite(name_dir_im+'/../'+'particlePicture/'+'%03d'%it+'/'+'%04d'%itImg_var+'.png', roi_0.astype('uint8'))
        I_roi_m=int(I_n-ray_L); I_roi_M=int(I_n+ray_L); J_roi_m=int(J_n-ray_L); J_roi_M=int(J_n+ray_L)
        roi_1=image_1[I_roi_m:I_roi_M,J_roi_m:J_roi_M]
        roi_1=nd.interpolation.rotate(roi_1,A_n,reshape=False,order=1,mode='constant',cval=0.,prefilter=False)
        roi_1=nd.interpolation.shift(roi_1,(dI_0,dJ_0),order=1,mode='constant',cval=0.,prefilter=False)
        imageio.imwrite(name_dir_im+'/../'+'particlePicture/'+'%03d'%it+'/'+'%04d'%(itImg_var+1)+'.png',roi_1.astype('uint8'))
    ##Store displacement:
    share_vec[it]=I_n+dI_0
    share_vec[len(pos_IJ)+it]=J_n+dJ_0
    share_vec[2*len(pos_IJ)+it]=A_n
    share_vec[3*len(pos_IJ)+it]=ext_res.fun


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Fonctions to manually improve the particle tracking:

def Improve(it):
    ## Define global variables:
    global name_dir_im, IJ_time, vec_cc_lg, vec_cc_sm, image_0, image_1, itImg_var
    ## Load current position and orientation:
    I_cur=IJ_time[it,0,itImg_var]
    J_cur=IJ_time[it,1,itImg_var]
    A_cur=IJ_time[it,2,itImg_var]
    S_cur=int(pos_IJ[it,2])
    I_n=I_cur.copy(); J_n=J_cur.copy()
    ## Distinction between small and large pictures:
    if S_cur:
        vec_cc=vec_cc_lg
        diam=diam_lg
    else:
        vec_cc=vec_cc_sm
        diam=diam_sm
    ## Manual detection of the center:
    cc=int(2.*min(diam,image_0.shape[0]-I_cur,I_cur,image_0.shape[1]-J_cur,J_cur,image_1.shape[0]-I_cur,I_cur,image_1.shape[1]-J_cur,J_cur)-1.)
    while cc>15:
        ### Load pictures:
        I_roi_m_0=int(I_cur-cc/2.); I_roi_M_0=int(I_cur+cc/2.); J_roi_m_0=int(J_cur-cc/2.); J_roi_M_0=int(J_cur+cc/2.)
        roi_0=image_0[I_roi_m_0:I_roi_M_0,J_roi_m_0:J_roi_M_0]
        I_roi_m_1=int(I_n-cc/2.); I_roi_M_1=int(I_n+cc/2.); J_roi_m_1=int(J_n-cc/2.); J_roi_M_1=int(J_n+cc/2.)
        roi_1=image_1[I_roi_m_1:I_roi_M_1,J_roi_m_1:J_roi_M_1]
        ### Manual track the center:
        plt.figure(num=None,figsize=(20,10),dpi=100,facecolor='w',edgecolor='k')
        plt.subplot(1,2,1)
        plt.imshow(roi_0,cmap=plt.cm.Greys)
        plt.plot(roi_0.shape[0]/2,roi_0.shape[0]/2,'ob',markersize=5)
        plt.xlabel('undeformed')
        plt.axis([0,cc,cc,0])
        plt.subplot(1,2,2)
        plt.imshow(roi_1,cmap=plt.cm.Greys)
        plt.xlabel('deformed')
        plt.title('Select the point corresponding with the blue dot')
        select_0=plt.ginput(1,timeout=-1)
        plt.close()
        ### update the current position:
        I_n=I_n+int(select_0[0][1]-cc/2.)
        J_n=J_n+int(select_0[0][0]-cc/2.)
        ### Downsize:
        cc=int(cc/2.)
        
    ## Manual detection of the rotation:
    I_cur_r=I_cur
    J_cur_r=J_cur+0.7*diam/2
    I_n_r=I_cur_r.copy(); J_n_r=J_cur_r.copy()
    cc=int(2.*min(diam,image_0.shape[0]-I_cur_r,I_cur_r,image_0.shape[1]-J_cur_r,J_cur_r,image_1.shape[0]-I_cur_r,I_cur_r,image_1.shape[1]-J_cur_r,J_cur_r)-1.)
    while cc>15:
        ### Load pictures:
        I_roi_m_0=int(I_cur_r-cc/2.); I_roi_M_0=int(I_cur_r+cc/2.); J_roi_m_0=int(J_cur_r-cc/2.); J_roi_M_0=int(J_cur_r+cc/2.)
        roi_0=image_0[I_roi_m_0:I_roi_M_0,J_roi_m_0:J_roi_M_0]
        I_roi_m_1=int(I_n_r-cc/2.); I_roi_M_1=int(I_n_r+cc/2.); J_roi_m_1=int(J_n_r-cc/2.); J_roi_M_1=int(J_n_r+cc/2.)
        roi_1=image_1[I_roi_m_1:I_roi_M_1,J_roi_m_1:J_roi_M_1]
        ### Manual track the center:
        plt.figure(num=None,figsize=(20,10),dpi=100,facecolor='w',edgecolor='k')
        plt.subplot(1,2,1)
        plt.imshow(roi_0,cmap=plt.cm.Greys)
        plt.plot(roi_0.shape[0]/2,roi_0.shape[0]/2,'ob',markersize=5)
        plt.xlabel('undeformed')
        plt.axis([0,cc,cc,0])
        plt.subplot(1,2,2)
        plt.imshow(roi_1,cmap=plt.cm.Greys)
        plt.xlabel('deformed')
        plt.title('Select the point corresponding with the blue dot')
        select_0=plt.ginput(1,timeout=-1)
        plt.close()
        ### update the current position:
        I_n_r=I_n_r+int(select_0[0][1]-cc/2.)
        J_n_r=J_n_r+int(select_0[0][0]-cc/2.)
        ### Downsize:
        cc=int(cc/2.)
    
    dA_0=m.atan2(I_n_r-I_n,J_n_r-J_n)*180./m.pi
    ## Extract the newer ROI:
    cc=vec_cc[-1]
    I_roi_m_0=int(I_cur-cc/2.); I_roi_M_0=int(I_cur+cc/2.); J_roi_m_0=int(J_cur-cc/2.); J_roi_M_0=int(J_cur+cc/2.)
    roi_0=image_0[I_roi_m_0:I_roi_M_0,J_roi_m_0:J_roi_M_0]
    I_roi_m_nn=int(I_n-cc/2.); I_roi_M_nn=int(I_n+cc/2.); J_roi_m_nn=int(J_n-cc/2.); J_roi_M_nn=int(J_n+cc/2.)
    roi_1=image_1[I_roi_m_nn:I_roi_M_nn,J_roi_m_nn:J_roi_M_nn]
    ## Optimisation of the correlation:
    ext_res=minimize(CorrelVal,np.array([0.1,0.1,dA_0]),args=(roi_0,roi_1),method='Nelder-Mead',tol=1e-4,options={'maxiter':500,'maxfev':1000,'disp':verbose}) 
    ## Final and accurate position and rotation measurement: 
    dI_0=ext_res.x[0]
    dJ_0=ext_res.x[1]
    A_n=A_cur+ext_res.x[2]
    print('particle '+str(it)+': '+str(ext_res.fun)+'  !! Improved !!')
    ##Picture extraction:
    ray_L=int(diam*1.2/2.)
    I_roi_m=int(I_n-ray_L); I_roi_M=int(I_n+ray_L); J_roi_m=int(J_n-ray_L); J_roi_M=int(J_n+ray_L)
    roi_1=image_1[I_roi_m:I_roi_M,J_roi_m:J_roi_M]
    roi_1=nd.interpolation.rotate(roi_1,A_n,reshape=False,order=1,mode='constant',cval=0.,prefilter=False)
    roi_1=nd.interpolation.shift(roi_1,(dI_0,dJ_0),order=1,mode='constant',cval=0.,prefilter=False)
    imageio.imwrite(name_dir_im+'/../'+'particlePicture/'+'%03d'%it+'/'+'%04d'%(itImg_var+1)+'.png',roi_1.astype('uint8'))
    ##Store displacement:
    IJ_time[it,0,itImg_var+1]=I_n+dI_0
    IJ_time[it,1,itImg_var+1]=J_n+dJ_0
    IJ_time[it,2,itImg_var+1]=A_n
    IJ_time[it,3,itImg_var+1]=ext_res.fun


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Particle tracking and picture extraction:

## Initialize storage:
IJ_time=np.zeros((len(pos_IJ),4,nb_pict))
IJ_time[:,0:2,0]=pos_IJ[:,0:2]
os.system('mkdir '+name_dir_im+'/../'+'particlePicture')

## Initialize cell dimension vectors:
vec_cc_lg=np.linspace(cc_lg_M,cc_lg_m,n_cc)
vec_cc_sm=np.linspace(cc_sm_M,cc_sm_m,n_cc)

## Loop over the pictures:
for itImg in range(nb_pict-1):
    print('picture '+str(itImg))
    itImg_var=itImg
    ### Load the current picture:
    image_0=Image.open(os.path.join(name_dir_im,'%04d'%itImg+name_ext_im))
    image_0=ImageOps.expand(image_0,border=pp,fill='black')
    image_0=np.array(image_0).astype('float')
    image_1=Image.open(os.path.join(name_dir_im,'%04d'%(itImg+1)+name_ext_im))
    image_1=ImageOps.expand(image_1,border=pp,fill='black')
    image_1=np.array(image_1).astype('float')
    ### Loop over the particles:
    p=Pool(nb_proc)
    p.map(Main,range(pos_IJ.shape[0]))
    p.close()
    IJ_time[:,0,itImg_var+1]=share_vec[0:pos_IJ.shape[0]]
    IJ_time[:,1,itImg_var+1]=share_vec[pos_IJ.shape[0]:2*pos_IJ.shape[0]]
    IJ_time[:,2,itImg_var+1]=share_vec[2*pos_IJ.shape[0]:3*pos_IJ.shape[0]]
    IJ_time[:,3,itImg_var+1]=share_vec[3*pos_IJ.shape[0]:4*pos_IJ.shape[0]]
    ### Check if everything is properly tracked:
    vec_it=np.where(IJ_time[:,3,itImg_var+1]>corr_accpt)[0]
    if len(vec_it):
        print('Problems occured with small particles CLOSE the window to fix it manually...')
        plt.plot(0,0,'ok')
        plt.title('step '+str(itImg)+'/'+str(nb_pict-1)+'\nclose me')
        plt.show()
        plt.close()
        for it in vec_it:
            Improve(it)
        A=input('Do you want to keep on? Press enter to continue and entre No to stop:')
        if A=='No':
            break # To finish
    
    del image_0,image_1


## Save particle position and angle:
np.savetxt(name_dir_im+'/../'+'particlePicture/I_stp.txt',IJ_time[:,0,:],fmt='%f')
np.savetxt(name_dir_im+'/../'+'particlePicture/J_stp.txt',IJ_time[:,1,:],fmt='%f')
np.savetxt(name_dir_im+'/../'+'particlePicture/A_stp.txt',IJ_time[:,2,:],fmt='%f')
np.savetxt(name_dir_im+'/../'+'particlePicture/error_stp.txt',IJ_time[:,3,:],fmt='%f')

## Display error:
err=np.loadtxt(name_dir_im+'/../'+'particlePicture/error_stp.txt')
[I,J]=np.where(err>corr_accpt)
plt.imshow(err,interpolation='none',vmin=0,vmax=corr_accpt,aspect='auto')
for it_err in range(len(I)):
    plt.text(J[it_err],I[it_err],'%03d'%I[it_err]+'-'+str(J[it_err]),fontsize=5)

plt.xlabel('steps')
plt.ylabel('particles')
plt.title('error')
plt.colorbar()
plt.savefig(name_dir_im+'/../'+'particlePicture/error_stp.png',dpi=200)
plt.close()







