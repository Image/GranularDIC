#This code permits to specifiy the nature of the grain (rigide or soft)


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Libraries:

##Commons ones:
import os
import numpy as np
from PIL import Image
Image.MAX_IMAGE_PIXELS = 933120000
from PIL import ImageOps
import yaml

##Display one:
from matplotlib import pyplot as plt


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Folder with pictures:
name_dir_im=data_input['picture handling']['folder']

## Picture extension:
name_ext_im=data_input['picture handling']['extension']

## Image reduction factor for making small illustrative pictures:
ImRat_med=data_input['picture handling']['medium reduction ratio']

## Black border on the edges:
pp=data_input['picture handling']['image border']


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Select rigid particles:

## Load and resize initial picture:
### Load picture:
image_0=Image.open(os.path.join(name_dir_im,'%04d'%0+name_ext_im))
image_0=ImageOps.expand(image_0,border=pp,fill='black')
### Resize picture:
size_img_0=image_0.size
image_0.thumbnail((int(size_img_0[0]*ImRat_med),int(size_img_0[1]*ImRat_med)),Image.ANTIALIAS)
image_0=np.array(image_0).astype('float')

## Laod the particle position and initialize the grain nature vector (0: soft, 1: rigid):

if os.path.isfile('initialParticlePosition/IJS_position.txt'):
    IJS=np.loadtxt('initialParticlePosition/IJS_position.txt')
    vec_nat=np.zeros(IJS.shape[0])
else:
    IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')
    vec_nat=IJSN[:,3]
    IJS=IJSN[:,0:3]

## Select rigid grains:
### Define selection function:
def onclick(event):
    global IJS, vec_nat
    if event.button==3 and event.inaxes:
        #### Get click coordinate:
        x0=event.xdata
        y0=event.ydata
        #### Get closest particle:
        d=np.sqrt((IJS[:,0]*ImRat_med-y0)**2.+(IJS[:,1]*ImRat_med-x0)**2.)
        I=np.where(d==np.min(d))[0][0]
        vec_nat[I]=not vec_nat[I]
        if vec_nat[I]==0:
            col='c'
        else:
            col='b'
        if IJS[I,2]==0:
            siz=15
        else:
            siz=30
        plt.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.'+col, markersize=siz)
        plt.draw() 

##Selection loop:
### Plot picture:
fig,ax=plt.subplots(num=None,figsize=(16,12),dpi=100)
ax.imshow(image_0,cmap=plt.cm.Greys)

### Plot large soft grains:
I=np.where((IJS[:,2]==1)*(vec_nat==0))[0]
ax.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.c', markersize=30)

### Plot small soft grains:
I=np.where((IJS[:,2]==0)*(vec_nat==0))[0]
ax.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.c', markersize=15)

### Plot large rigid grains:
I=np.where((IJS[:,2]==1)*(vec_nat==1))[0]
ax.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.b', markersize=30)

### Plot small rigid grains:
I=np.where((IJS[:,2]==0)*(vec_nat==1))[0]
ax.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.b', markersize=15)

ax.set_title('right clic close to the point you want to change \n or close \n dark -> rigid')
cid=fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Save results:

## Stack and save vector:
IJSN=np.vstack((IJS.T,vec_nat)).T
np.savetxt('initialParticlePosition/IJSN_position.txt',IJSN,fmt='%d')
os.system('rm initialParticlePosition/IJS_position.txt')

## Plot result:
### Plot picture:
plt.imshow(image_0,cmap=plt.cm.Greys)
plt.title('Grain positions, natures & numbers')

### Plot grain positions:
I=np.where((IJS[:,2]==1)*(vec_nat==0))[0]
plt.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.c', markersize=30)
I=np.where((IJS[:,2]==0)*(vec_nat==0))[0]
plt.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.c', markersize=15)
I=np.where((IJS[:,2]==1)*(vec_nat==1))[0]
plt.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.b', markersize=30)
I=np.where((IJS[:,2]==0)*(vec_nat==1))[0]
plt.plot(IJS[I,1]*ImRat_med,IJS[I,0]*ImRat_med,'.b', markersize=15)

### Plot large grains:
I=np.where(IJS[:,2]==1)[0]
for it in range(len(I)):
    I_cur=IJS[I[it],0]*ImRat_med
    J_cur=IJS[I[it],1]*ImRat_med
    plt.text(J_cur,I_cur+10,'%03d'%I[it],horizontalalignment='center',verticalalignment='center',fontsize=8, color='black',weight='bold')

### Plot small grains:
I=np.where(IJS[:,2]==0)[0]
for it in range(len(I)):
    I_cur=IJS[I[it],0]*ImRat_med
    J_cur=IJS[I[it],1]*ImRat_med
    plt.text(J_cur,I_cur+10,'%03d'%I[it],horizontalalignment='center',verticalalignment='center',fontsize=5, color='black',weight='bold')

plt.axis('off')

plt.savefig(name_dir_im+'/../'+'initialParticlePosition/name_position_nature.png',dpi=200)
plt.close() 


















