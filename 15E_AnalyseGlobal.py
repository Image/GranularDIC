
#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np

##Display one:
from matplotlib import pyplot as plt


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load data:

coordination=np.loadtxt('result/global/coordination.txt')
displacement=np.loadtxt('result/global/displacement.txt')
force=np.loadtxt('result/global/force.txt')
global_strain=np.loadtxt('result/global/global_strain.txt')
global_stress=np.loadtxt('result/global/global_stress.txt')
packing_fraction_corrected=np.loadtxt('result/global/packing_fraction_corrected.txt')
packing_fraction_rough=np.loadtxt('result/global/packing_fraction_rough.txt')
particle_anisotropy=np.loadtxt('result/global/particle_anisotropy.txt')
particle_asphericity=np.loadtxt('result/global/particle_asphericity.txt')
specific_contact_length=np.loadtxt('result/global/specific_contact_length.txt')
void_area=np.loadtxt('result/global/void_area.txt')
void_asphecity=np.loadtxt('result/global/void_asphecity.txt')
void_solidity=np.loadtxt('result/global/void_solidity.txt')

IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')
if np.min(IJSN[:,3])==0:
    eigenvalue_1_sft=np.loadtxt('result/global/eigenvalue_1_soft.txt')
    eigenvalue_2_sft=np.loadtxt('result/global/eigenvalue_2_soft.txt')
    invariant_1_sft=np.loadtxt('result/global/invariant_1_soft.txt')
    invariant_2_sft=np.loadtxt('result/global/invariant_2_soft.txt')
    vonMises_sft=np.loadtxt('result/global/vonMises_soft.txt')

if np.max(IJSN[:,3])==1:
    eigenvalue_1_rgd=np.loadtxt('result/global/eigenvalue_1_rigid.txt')
    eigenvalue_2_rgd=np.loadtxt('result/global/eigenvalue_2_rigid.txt')
    invariant_1_rgd=np.loadtxt('result/global/invariant_1_rigid.txt')
    invariant_2_rgd=np.loadtxt('result/global/invariant_2_rigid.txt')
    vonMises_rgd=np.loadtxt('result/global/vonMises_rigid.txt')

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Plot global mechanical observables:


plt.plot(packing_fraction_rough,packing_fraction_corrected,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$\phi_{corrected}$')
plt.show()
plt.close()


plt.plot(packing_fraction_rough,coordination,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$Z$')
plt.show()
plt.close()


plt.plot(global_strain,global_stress,'-',linewidth=2)
plt.xlabel('$\epsilon$')
plt.ylabel('$\sigma$')
plt.show()
plt.close()


plt.plot(packing_fraction_rough,global_stress,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$\sigma$')
plt.show()
plt.close()


plt.plot(packing_fraction_rough,global_strain,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$\epsilon$')
plt.show()
plt.close()


plt.plot(coordination,force,'-',linewidth=2)
plt.xlabel('$Z$')
plt.ylabel('$f$')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Plot particle geometry observables:

plt.plot(packing_fraction_rough,particle_anisotropy,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('grain anisotropy')
plt.show()
plt.close()


plt.plot(coordination,particle_anisotropy,'-',linewidth=2)
plt.xlabel('$Z$')
plt.ylabel('grain anisotropy')
plt.show()
plt.close()


plt.plot(packing_fraction_rough,particle_asphericity,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('grain asphericity')
plt.show()
plt.close()


plt.plot(packing_fraction_rough,specific_contact_length,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('specific contact length')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Plot void geometry observables:

plt.plot(packing_fraction_rough,void_area,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('void area')
plt.show()
plt.close()

plt.plot(packing_fraction_rough,void_solidity,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('void solidity')
plt.show()
plt.close()

plt.plot(packing_fraction_rough,void_asphecity,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('void asphericity')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Plot strain observables:

plt.plot(packing_fraction_rough,invariant_1_sft-1,'-',linewidth=2)
plt.plot(packing_fraction_rough,invariant_2_sft,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$C$ invariant')
plt.legend(['I1-1','I2'])
plt.show()
plt.close()

plt.plot(packing_fraction_rough,invariant_1_rgd,'-',linewidth=2)
plt.plot(packing_fraction_rough,invariant_2_rgd,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$E$ invariant')
plt.legend(['I1','I2'])
plt.show()
plt.close()


plt.plot(coordination,invariant_1_sft-1,'-',linewidth=2)
plt.plot(coordination,invariant_2_sft,'-',linewidth=2)
plt.xlabel('$Z$')
plt.ylabel('$C$ invariant')
plt.legend(['I1-1','I2'])
plt.show()
plt.close()

plt.plot(coordination,invariant_1_rgd,'-',linewidth=2)
plt.plot(coordination,invariant_2_rgd,'-',linewidth=2)
plt.xlabel('$Z$')
plt.ylabel('$E$ invariant')
plt.legend(['I1','I2'])
plt.show()
plt.close()


plt.plot(packing_fraction_rough,eigenvalue_1_sft,'-',linewidth=2)
plt.plot(packing_fraction_rough,eigenvalue_2_sft,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$C$ eigenvalue')
plt.legend(['E1','E2'])
plt.show()
plt.close()

plt.plot(packing_fraction_rough,eigenvalue_1_rgd,'-',linewidth=2)
plt.plot(packing_fraction_rough,eigenvalue_2_rgd,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('$E$ eigenvalue')
plt.legend(['E1','E2'])
plt.show()
plt.close()


plt.plot(packing_fraction_rough,vonMises_sft,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('von Mises $C$')
plt.show()
plt.close()

plt.plot(packing_fraction_rough,vonMises_rgd,'-',linewidth=2)
plt.xlabel('$\phi$')
plt.ylabel('von Mises $E$')
plt.show()
plt.close()


plt.plot(global_strain,vonMises_sft,'-',linewidth=2)
plt.xlabel('$\epsilon$')
plt.ylabel('von Mises $C$')
plt.show()
plt.close()

plt.plot(global_strain,vonMises_rgd,'-',linewidth=2)
plt.xlabel('$\epsilon$')
plt.ylabel('von Mises $E$')
plt.show()
plt.close()


plt.plot(global_stress,vonMises_sft,'-',linewidth=2)
plt.xlabel('$\sigma$')
plt.ylabel('von Mises $C$')
plt.show()
plt.close()

plt.plot(global_stress,vonMises_rgd,'-',linewidth=2)
plt.xlabel('$\sigma$')
plt.ylabel('von Mises $E$')
plt.show()
plt.close()



