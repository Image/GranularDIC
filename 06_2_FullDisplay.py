# This code load the DIC and tracking results for each grain of multigrain experiments  and
# - display results for each particles
# - display results for the full system


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
from scipy.interpolate import griddata
from PIL import Image
import math as m
import yaml
from matplotlib.path import Path

##Display one:
from matplotlib import pyplot as plt
import matplotlib.cm as cm 

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Particule diameter [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Correlation parameter:
### Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']

## Displaying parameter:
### Local scale displaying of results (0: none, 1: reduced, 2: full):
local_disp=data_input['displaying']['local displaying']
### Displaying accuracy for global movies:
disp_acc_mov=data_input['displaying']['field accuracy coarse']
### Displaying accuracy for fancy pictures:
disp_acc_fancy=data_input['displaying']['field accuracy thin']


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Plot the edge and shape evolution for each particle:

## Loop over particles:
### Pick loop size:
if local_disp==0:
    n_plot=0
elif local_disp==1:
    n_plot=min(10,nb_part)
else :
    n_plot=nb_part
### Loop:
for it_p in range(n_plot):
    
    ### Display:
    print('Edge plot - particle: '+'%03d'%it_p)
    
    ### Load data:
    I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
    J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
    
    ### Cell displacements:
    mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
    mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
    
    ### Outlier matrix:
    mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
    
    ### Make folder to store pictures:
    os.system('mkdir tmp0')
    
    ### Loop over the frames:
    flag0=True
    for itImg in range(nb_img):
        
        #### Load the current picture:
        pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(itImg).zfill(4)+'.png'))
        
        #### Load edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(itImg).zfill(4)+'.txt')
        
        #### Loop edges:
        edge=np.append(edge,edge[0]).astype('int')
        
        #### Index of valid cells:
        I_val=np.where(mat_outl[:,itImg]==0)[0]
        
        #### Extract current displacements and correlations:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        
        if flag0:
            #### Limit plot computation:
            jm=0; jM=pict1.shape[1]
            im=0; iM=pict1.shape[0]
            flag0=False
        
        #### Get current edge positions:
        I_cur_edg=I_cur[edge]
        J_cur_edg=J_cur[edge]
        
        #### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        
        #### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        
        #### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        
        #!DEBUG!
        # ~ plt.plot(J_cur_edg,I_cur_edg,'-ob')
        # ~ plt.plot(J_c-r_cur_edg*np.cos(a_cur_edg),(I_c-r_cur_edg*np.sin(a_cur_edg)),'+r')
        # ~ plt.plot(J_cur_edg_imp,I_cur_edg_imp,'.-m')
        # ~ plt.show()
        
        #### Plot deformed shape with edges:
        plt.imshow(pict1,cmap='gray')
        plt.plot(J_cur[I_val],I_cur[I_val],'.c',markersize=3)
        plt.plot(J_cur,I_cur,'.r',markersize=1.5)
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-b',linewidth=3)
        plt.axis('equal')
        plt.axis('off')
        plt.title('particle '+'%03d'%it_p+' - step '+'%03d'%itImg)
        plt.axis([jm,jM,jm,jM])
        plt.savefig('tmp0/'+'%04d'%(itImg)+'.png',dpi=200)
        plt.close()
    
    ### Make the movie:
    os.system('mkdir result/'+'%03d'%it_p+'/video')
    null=os.system('ffmpeg -y -r 5 -f image2 -i tmp0/%04d.png -qscale 10 result/'+'%03d'%it_p+'/video/shape_edge.avi')
    null=os.system('rm -rf tmp0')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Plot the edge and shape evolution for the whole system:

## Loading tracking data (angle,i,j):
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Storage folder:
os.system('mkdir video')
os.system('mkdir video/edge')

## Vector to store image size:
vec_im_siz=np.zeros(nb_part)

## Loop over the pictures:
for it_stp in range(nb_img):
    
    ### Display:
    print('Edge plot - step: '+str(it_stp))
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ### Prepare figure:
    plt.figure()
    if it_stp==0:
        JM=np.max(J_cur0)+diam_lg/2.
        Im=-np.max(I_cur0)-diam_lg/2.
    
    ### Plot for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        ##### Cell outlier:
        mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
        I_val=np.where(mat_outl[:,it_stp]==0)[0]
        #### Get image size:
        if it_stp==0:
            pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(it_stp).zfill(4)+'.png'))
            vec_im_siz[it_p]=pict1.shape[0]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        
        #### Plot deformed shape and edges:
        plt.plot(J_n[I_val],I_n[I_val],'.b',markersize=0.2)
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=1.5)
    
    ### Save figure:
    plt.axis([0,JM,Im,0])
    plt.axis('equal')
    plt.axis('off')
    plt.title(str(it_stp))
    plt.savefig('video/edge/'+'%03d'%(it_stp)+'.png',dpi=400)
    plt.close()

## Make movie:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/edge/%03d.png -qscale 1 video/edge/edge.avi')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Display displacement magnitude for individual particles:

## Loop over particles:
### Pick loop size:
if local_disp==0:
    n_plot=0
elif local_disp==1:
    n_plot=min(10,nb_part)
else :
    n_plot=nb_part

### Loop:
for it_p in range(n_plot):
    
    ### Display:
    print('Displacement plot - particle: '+'%03d'%it_p)
    
    ### Load data:
    #### Cell positions:
    I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
    J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
    #### Cell displacements:
    mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
    mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
    #### Outliers:
    mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
    
    ### Make folder to store pictures:
    os.system('mkdir tmp1')
    
    ### Loop over the frames:
    flag0=True
    for itImg in list(reversed(range(nb_img))): 
        
        #### Extract current displacements:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        
        #### Remove outliers:
        I_val=np.where(mat_outl[:,itImg]==0)[0]
        I_cur=I_cur[I_val]
        J_cur=J_cur[I_val]
        dI_cur=dI_cur[I_val]
        dJ_cur=dJ_cur[I_val]
        
        #### Get particle edges for plotting: 
        if flag0:
            #### Limit plot computation:
            jm=int(np.min(J0)*1.1); jM=int(np.max(J0)*1.1)
            im=int(np.min(I0)*1.1); iM=int(np.max(I0)*1.1)
        
        #### Extract fields:
        ##### Compute regular grid:
        vec_i=np.linspace(im,iM,int((iM-im)/dd_0*2)).astype('int')
        vec_j=np.linspace(jm,jM,int((jM-jm)/dd_0*2)).astype('int')
        grid_i,grid_j=np.meshgrid(vec_i,vec_j) 
        ##### Interpolate:
        grid_di=griddata(np.array([I_cur,J_cur]).T,dI_cur,(grid_i,grid_j),method='linear')
        grid_dj=griddata(np.array([I_cur,J_cur]).T,dJ_cur,(grid_i,grid_j),method='linear')
        
        #### Plot absolute displacement:
        grid_disp=np.sqrt(grid_dj*grid_dj+grid_di*grid_di)
        if flag0:
            vmax_disp=np.nanmax(np.abs(grid_disp))
            vmin_disp=0
            flag0=False
        
        plt.imshow(grid_disp,extent=(jm,jM,im,iM),vmin=vmin_disp,vmax=vmax_disp,cmap=plt.cm.jet)
        plt.colorbar()
        plt.axis('equal')
        plt.axis('off')
        plt.title('particle '+'%03d'%it_p+' '+'%03d'%itImg)
        plt.axis([jm,jM,jm,jM])
        plt.savefig('tmp1/'+'%04d'%(itImg)+'.png',dpi=200)
        plt.close()
    
    ### Make movies:
    null=os.system('ffmpeg -y -r 5 -f image2 -i tmp1/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/magnitude_displacement.avi')
    null=os.system('rm -rf tmp1')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Collective display for position and orientation evolution:

## Loading tracking data:
### For large particles (angle,i,j):
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Storage folder:
os.system('mkdir video/translateRotate')

## Load particle dimension vector:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Loop over the pictures: 
for it_stp in range(nb_img):
    
    ### Display:
    print('Orientation plot - step: '+str(it_stp))
    
    ### Load current positions and orientations:
    A_cur0=A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ### Prepare figure:
    fig=plt.figure()
    ax=fig.add_subplot(1, 1, 1)
    if it_stp==0:
        JM=np.max(J_cur0)+diam_lg/2.
        Im=-np.max(I_cur0)-diam_lg/2.
    
    plt.axis([0,JM,Im,0])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot position and orientation:
    for it_p in range(nb_part):
        if (IJSN[it_p,2]==1) and (IJSN[it_p,3]==1):
            ### Plot for large rigid particles:
            ax.add_patch(plt.Circle((J_cur0[it_p],-I_cur0[it_p]),diam_lg/2,color='b'))
            plt.plot([J_cur0[it_p]-diam_lg/4*m.cos(A_cur0[it_p]/180.*m.pi),J_cur0[it_p]+diam_lg/4*m.cos(A_cur0[it_p]/180.*m.pi)],
            [-I_cur0[it_p]-diam_lg/4*m.sin(A_cur0[it_p]/180.*m.pi),-I_cur0[it_p]+diam_lg/4*m.sin(A_cur0[it_p]/180.*m.pi)],'-w',linewidth=2)
        
        if (IJSN[it_p,2]==1) and (IJSN[it_p,3]==0):
            ### Plot for large soft particles:
            ax.add_patch(plt.Circle((J_cur0[it_p],-I_cur0[it_p]),diam_lg/2,color='c'))
            plt.plot([J_cur0[it_p]-diam_lg/4*m.cos(A_cur0[it_p]/180.*m.pi),J_cur0[it_p]+diam_lg/4*m.cos(A_cur0[it_p]/180.*m.pi)],
            [-I_cur0[it_p]-diam_lg/4*m.sin(A_cur0[it_p]/180.*m.pi),-I_cur0[it_p]+diam_lg/4*m.sin(A_cur0[it_p]/180.*m.pi)],'-w',linewidth=2)
        
        if (IJSN[it_p,2]==0) and (IJSN[it_p,3]==1):
            ### Plot for small rigid particles:
            ax.add_patch(plt.Circle((J_cur0[it_p],-I_cur0[it_p]),diam_sm/2,color='b'))
            plt.plot([J_cur0[it_p]-diam_sm/4*m.cos(A_cur0[it_p]/180.*m.pi),J_cur0[it_p]+diam_sm/4*m.cos(A_cur0[it_p]/180.*m.pi)],
            [-I_cur0[it_p]-diam_sm/4*m.sin(A_cur0[it_p]/180.*m.pi),-I_cur0[it_p]+diam_sm/4*m.sin(A_cur0[it_p]/180.*m.pi)],'-w',linewidth=2)
            
        if (IJSN[it_p,2]==0) and (IJSN[it_p,3]==0):
            ### Plot for small rigid particles:
            ax.add_patch(plt.Circle((J_cur0[it_p],-I_cur0[it_p]),diam_sm/2,color='c'))
            plt.plot([J_cur0[it_p]-diam_sm/4*m.cos(A_cur0[it_p]/180.*m.pi),J_cur0[it_p]+diam_sm/4*m.cos(A_cur0[it_p]/180.*m.pi)],
            [-I_cur0[it_p]-diam_sm/4*m.sin(A_cur0[it_p]/180.*m.pi),-I_cur0[it_p]+diam_sm/4*m.sin(A_cur0[it_p]/180.*m.pi)],'-w',linewidth=2)
    
    ### Save figure:
    plt.title(str(it_stp))
    plt.savefig('video/translateRotate/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()

## Make movie:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/translateRotate/%03d.png -qscale 1 video/translateRotate/translateRotate.avi')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Collective display for absolute displacement evolution:

## Storage folder:
os.system('mkdir video/absolute_displacement')

## Load data to plot:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Image size storage:
vec_im_siz=np.zeros(nb_part)

## Loop over the pictures:
for it_stp in list(reversed(range(nb_img))):
    
    ### Display:
    print('Displacement plot - step: '+str(it_stp))
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ### Prepare figure:
    plt.figure()
    if it_stp==nb_img-1:
        JM=np.max(J_p[:,0])+diam_lg/2.
        Jm=np.min(J_p[:,0])-diam_lg/2.
        Im=-np.max(I_p[:,0])-diam_lg/2.
        IM=-np.min(I_p[:,0])+diam_lg/2.
    
    ### Initialize storage vectors:
    #### For soft particles:
    II00_s=np.array([])
    JJ00_s=np.array([])
    DI00_s=np.array([])
    DJ00_s=np.array([])
    #### For rigid particles:
    II00_r=np.array([])
    JJ00_r=np.array([])
    DI00_r=np.array([])
    DJ00_r=np.array([])
    
    ### Loop over particles to collect information on cells:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements and outlier:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp] 
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        #### Remove outliers:
        I_val=np.where(mat_outl[:,it_stp]==0)[0]
        I_cur=I_cur[I_val]
        J_cur=J_cur[I_val]
        dI_cur=dI_cur[I_val]
        dJ_cur=dJ_cur[I_val]
        #### Fix position:
        ##### Get image size:
        if it_stp==nb_img-1:
            pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(it_stp).zfill(4)+'.png'))
            vec_im_siz[it_p]=pict1.shape[0]
        ##### Correction of the cell positions:
        I_cur=I_cur-vec_im_siz[it_p]/2
        J_cur=J_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Store data:
        if bool(IJSN[it_p,3]):
            II00_r=np.append(II00_r,I_n)
            JJ00_r=np.append(JJ00_r,J_n)
            DI00_r=np.append(DI00_r,dI_cur)
            DJ00_r=np.append(DJ00_r,dJ_cur)
        else:
            II00_s=np.append(II00_s,I_n)
            JJ00_s=np.append(JJ00_s,J_n)
            DI00_s=np.append(DI00_s,dI_cur)
            DJ00_s=np.append(DJ00_s,dJ_cur)
    
    # ~ #!!DEBUG!!
    # ~ plt.plot(II00_r,JJ00_r,'.r')
    # ~ plt.plot(II00_s,JJ00_s,'.b')
    # ~ plt.show()
    # ~ plt.close()
    
    ### Extract fields:
    #### Compute regular grid:
    if it_stp==nb_img-1:
        vec_i=np.linspace(-IM,-Im,int((-Im+IM)/(dd_0*disp_acc_mov))).astype('int')
        vec_j=np.linspace(Jm,JM,int((JM-Jm)/(dd_0*disp_acc_mov))).astype('int')
        grid_i,grid_j=np.meshgrid(vec_i,vec_j) 
        vec_i=grid_i.flatten()
        vec_j=grid_j.flatten()
    
    #### Get points where to interpolate:
    vec_in_r=np.float('nan')*np.ones(vec_i.shape)
    vec_in_s=np.float('nan')*np.ones(vec_i.shape)
    for it_p in range(nb_part):
        #### Load data:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt')
        edge=np.append(edge,edge[0]).astype('int')
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get pixel inside:
        if bool(IJSN[it_p,3]):
            vec_in_r[Path(np.vstack((I_n[edge],J_n[edge])).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
        else:
            vec_in_s[Path(np.vstack((I_n[edge],J_n[edge])).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
    
    #### Interpolate:
    if np.max(IJSN[:,3])==1:
        grid_in_r=vec_in_r.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_di_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,DI00_r,(grid_i,grid_j),method='linear')
        grid_dj_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,DJ00_r,(grid_i,grid_j),method='linear')
        grid_disp_r=np.sqrt(grid_dj_r*grid_dj_r+grid_di_r*grid_di_r)
    
    if np.min(IJSN[:,3])==0:
        grid_in_s=vec_in_s.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_di_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,DI00_s,(grid_i,grid_j),method='linear')
        grid_dj_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,DJ00_s,(grid_i,grid_j),method='linear')
        grid_disp_s=np.sqrt(grid_dj_s*grid_dj_s+grid_di_s*grid_di_s)
    
    ### Plot absolute displacement:
    if it_stp==nb_img-1:
        if np.max(IJSN[:,3])==1:
            disp_r_tmp=grid_disp_r.flatten()
            disp_r_tmp=np.sort(disp_r_tmp[np.where(~np.isnan(disp_r_tmp))[0]])
            max_disp_r=disp_r_tmp[int(0.99*len(disp_r_tmp))]
            min_disp_r=0.
        
        if np.min(IJSN[:,3])==0:
            disp_s_tmp=grid_disp_s.flatten()
            disp_s_tmp=np.sort(disp_s_tmp[np.where(~np.isnan(disp_s_tmp))[0]])
            max_disp_s=disp_s_tmp[int(0.99*len(disp_s_tmp))]
            min_disp_s=0.
    
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_disp_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_disp_r,vmax=max_disp_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_disp_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_disp_s,vmax=max_disp_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    plt.title(str(it_stp))
    plt.savefig('video/absolute_displacement/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()

## Make movie:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/absolute_displacement/%03d.png -qscale 1 video/absolute_displacement/absolute_displacement.avi')

## Make a fancy display for 2 frames:
for it_stp in list([nb_img-1,int(0.95*nb_img)-1]):
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ### Prepare figure:
    plt.figure()
    JM=np.max(J_cur0)+diam_lg/2.
    Jm=np.min(J_cur0)-diam_lg/2.
    Im=-np.max(I_cur0)-diam_lg/2.
    IM=-np.min(I_cur0)+diam_lg/2.
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Initialize storage vectors:
    #### For soft particles:
    II00_s=np.array([])
    JJ00_s=np.array([])
    DI00_s=np.array([])
    DJ00_s=np.array([])
    #### For rigid particles:
    II00_r=np.array([])
    JJ00_r=np.array([])
    DI00_r=np.array([])
    DJ00_r=np.array([])
    
    ### Loop over particles to collect information on cells:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements and outlier:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp] 
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        #### Remove outliers:
        I_val=np.where(mat_outl[:,it_stp]==0)[0]
        I_cur=I_cur[I_val]
        J_cur=J_cur[I_val]
        dI_cur=dI_cur[I_val]
        dJ_cur=dJ_cur[I_val]
        #### Fix position:
        ##### Correction of the cell positions:
        I_cur=I_cur-vec_im_siz[it_p]/2
        J_cur=J_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Store data:
        if bool(IJSN[it_p,3]):
            II00_r=np.append(II00_r,I_n)
            JJ00_r=np.append(JJ00_r,J_n)
            DI00_r=np.append(DI00_r,dI_cur)
            DJ00_r=np.append(DJ00_r,dJ_cur)
        else:
            II00_s=np.append(II00_s,I_n)
            JJ00_s=np.append(JJ00_s,J_n)
            DI00_s=np.append(DI00_s,dI_cur)
            DJ00_s=np.append(DJ00_s,dJ_cur)
    
    ### Extract fields:
    #### Compute regular grid:
    vec_i=np.linspace(-IM,-Im,int((-Im+IM)/(dd_0*disp_acc_fancy))).astype('int')
    vec_j=np.linspace(Jm,JM,int((JM-Jm)/(dd_0*disp_acc_fancy))).astype('int')
    grid_i,grid_j=np.meshgrid(vec_i,vec_j) 
    vec_i=grid_i.flatten()
    vec_j=grid_j.flatten()
    
    #### Get point where to interpolate:
    vec_in_r=np.float('nan')*np.ones(vec_i.shape)
    vec_in_s=np.float('nan')*np.ones(vec_i.shape)
    for it_p in range(nb_part):
        #### Load data:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt')
        edge=np.append(edge,edge[0]).astype('int')
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get pixel inside:
        if bool(IJSN[it_p,3]):
            vec_in_r[Path(np.vstack((I_n[edge],J_n[edge])).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
        else:
            vec_in_s[Path(np.vstack((I_n[edge],J_n[edge])).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
    
    #### Interpolate:
    if np.max(IJSN[:,3])==1:
        grid_in_r=vec_in_r.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_di_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,DI00_r,(grid_i,grid_j),method='linear')
        grid_dj_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,DJ00_r,(grid_i,grid_j),method='linear')
        grid_disp_r=np.sqrt(grid_dj_r*grid_dj_r+grid_di_r*grid_di_r)
    
    if np.min(IJSN[:,3])==0:
        grid_in_s=vec_in_s.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_di_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,DI00_s,(grid_i,grid_j),method='linear')
        grid_dj_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,DJ00_s,(grid_i,grid_j),method='linear')
        grid_disp_s=np.sqrt(grid_dj_s*grid_dj_s+grid_di_s*grid_di_s)
    
    ### Plot absolute displacement:
    if np.max(IJSN[:,3])==1:
        disp_r_tmp=grid_disp_r.flatten()
        disp_r_tmp=np.sort(disp_r_tmp[np.where(~np.isnan(disp_r_tmp))[0]])
        max_disp_r=disp_r_tmp[int(0.99*len(disp_r_tmp))]
        min_disp_r=0.
        plt.imshow(grid_disp_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_disp_r,vmax=max_disp_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        disp_s_tmp=grid_disp_s.flatten()
        disp_s_tmp=np.sort(disp_s_tmp[np.where(~np.isnan(disp_s_tmp))[0]])
        max_disp_s=disp_s_tmp[int(0.99*len(disp_s_tmp))]
        min_disp_s=0.
        plt.imshow(grid_disp_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_disp_s,vmax=max_disp_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/absolute_displacement/fancy_'+'%03d'%(it_stp)+'.png',dpi=300)
    plt.savefig('video/absolute_displacement/fancy_'+'%03d'%(it_stp)+'.svg')
    plt.close()
























