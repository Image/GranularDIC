# This code load the DIC and tracking results for each grain and display the result for each grain and the full panel results


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
from scipy import ndimage as nd
from scipy.interpolate import griddata
import math as m
import yaml
from matplotlib.path import Path
from PIL import Image

##Display one:
from matplotlib import pyplot as plt
import matplotlib.cm as cm 

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Particule diameter [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Correlation parameter:
### Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']

## Displaying parameter:
### Local scale displaying of results (0: none, 1: reduced, 2: full):
local_disp=data_input['displaying']['local displaying']
### Displaying accuracy for global movies:
disp_acc_mov=data_input['displaying']['field accuracy coarse']
### Displaying accuracy for fancy pictures:
disp_acc_fancy=data_input['displaying']['field accuracy thin']
### Grain scale displaying nature (I1|I2|eigen1_val|eigen2_val|eigen1_vec|eigen2_vec|VM, of C (soft) or E (rigid)):
local_nat_I1=data_input['displaying']['local field']['invariant 1']
local_nat_I2=data_input['displaying']['local field']['invariant 2']
local_nat_Eval1=data_input['displaying']['local field']['eigenvalue 1']
local_nat_Eval2=data_input['displaying']['local field']['eigenvalue 2']
local_nat_Evec1=data_input['displaying']['local field']['eigenvector 1']
local_nat_Evec2=data_input['displaying']['local field']['eigenvector 2']
local_nat_VM=data_input['displaying']['local field']['von Mises']


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Display I1, I2, eigen1, eigen2, VM of C (soft) or E (rigid) for individual particles:

## Load particle nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Loop over particles:
### Pick loop size:
if local_disp==0:
    n_plot=0
elif local_disp==1:
    n_plot=min(10,nb_part)
else :
    n_plot=nb_part
### Loop:
for it_p in range(n_plot):
    
    ### Display:
    print('particle: '+'%03d'%it_p)
    
    ### Make folder to store pictures:
    os.system('mkdir tmp1')
    os.system('mkdir tmp2')
    os.system('mkdir tmp3')
    os.system('mkdir tmp4')
    os.system('mkdir tmp5')
    os.system('mkdir tmp6')
    os.system('mkdir tmp7')
    
    ### Extract position grid in Lagrangian:
    grid_I=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_I.txt')
    grid_J=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_J.txt')
    
    ### Extract limits:
    jm=int(np.min(grid_J)*1.05); jM=int(np.max(grid_J)*1.05)
    im=int(np.min(grid_I)*1.05); iM=int(np.max(grid_I)*1.05)
    
    if bool(IJSN[it_p,3]):
        tensor='strain_E'
    else:
        tensor='right_Cauchy_Green_C'
    
    ### Loop over the frames:
    for itImg in list(reversed(range(nb_img))): 
        ####Load elasticity components:
        grid_EgV1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_eigenvalue_1.txt')
        grid_EgV2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_eigenvalue_2.txt')
        grid_EgVec1_J=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_eigenvector_1_J.txt')
        grid_EgVec1_I=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_eigenvector_1_I.txt')
        grid_EgVec2_J=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_eigenvector_2_J.txt')
        grid_EgVec2_I=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_eigenvector_2_I.txt')
        grid_I1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_invariant_1.txt')
        grid_I2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%itImg+'_invariant_2.txt')
        grid_VM=np.sqrt(grid_EgV1*grid_EgV1+grid_EgV2*grid_EgV2-grid_EgV1*grid_EgV2)
        
        #### Plot invariant 1:
        if local_nat_I1:
            if (itImg==nb_img-1):
                vmax_I1=np.nanmean(grid_I1)+3.*np.nanstd(grid_I1)
                vmin_I1=np.nanmean(grid_I1)-3.*np.nanstd(grid_I1)
            
            plt.imshow(grid_I1.T,extent=(jm,jM,im,iM),origin='lower',vmin=vmin_I1,vmax=vmax_I1,cmap=plt.cm.jet)
            plt.colorbar()
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp1/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
        
        #### Plot invariant 2:
        if local_nat_I2:
            if (itImg==nb_img-1):
                vmax_I2=np.nanmean(grid_I2)+3.*np.nanstd(grid_I2)
                vmin_I2=np.nanmean(grid_I2)-3.*np.nanstd(grid_I2)
            
            plt.imshow(grid_I2.T,extent=(jm,jM,im,iM),origin='lower',vmin=vmin_I2,vmax=vmax_I2,cmap=plt.cm.jet)
            plt.colorbar()
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp2/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
        
        #### Plot eigenvalue 1:
        if local_nat_Eval1:
            if (itImg==nb_img-1):
                vmax_EgV1=np.nanmean(grid_EgV1)+3.*np.nanstd(grid_EgV1)
                vmin_EgV1=np.nanmean(grid_EgV1)-3.*np.nanstd(grid_EgV1)
            
            plt.imshow(grid_EgV1.T,extent=(jm,jM,im,iM),origin='lower',vmin=vmin_EgV1,vmax=vmax_EgV1,cmap=plt.cm.jet)
            plt.colorbar()
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp3/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
        
        #### Plot eigenvalue 2:
        if local_nat_Eval2:
            if (itImg==nb_img-1):
                vmax_EgV2=np.nanmean(grid_EgV2)+3.*np.nanstd(grid_EgV2)
                vmin_EgV2=np.nanmean(grid_EgV2)-3.*np.nanstd(grid_EgV2)
            
            plt.imshow(grid_EgV2.T,extent=(jm,jM,im,iM),origin='lower',vmin=vmin_EgV2,vmax=vmax_EgV2,cmap=plt.cm.jet)
            plt.colorbar()
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp4/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
        
        #### Plot eigenvector 1:
        if local_nat_Evec1:
            plt.streamplot(grid_J[0,:],grid_I[:,0],grid_EgVec1_J,grid_EgVec1_I,linewidth=2,density=1.5,arrowstyle='->',arrowsize=4,color='k')
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp5/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
        
        #### Plot eigenvector 2:
        if local_nat_Evec2:
            plt.streamplot(grid_J[0,:],grid_I[:,0],grid_EgVec2_J,grid_EgVec2_I,linewidth=2,density=1.5,arrowstyle='->',arrowsize=4,color='k')
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp6/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
        
        #### Plot von Mises strain:
        if local_nat_VM:
            if (itImg==nb_img-1):
                vmax_VM=np.nanmean(grid_VM)+3.*np.nanstd(grid_VM)
                vmin_VM=np.nanmean(grid_VM)-3.*np.nanstd(grid_VM)
            
            plt.imshow(grid_VM.T,extent=(jm,jM,im,iM),origin='lower',vmin=vmin_VM,vmax=vmax_VM,cmap=plt.cm.jet)
            plt.colorbar()
            plt.axis('equal')
            plt.axis('off')
            plt.title('%03d'%it_p+' '+'%03d'%itImg)
            plt.axis([jm,jM,jm,jM])
            plt.savefig('tmp7/'+'%04d'%(itImg)+'.png',dpi=200)
            plt.close()
    
    ### Make movies:
    if local_nat_I1:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp1/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_invariant_1.avi')
    if local_nat_I2:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp2/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_invariant_2.avi')
    if local_nat_Eval1:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp3/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_eigenvalue_1.avi')
    if local_nat_Eval2:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp4/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_eigenvalue_2.avi')
    if local_nat_Evec1:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp5/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_eigenvector_1.avi')
    if local_nat_Evec2:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp6/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_eigenvector_2.avi')
    if local_nat_VM:
        null=os.system('ffmpeg -y -r 5 -f image2 -i tmp7/%04d.png -qscale 1 result/'+'%03d'%it_p+'/video/strain_vonMises.avi')
    null=os.system('rm -rf tmp1')
    null=os.system('rm -rf tmp2')
    null=os.system('rm -rf tmp3')
    null=os.system('rm -rf tmp4')
    null=os.system('rm -rf tmp5')
    null=os.system('rm -rf tmp6')
    null=os.system('rm -rf tmp7')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Collective display for the C (soft) or E (rigid) invariant and eigenvalue evolutions:

## Loading tracking data:
### For large particles (angle,i,j):
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Storage folder:
os.system('mkdir video/strain_invariant_1')
os.system('mkdir video/strain_invariant_2')
os.system('mkdir video/strain_eigenvalue_1')
os.system('mkdir video/strain_eigenvalue_2')
os.system('mkdir video/strain_vonMises')

## Load particle nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Get boundaries:
I_cur0=I_p[:,0]; J_cur0=J_p[:,0]
JM=np.max(J_cur0)+diam_lg/2.
Jm=np.min(J_cur0)-diam_lg/2.
Im=-np.max(I_cur0)-diam_lg/2.
IM=-np.min(I_cur0)+diam_lg/2.

## Image size storage:
vec_im_siz=np.zeros(nb_part)

## Loop over the pictures:   
for it_stp in list(reversed(range(nb_img))):
    
    ### Display:
    print('step:'+ str(it_stp))
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ## Prepare figure:
    plt.figure()
    
    ### Initialize storage vectors:
    #### For rigid particles:
    II00_r=np.array([])
    JJ00_r=np.array([])
    I100_r=np.array([])
    I200_r=np.array([])
    E100_r=np.array([])
    E200_r=np.array([])
    VM00_r=np.array([])
    #### For soft particles:
    II00_s=np.array([])
    JJ00_s=np.array([])
    I100_s=np.array([])
    I200_s=np.array([])
    E100_s=np.array([])
    E200_s=np.array([])
    VM00_s=np.array([])
    
    ### Loop over particles:
    for it_p in range(nb_part):
        ####Get particle nature:
        if bool(IJSN[it_p,3]):
            tensor='strain_E'
        else:
            tensor='right_Cauchy_Green_C'
        
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_I.txt').flatten()
        J0=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_J.txt').flatten()
        ##### Cell displacements:
        dI_cur=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_I.txt').flatten()
        dJ_cur=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_J.txt').flatten()
        ####Load elasticity components:
        vec_EgV1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_eigenvalue_1.txt').flatten()
        vec_EgV2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_eigenvalue_2.txt').flatten()
        vec_I1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_invariant_1.txt').flatten()
        vec_I2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_invariant_2.txt').flatten()
        vec_VM=np.sqrt(vec_EgV1*vec_EgV1+vec_EgV2*vec_EgV2-vec_EgV1*vec_EgV2)
        #### Get image size:
        if it_stp==nb_img-1:
            pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(it_stp).zfill(4)+'.png'))
            vec_im_siz[it_p]=pict1.shape[0]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        
        #### Store data:
        if bool(IJSN[it_p,3]):
            II00_r=np.append(II00_r,I_n)
            JJ00_r=np.append(JJ00_r,J_n)
            I100_r=np.append(I100_r,vec_I1)
            I200_r=np.append(I200_r,vec_I2)
            E100_r=np.append(E100_r,vec_EgV1)
            E200_r=np.append(E200_r,vec_EgV2)
            VM00_r=np.append(VM00_r,vec_VM)
        else:
            II00_s=np.append(II00_s,I_n)
            JJ00_s=np.append(JJ00_s,J_n)
            I100_s=np.append(I100_s,vec_I1)
            I200_s=np.append(I200_s,vec_I2)
            E100_s=np.append(E100_s,vec_EgV1)
            E200_s=np.append(E200_s,vec_EgV2)
            VM00_s=np.append(VM00_s,vec_VM)
    
    # ~ #!!Debug!!
    # ~ plt.plot(JJ00_r,II00_r,'.b')
    # ~ plt.plot(JJ00_s,II00_s,'.c')
    # ~ plt.axis('equal')
    # ~ plt.show()
    # ~ plt.close()
    
    ### Remove nan and peak maxima for plotting:
    if np.max(IJSN[:,3])==1:
        II=np.where(~np.isnan(II00_r)*~np.isnan(JJ00_r)*~np.isnan(I100_r)*~np.isnan(I200_r)*~np.isnan(E100_r)*~np.isnan(E200_r)*~np.isnan(VM00_r))
        II00_r=II00_r[II]
        JJ00_r=JJ00_r[II]
        I100_r=I100_r[II]
        I200_r=I200_r[II]
        E100_r=E100_r[II]
        E200_r=E200_r[II]
        VM00_r=VM00_r[II]
        if it_stp==nb_img-1:
            max_I1_r=np.nanmean(I100_r)+3.*np.nanstd(I100_r)
            min_I1_r=np.nanmean(I100_r)-3.*np.nanstd(I100_r)
            max_I2_r=np.nanmean(I200_r)+3.*np.nanstd(I200_r)
            min_I2_r=np.nanmean(I200_r)-3.*np.nanstd(I200_r)
            max_E1_r=np.nanmean(E100_r)+3.*np.nanstd(E100_r) 
            min_E1_r=np.nanmean(E100_r)-3.*np.nanstd(E100_r)
            max_E2_r=np.nanmean(E200_r)+3.*np.nanstd(E200_r)
            min_E2_r=np.nanmean(E200_r)-3.*np.nanstd(E200_r)
            max_VM_r=np.nanmean(VM00_r)+3.*np.nanstd(VM00_r)
            min_VM_r=np.nanmean(VM00_r)-3.*np.nanstd(VM00_r)
    
    if np.min(IJSN[:,3])==0:
        II=np.where(~np.isnan(II00_s)*~np.isnan(JJ00_s)*~np.isnan(I100_s)*~np.isnan(I200_s)*~np.isnan(E100_s)*~np.isnan(E200_s)*~np.isnan(VM00_s))
        II00_s=II00_s[II]
        JJ00_s=JJ00_s[II]
        I100_s=I100_s[II]
        I200_s=I200_s[II]
        E100_s=E100_s[II]
        E200_s=E200_s[II]
        VM00_s=VM00_s[II]
        if it_stp==nb_img-1:
            max_I1_s=np.nanmean(I100_s)+3.*np.nanstd(I100_s)
            min_I1_s=np.nanmean(I100_s)-3.*np.nanstd(I100_s)
            max_I2_s=np.nanmean(I200_s)+3.*np.nanstd(I200_s)
            min_I2_s=np.nanmean(I200_s)-3.*np.nanstd(I200_s)
            max_E1_s=np.nanmean(E100_s)+3.*np.nanstd(E100_s) 
            min_E1_s=np.nanmean(E100_s)-3.*np.nanstd(E100_s)
            max_E2_s=np.nanmean(E200_s)+3.*np.nanstd(E200_s)
            min_E2_s=np.nanmean(E200_s)-3.*np.nanstd(E200_s)
            max_VM_s=np.nanmean(VM00_s)+3.*np.nanstd(VM00_s)
            min_VM_s=np.nanmean(VM00_s)-3.*np.nanstd(VM00_s)
    
    ### Extract fields:
    #### Compute regular grid:
    if it_stp==nb_img-1:
        vec_i=np.linspace(-IM,-Im,int((-Im+IM)/(dd_0*disp_acc_mov))).astype('int')
        vec_j=np.linspace(Jm,JM,int((JM-Jm)/(dd_0*disp_acc_mov))).astype('int')
        grid_i,grid_j=np.meshgrid(vec_i,vec_j) 
        vec_i=grid_i.flatten()
        vec_j=grid_j.flatten()
    
    #### Get point where to interpolate:
    vec_in_r=np.float('nan')*np.ones(vec_i.shape)
    vec_in_s=np.float('nan')*np.ones(vec_i.shape)
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        ##### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Get pixel inside:
        if bool(IJSN[it_p,3]):
            vec_in_r[Path(np.vstack((I_cur_edg_imp,J_cur_edg_imp)).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
        else:
            vec_in_s[Path(np.vstack((I_cur_edg_imp,J_cur_edg_imp)).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
    
    #### Interpolate:
    if np.max(IJSN[:,3])==1:
        grid_in_r=vec_in_r.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_I1_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,I100_r,(grid_i,grid_j),method='linear')
        grid_I2_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,I200_r,(grid_i,grid_j),method='linear')
        grid_E1_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,E100_r,(grid_i,grid_j),method='linear')
        grid_E2_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,E200_r,(grid_i,grid_j),method='linear')
        grid_VM_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,VM00_r,(grid_i,grid_j),method='linear')
    
    if np.min(IJSN[:,3])==0:
        grid_in_s=vec_in_s.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_I1_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,I100_s,(grid_i,grid_j),method='linear')
        grid_I2_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,I200_s,(grid_i,grid_j),method='linear')
        grid_E1_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,E100_s,(grid_i,grid_j),method='linear')
        grid_E2_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,E200_s,(grid_i,grid_j),method='linear')
        grid_VM_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,VM00_s,(grid_i,grid_j),method='linear')
    
    ##//|\\##//|\\##//|\\##
    ### Plot I1:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_I1_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I1_r,vmax=max_I1_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_I1_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I1_s,vmax=max_I1_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_invariant_1/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot I2:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_I2_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I2_r,vmax=max_I2_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_I2_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I2_s,vmax=max_I2_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_invariant_2/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot E1:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_E1_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E1_r,vmax=max_E1_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_E1_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E1_s,vmax=max_E1_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_eigenvalue_1/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot E2:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_E2_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E2_r,vmax=max_E2_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_E2_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E2_s,vmax=max_E2_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_eigenvalue_2/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot VM:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_VM_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_VM_r,vmax=max_VM_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_VM_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_VM_s,vmax=max_VM_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_vonMises/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()

## Make movies:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/strain_invariant_1/%03d.png -qscale 1 video/strain_invariant_1/strain_invariant_1.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/strain_invariant_2/%03d.png -qscale 1 video/strain_invariant_2/strain_invariant_2.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/strain_eigenvalue_1/%03d.png -qscale 1 video/strain_eigenvalue_1/strain_eigenvalue_1.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/strain_eigenvalue_2/%03d.png -qscale 1 video/strain_eigenvalue_2/strain_eigenvalue_2.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/strain_vonMises/%03d.png -qscale 1 video/strain_vonMises/strain_vonMises.avi')


## Make a fancy display for 2 frames:
for it_stp in list([nb_img-1,int(0.95*nb_img)-1]):
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ## Prepare figure:
    plt.figure()
    
    ### Initialize storage vectors:
    #### For rigid particles:
    II00_r=np.array([])
    JJ00_r=np.array([])
    I100_r=np.array([])
    I200_r=np.array([])
    E100_r=np.array([])
    E200_r=np.array([])
    VM00_r=np.array([])
    #### For soft particles:
    II00_s=np.array([])
    JJ00_s=np.array([])
    I100_s=np.array([])
    I200_s=np.array([])
    E100_s=np.array([])
    E200_s=np.array([])
    VM00_s=np.array([])
    
    ### Loop over particles:
    for it_p in range(nb_part):
        ####Get particle nature:
        if bool(IJSN[it_p,3]):
            tensor='strain_E'
        else:
            tensor='right_Cauchy_Green_C'
        
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_I.txt').flatten()
        J0=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_J.txt').flatten()
        ##### Cell displacements:
        dI_cur=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_I.txt').flatten()
        dJ_cur=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_J.txt').flatten()
        ####Load elasticity components:
        vec_EgV1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_eigenvalue_1.txt').flatten()
        vec_EgV2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_eigenvalue_2.txt').flatten()
        vec_I1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_invariant_1.txt').flatten()
        vec_I2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_invariant_2.txt').flatten()
        vec_VM=np.sqrt(vec_EgV1*vec_EgV1+vec_EgV2*vec_EgV2-vec_EgV1*vec_EgV2)
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Store data:
        if bool(IJSN[it_p,3]):
            II00_r=np.append(II00_r,I_n)
            JJ00_r=np.append(JJ00_r,J_n)
            I100_r=np.append(I100_r,vec_I1)
            I200_r=np.append(I200_r,vec_I2)
            E100_r=np.append(E100_r,vec_EgV1)
            E200_r=np.append(E200_r,vec_EgV2)
            VM00_r=np.append(VM00_r,vec_VM)
        else:
            II00_s=np.append(II00_s,I_n)
            JJ00_s=np.append(JJ00_s,J_n)
            I100_s=np.append(I100_s,vec_I1)
            I200_s=np.append(I200_s,vec_I2)
            E100_s=np.append(E100_s,vec_EgV1)
            E200_s=np.append(E200_s,vec_EgV2)
            VM00_s=np.append(VM00_s,vec_VM)
    
    ### Remove nan:
    if np.max(IJSN[:,3])==1:
        II=np.where(~np.isnan(I100_r)*~np.isnan(I200_r)*~np.isnan(E100_r)*~np.isnan(E200_r)*~np.isnan(VM00_r))
        II00_r=II00_r[II]
        JJ00_r=JJ00_r[II]
        I100_r=I100_r[II]
        I200_r=I200_r[II]
        E100_r=E100_r[II]
        E200_r=E200_r[II]
        VM00_r=VM00_r[II]
    
    if np.min(IJSN[:,3])==0:
        II=np.where(~np.isnan(I100_s)*~np.isnan(I200_s)*~np.isnan(E100_s)*~np.isnan(E200_s)*~np.isnan(VM00_s))
        II00_s=II00_s[II]
        JJ00_s=JJ00_s[II]
        I100_s=I100_s[II]
        I200_s=I200_s[II]
        E100_s=E100_s[II]
        E200_s=E200_s[II]
        VM00_s=VM00_s[II]
    
    ### Extract fields:
    #### Compute regular grid:
    if it_stp==nb_img-1:
        vec_i=np.linspace(-IM,-Im,int((-Im+IM)/(dd_0*disp_acc_fancy))).astype('int')
        vec_j=np.linspace(Jm,JM,int((JM-Jm)/(dd_0*disp_acc_fancy))).astype('int')
        grid_i,grid_j=np.meshgrid(vec_i,vec_j) 
        vec_i=grid_i.flatten()
        vec_j=grid_j.flatten()
    
    #### Get point where to interpolate:
    vec_in_r=np.float('nan')*np.ones(vec_i.shape)
    vec_in_s=np.float('nan')*np.ones(vec_i.shape)
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        ##### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Get pixel inside:
        if bool(IJSN[it_p,3]):
            vec_in_r[Path(np.vstack((I_cur_edg_imp,J_cur_edg_imp)).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
        else:
            vec_in_s[Path(np.vstack((I_cur_edg_imp,J_cur_edg_imp)).T).contains_points(np.vstack((vec_i,vec_j)).T)]=1
    
    #### Interpolate:
    if np.max(IJSN[:,3])==1:
        grid_in_r=vec_in_r.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_I1_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,I100_r,(grid_i,grid_j),method='linear')
        grid_I2_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,I200_r,(grid_i,grid_j),method='linear')
        grid_E1_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,E100_r,(grid_i,grid_j),method='linear')
        grid_E2_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,E200_r,(grid_i,grid_j),method='linear')
        grid_VM_r=grid_in_r*griddata(np.array([II00_r,JJ00_r]).T,VM00_r,(grid_i,grid_j),method='linear')
    
    if np.min(IJSN[:,3])==0:
        grid_in_s=vec_in_s.reshape(grid_i.shape[0],grid_i.shape[1])
        grid_I1_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,I100_s,(grid_i,grid_j),method='linear')
        grid_I2_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,I200_s,(grid_i,grid_j),method='linear')
        grid_E1_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,E100_s,(grid_i,grid_j),method='linear')
        grid_E2_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,E200_s,(grid_i,grid_j),method='linear')
        grid_VM_s=grid_in_s*griddata(np.array([II00_s,JJ00_s]).T,VM00_s,(grid_i,grid_j),method='linear')
    
    ##//|\\##//|\\##//|\\##
    ### Plot I1:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_I1_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I1_r,vmax=max_I1_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_I1_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I1_s,vmax=max_I1_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Get improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        edge=np.append(edge,edge[0]).astype('int')
        I_cur_edg=I_n[edge]
        J_cur_edg=J_n[edge]
        ##### Convert in circular coordinates:
        I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
        r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
        a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        I_cur_edg_imp=I_c-r_cur_edg_smth*np.sin(a_cur_edg)+Im+IM
        J_cur_edg_imp=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
        #### Plot deformed shape:
        plt.plot(J_cur_edg_imp,I_cur_edg_imp,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_invariant_1/fancy_'+'%03d'%(it_stp)+'.png',dpi=300)
    plt.savefig('video/strain_invariant_1/fancy_'+'%03d'%(it_stp)+'.svg')    
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot I2:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_I2_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I2_r,vmax=max_I2_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_I2_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_I2_s,vmax=max_I2_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt')
        edge=np.append(edge,edge[0]).astype('int')
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Plot deformed shape:
        plt.plot(J_n[edge],I_n[edge]+Im+IM,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_invariant_2/fancy_'+'%03d'%(it_stp)+'.png',dpi=300)
    plt.savefig('video/strain_invariant_2/fancy_'+'%03d'%(it_stp)+'.svg')  
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot E1:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_E1_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E1_r,vmax=max_E1_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_E1_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E1_s,vmax=max_E1_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt')
        edge=np.append(edge,edge[0]).astype('int')
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Plot deformed shape:
        plt.plot(J_n[edge],I_n[edge]+Im+IM,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_eigenvalue_1/fancy_'+'%03d'%(it_stp)+'.png',dpi=300)
    plt.savefig('video/strain_eigenvalue_1/fancy_'+'%03d'%(it_stp)+'.svg')  
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot E2:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_E2_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E2_r,vmax=max_E2_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_E2_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_E2_s,vmax=max_E2_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt')
        edge=np.append(edge,edge[0]).astype('int')
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Plot deformed shape:
        plt.plot(J_n[edge],I_n[edge]+Im+IM,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_eigenvalue_2/fancy_'+'%03d'%(it_stp)+'.png',dpi=300)
    plt.savefig('video/strain_eigenvalue_2/fancy_'+'%03d'%(it_stp)+'.svg') 
    plt.close()
    
    ##//|\\##//|\\##//|\\##
    ### Plot VM:
    if np.max(IJSN[:,3])==1:
        plt.imshow(grid_VM_r.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_VM_r,vmax=max_VM_r,cmap=plt.cm.cool)
        plt.colorbar()
    
    if np.min(IJSN[:,3])==0:
        plt.imshow(grid_VM_s.T,extent=(Jm,JM,Im,IM),origin='lower',vmin=min_VM_s,vmax=max_VM_s,cmap=plt.cm.hot)
        plt.colorbar()
    
    plt.axis([Jm,JM,Im,IM])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot edge for particles:
    for it_p in range(nb_part):
        #### Load data:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt')
        edge=np.append(edge,edge[0]).astype('int')
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        #### Plot deformed shape:
        plt.plot(J_n[edge],I_n[edge]+Im+IM,'-k',linewidth=2)
    
    ### Save figure:
    plt.savefig('video/strain_vonMises/fancy_'+'%03d'%(it_stp)+'.png',dpi=300)
    plt.savefig('video/strain_vonMises/fancy_'+'%03d'%(it_stp)+'.svg') 
    plt.close()


