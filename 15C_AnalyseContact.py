# This code permits to analyse contacts


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import math as m
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Global inputs:
### Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

### Numbering:
#### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
#### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

### Particule diameter [px]:
#### Large:
diam_lg=data_input['general']['particle diameter']['large']

## Local inputs:
### Colormap for plotting:
cm=plt.cm.get_cmap('jet')
### Binarisation number for PDF:
n_bin=10
### Packing fraction binarisation:
t_bin=10


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Function to compute linear pdfs:

def hist_pdf_lin(Q,X):
    P=np.zeros(len(X)-1)
    rP=np.zeros(len(X)-1)
    Q_len=float(len(Q))
    for it in range(len(X)-1):
        I=np.where((X[it]<=Q)*(Q<X[it+1]))[0]
        P[it]=float(len(I))/Q_len/(X[it+1]-X[it])
        rP[it]=np.std(Q[I])/m.sqrt(len(I))/(X[it+1]-X[it])/(X[it+1]-X[it])
    
    X=0.5*(X[0:len(X)-1]+X[1:len(X)])
    return [X,P,rP]


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Contact length PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

## Loop over packing fractions:
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<=vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        if os.path.isfile('result/contact/ctt_stp_'+str(it_i).zfill(3)+'.txt'): 
            Q=np.loadtxt('result/contact/ctt_stp_'+str(it_i).zfill(3)+'.txt')
            if Q.ndim>1:
                lst_Q.append(Q[:,4])
            else:
                lst_Q.append(Q[4])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    ### Scale with large particle perimeter:
    vec_Q/=m.pi*diam_lg
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.xlim(0.,0.2)
# ~ plt.ylim(0.,42.)
plt.xlabel('contact length / large particle perimeter')
plt.ylabel('PDF')
plt.savefig('figure/PDFcontactLength.png')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Contact direction PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

## Loop over packing fractions:
fig=plt.figure()
ax=fig.add_subplot(111, polar=True)
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    lst_ctt1=[]
    lst_ctt2=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<=vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        if os.path.isfile('result/contact/ctt_stp_'+str(it_i).zfill(3)+'.txt'): 
            Q=np.loadtxt('result/contact/ctt_stp_'+str(it_i).zfill(3)+'.txt')
            if Q.ndim>1:
                lst_Q.append(Q[:,5])
                lst_ctt1.append(Q[:,0])
                lst_ctt2.append(Q[:,1])
            else:
                lst_Q.append(Q[5])
                lst_ctt1.append(Q[0])
                lst_ctt2.append(Q[1])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    vec_ctt1=np.hstack(lst_ctt2)
    vec_ctt2=np.hstack(lst_ctt1)
    ### Remove edge contacts:
    I=np.where((vec_ctt1>=0)*(vec_ctt2>=0))[0]
    vec_Q=vec_Q[I]
    ### Convert in degrees:
    vec_Q=vec_Q/m.pi*180.
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),int(1.5*n_bin)))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Period:
    X=np.hstack((X,180+X))/180.*m.pi; X=np.append(X,X[0])
    Y=np.hstack((P,P)); Y=np.append(Y,Y[0])
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    ax.plot(X,Y,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.savefig('figure/PDFcontactDirection.png')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Coordination PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

### Loop over particle:
mat_z=np.zeros((nb_part,nb_img))
for it_p in range(nb_part):
    mat_z[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/coordination.txt')

## Loop over packing fractions:
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        lst_Q.append(mat_z[:,it_i])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),int(n_bin)))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
# ~ plt.xlim(0.,9.)
# ~ plt.ylim(0.,0.6)
plt.xlabel('coordination')
plt.ylabel('PDF')
plt.savefig('figure/PDFcoordination.png')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Specific contact length PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

### Loop over particle:
mat_scl=np.zeros((nb_part,nb_img))
for it_p in range(nb_part):
    mat_scl[it_p,:]=np.loadtxt('result/'+str(it_p).zfill(3)+'/contact_length.txt')/np.loadtxt('result/'+str(it_p).zfill(3)+'/perimeter.txt')

## Loop over packing fractions:
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        lst_Q.append(mat_scl[:,it_i])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),int(n_bin)))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
# ~ plt.xlim(0.,1.)
# ~ plt.ylim(0.,7.)
plt.xlabel('specific contact length')
plt.ylabel('PDF')
plt.savefig('figure/PDFspecificContactLength.png')
plt.show()
plt.close()






