
#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import math as m
import yaml

##Display one:
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
import seaborn as sns

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Global inputs:
### Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

### Numbering:
#### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
#### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

### Particule diameter [px]:
#### Large:
diam_lg=data_input['general']['particle diameter']['large']
#### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Local inputs:
### Colormap for plotting:
cm=plt.cm.get_cmap('jet')
### Binarisation number for PDF:
n_bin=100
### Packing fraction binarisation:
t_bin=60

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Function to compute linear pdfs:

def hist_pdf_lin(Q,X):
    P=np.zeros(len(X)-1)
    rP=np.zeros(len(X)-1)
    Q_len=float(len(Q))
    for it in range(len(X)-1):
        I=np.where((X[it]<=Q)*(Q<X[it+1]))[0]
        P[it]=float(len(I))/Q_len/(X[it+1]-X[it])
        rP[it]=np.std(Q[I])/m.sqrt(len(I))/(X[it+1]-X[it])/(X[it+1]-X[it])
    
    X=0.5*(X[0:len(X)-1]+X[1:len(X)])
    return [X,P,rP]


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Von Mises strain PDF:

## Load particule nature vector:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make storage folder:
os.system('mkdir figure')

#------| For soft particles:
if np.min(IJSN[:,3])==0:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==0:
                    vec_e1=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_eigenvalue_1.txt')
                    vec_e2=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_eigenvalue_2.txt')
                    vec_cur=np.sqrt(vec_e1*vec_e1+vec_e2*vec_e2-vec_e1*vec_e2)
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(0.7<vec_Q)*(vec_Q<1.15)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(0.7,1.15)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFvmStrain_soft.png')
    plt.show()
    plt.close()

#------| For rigid particles:
if np.max(IJSN[:,3])==1:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==1:
                    vec_e1=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_eigenvalue_1.txt')
                    vec_e2=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_eigenvalue_2.txt')
                    vec_cur=np.sqrt(vec_e1*vec_e1+vec_e2*vec_e2-vec_e1*vec_e2)
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(0.<vec_Q)*(vec_Q<0.03)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(0.,0.03)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFvmStrain_rigid.png')
    plt.show()
    plt.close()



#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# First strain invariant PDF:

#------| For soft particles:
if np.min(IJSN[:,3])==0:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==0:
                    vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_invariant_1.txt')
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(1.2<vec_Q)*(vec_Q<2.8)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(1.2,2.8)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFstrainI1_soft.png')
    plt.show()
    plt.close()

#------| For rigid particles:
if np.max(IJSN[:,3])==1:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==1:
                    vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_invariant_1.txt')
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(-0.1<vec_Q)*(vec_Q<0.1)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(-0.1,0.1)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFstrainI1_rigid.png')
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Second strain invariant PDF:

#------| For soft particles:
if np.min(IJSN[:,3])==0:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==0:
                    vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_invariant_2.txt')
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(0.75<vec_Q)*(vec_Q<1.25)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(0.75,1.25)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFstrainI2_soft.png')
    plt.show()
    plt.close()

#------| For rigid particles:
if np.max(IJSN[:,3])==1:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==1:
                    vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_invariant_2.txt')
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(-0.0025<vec_Q)*(vec_Q<0.0025)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(-0.0025,0.0025)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFstrainI2_rigid.png')
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Principal eigenvalue PDF:

#------| For soft particles:
if np.min(IJSN[:,3])==0:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==0:
                    vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_eigenvalue_1.txt')
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(0.9<vec_Q)*(vec_Q<1.3)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(0.9,1.3)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFeigenVal1_soft.png')
    plt.show()
    plt.close()

#------| For rigid particles:
if np.max(IJSN[:,3])==1:
    ## Make packing fraction discretisation vector: 
    vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
    
    ## Loop over packing fractions:
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            #### Loop over particles:
            for it_p in range(nb_part):
                if IJSN[it_p,3]==1:
                    vec_cur=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_eigenvalue_2.txt')
                    vec_cur=np.reshape(vec_cur,(1,vec_cur.size))
                    vec_cur=vec_cur[~np.isnan(vec_cur)]
                    lst_Q.append(vec_cur)
        
        ### Convert into array:
        vec_Q=np.hstack(lst_Q)
        ### Remove remaining outlier:
        vec_Q=vec_Q[(-0.03<vec_Q)*(vec_Q<0.01)]
        ### Compute the PDF:
        X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
        ### Clean PDF:
        I=np.where(~np.isnan(P)*(P>1e-6))[0]; X=X[I]; P=P[I]; rP=rP[I]
        ### Plot PDF:
        flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
        plt.plot(X,P,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.xscale('linear')
    plt.yscale('log')
    plt.xlim(-0.03,0.01)
    #~ plt.ylim(0.7,42.)
    plt.xlabel('VM strain')
    plt.ylabel('PDF')
    plt.savefig('figure/PDFeigenVal1_rigid.png')
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Principal eigenvector display:

## Loading tracking data:
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Storage folder:
os.system('mkdir video')
os.system('mkdir video/principalDirection')

## Loop over the pictures:
for it_stp in range(nb_img):
    
    ### Load current positions and orientations:
    A_cur0=-A_p[:,it_stp]; I_cur0=I_p[:,it_stp]; J_cur0=J_p[:,it_stp]
    
    ### Prepare figure:
    plt.figure()
    if it_stp==0:
        JM=np.max(J_cur0)+diam_lg/2.
        Im=-np.max(I_cur0)-diam_lg/2.
    
    plt.axis([0,JM,Im,0])
    plt.axis('equal')
    plt.axis('off')
    
    ### Plot for particles:
    for it_p in range(nb_part):
        #### Load principal eigen vector, coordinate and displacement:
        if IJSN[it_p,3]==0:
            mat_Vi=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_eigenvector_1_J.txt')
            mat_Vj=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_stp).zfill(3)+'_eigenvector_1_I.txt')
        else:
            mat_Vi=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_eigenvector_1_J.txt')
            mat_Vj=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_stp).zfill(3)+'_eigenvector_1_I.txt')
        
        mat_gridI=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_I.txt')
        mat_gridJ=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_J.txt')
        mat_gridDI=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_I.txt')
        mat_gridDJ=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_J.txt')
        #### Remove nan:
        vec_Vi=np.reshape(mat_Vi,(1,mat_Vi.size)) 
        vec_Vj=np.reshape(mat_Vj,(1,mat_Vj.size)) 
        vec_I=np.reshape(mat_gridI,(1,mat_gridI.size))
        vec_J=np.reshape(mat_gridJ,(1,mat_gridJ.size))
        vec_DI=np.reshape(mat_gridDI,(1,mat_gridDI.size))
        vec_DJ=np.reshape(mat_gridDJ,(1,mat_gridDJ.size))
        vec_mask=~np.isnan(vec_Vi)
        vec_Vi=vec_Vi[vec_mask]
        vec_Vj=vec_Vj[vec_mask]
        vec_I=vec_I[vec_mask]
        vec_J=vec_J[vec_mask]
        vec_DI=vec_DI[vec_mask]
        vec_DJ=vec_DJ[vec_mask]
        #### Compute angle and rotate:
        II=np.where(-vec_Vi<0)[0]
        vec_Vi[II]=-vec_Vi[II]
        vec_Vj[II]=-vec_Vj[II]
        vec_a0=np.unwrap(np.arctan2(vec_Vj,-vec_Vi)+A_cur0[it_p]/180.*m.pi)
        
        # !Debug!
        #~ plt.plot(vec_a0,'o'); plt.show(); plt.close()
        #~ plt.plot(-vec_Vi,vec_Vj,'o'); plt.plot([0,np.sum(-vec_Vi)/len(vec_Vi)],[0,np.sum(vec_Vj)/len(vec_Vj)],'-r'); plt.show(); plt.close()
        
        a0=m.atan2(np.sum(np.sin(vec_a0)),np.sum(np.cos(vec_a0)))
        #### Compute coordinates:
        vec_I=vec_I+vec_DI
        vec_J=vec_J+vec_DJ
        vec_J_n=m.cos(A_cur0[it_p]/180.*m.pi)*vec_J+m.sin(A_cur0[it_p]/180.*m.pi)*vec_I
        vec_I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*vec_J+m.cos(A_cur0[it_p]/180.*m.pi)*vec_I
        vec_J_n=vec_J_n+J_cur0[it_p]
        vec_I_n=vec_I_n+I_cur0[it_p]
        #### Plot deformed shape and angle:
        sc=plt.scatter(vec_J_n,-vec_I_n,c=vec_a0,s=5,marker='s', edgecolors='none',cmap=ListedColormap(sns.color_palette("husl",100)),vmin=-m.pi/2.,vmax=m.pi/2.)
        plt.plot(X_edg,Y_edg,'-k',linewidth=2)
        plt.plot(np.mean(J_n)+0.25*diam_sm*m.cos(a0)*np.array([-1.,1.]),np.mean(-I_n)+0.25*diam_sm*m.sin(a0)*np.array([-1.,1.]),'-w',linewidth=3)
    
    ### Set axis:
    if (it_stp==0):
        I_m=np.min(I_cur0)-diam_sm
        I_M=np.max(I_cur0)+diam_sm
        J_m=np.min(J_cur0)-diam_sm
        J_M=np.max(J_cur0)+diam_sm
    
    plt.axis([J_m,J_M,-I_M,-I_m])
    plt.colorbar(sc)
    
    ### Save figure:
    plt.savefig('video/principalDirection/'+str(it_stp).zfill(3)+'.png',dpi=400)
    plt.close()

## Make movie:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/principalDirection/%03d.png -qscale 1 video/principalDirection/principalDirection.avi')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Principal eigenvector PDF:

## Loading tracking data:
A_p=np.loadtxt('particlePicture/A_stp.txt')

vec_Val_th=1.

#------| For soft particles:
if np.min(IJSN[:,3])==0:
    ## Loop over packing fractions:
    fig=plt.figure()
    ax=fig.add_subplot(111, polar=True)
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            ### Load current orientations:
            A_cur0=-A_p[:,it_i]
            ### Loop over large particles:
            for it_p in range(nb_part):
                #### Load data:
                if IJSN[it_p,3]==0:
                    mat_Vi=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_eigenvector_1_J.txt')
                    mat_Vj=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_eigenvector_1_I.txt')
                    mat_Val=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/right_Cauchy_Green_C/'+str(it_i).zfill(3)+'_eigenvalue_1.txt')
                
                #### Remove nan:
                vec_Vi=np.reshape(mat_Vi,(1,mat_Vi.size)) 
                vec_Vj=np.reshape(mat_Vj,(1,mat_Vj.size)) 
                vec_Val=np.reshape(mat_Val,(1,mat_Val.size)) 
                vec_mask=~np.isnan(vec_Vi)
                vec_Vi=vec_Vi[vec_mask]
                vec_Vj=vec_Vj[vec_mask]
                vec_Val=vec_Val[vec_mask]
                II=np.where(vec_Val>vec_Val_th)[0]
                vec_Vi=vec_Vi[II]
                vec_Vj=vec_Vj[II]
                vec_Val=vec_Val[II]
                if len(vec_Vi)>0:
                    #### Compute angle and rotate:
                    II=np.where(-vec_Vi<0)[0]
                    vec_Vi[II]=-vec_Vi[II]
                    vec_Vj[II]=-vec_Vj[II]
                    lst_Q.append(np.unwrap(np.arctan2(vec_Vj,-vec_Vi)+A_cur0[it_p]/180.*m.pi))
            
            if len(lst_Q)>0:
                ### Convert into array:
                vec_Q=np.hstack(lst_Q)
                vec_Q=vec_Q/m.pi*180.
                ### Reduce to +-90:
                II=np.where(vec_Q<-90.)[0]
                vec_Q[II]=vec_Q[II]+180.
                II=np.where(vec_Q>90.)[0]
                vec_Q[II]=vec_Q[II]-180.
                
                ### Compute the PDF:
                X,P,rP=hist_pdf_lin(vec_Q,np.linspace(-90,90,int(0.7*n_bin)))
                ### Period:
                X=np.hstack((X,180+X))/180.*m.pi; X=np.append(X,X[0])
                Y=np.hstack((P,P)); Y=np.append(Y,Y[0])
                ### Plot PDF:
                flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
                ax.plot(X,Y,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.savefig('figure/PDFeigenDirection_soft.png')
    plt.show()
    plt.close()

#------| For rigid particles:
if np.max(IJSN[:,3])==1:
    ## Loop over packing fractions:
    fig=plt.figure()
    ax=fig.add_subplot(111, polar=True)
    for it_phi in range(len(vec_pf)-1):
        ### Initialize data list:
        lst_Q=[]
        ### Current steps:
        I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
        ### Loop over current steps to collect data:
        for it_i in I_cur:
            ### Load current orientations:
            A_cur0=-A_p[:,it_i]
            ### Loop over large particles:
            for it_p in range(nb_part):
                #### Load data:
                if IJSN[it_p,3]==1:
                    mat_Vi=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_eigenvector_1_J.txt')
                    mat_Vj=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_eigenvector_1_I.txt')
                    mat_Val=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/strain_E/'+str(it_i).zfill(3)+'_eigenvalue_1.txt')
                
                #### Remove nan:
                vec_Vi=np.reshape(mat_Vi,(1,mat_Vi.size)) 
                vec_Vj=np.reshape(mat_Vj,(1,mat_Vj.size)) 
                vec_Val=np.reshape(mat_Val,(1,mat_Val.size)) 
                vec_mask=~np.isnan(vec_Vi)
                vec_Vi=vec_Vi[vec_mask]
                vec_Vj=vec_Vj[vec_mask]
                vec_Val=vec_Val[vec_mask]
                II=np.where(vec_Val>vec_Val_th)[0]
                vec_Vi=vec_Vi[II]
                vec_Vj=vec_Vj[II]
                vec_Val=vec_Val[II]
                if len(vec_Vi)>0:
                    #### Compute angle and rotate:
                    II=np.where(-vec_Vi<0)[0]
                    vec_Vi[II]=-vec_Vi[II]
                    vec_Vj[II]=-vec_Vj[II]
                    lst_Q.append(np.unwrap(np.arctan2(vec_Vj,-vec_Vi)+A_cur0[it_p]/180.*m.pi))
            
            if len(lst_Q)>0:
                ### Convert into array:
                vec_Q=np.hstack(lst_Q)
                vec_Q=vec_Q/m.pi*180.
                ### Reduce to +-90:
                II=np.where(vec_Q<-90.)[0]
                vec_Q[II]=vec_Q[II]+180.
                II=np.where(vec_Q>90.)[0]
                vec_Q[II]=vec_Q[II]-180.
                
                ### Compute the PDF:
                X,P,rP=hist_pdf_lin(vec_Q,np.linspace(-90,90,int(0.7*n_bin)))
                ### Period:
                X=np.hstack((X,180+X))/180.*m.pi; X=np.append(X,X[0])
                Y=np.hstack((P,P)); Y=np.append(Y,Y[0])
                ### Plot PDF:
                flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
                ax.plot(X,Y,'-',color=cm(flt_col),linewidth=2)
    
    sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
    plt.colorbar(sc)
    plt.savefig('figure/PDFeigenDirection_rigid.png')
    plt.show()
    plt.close()






