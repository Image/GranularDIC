# This code load the DIC results for each grain and compute the different fields on a regular thin grid


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
from scipy.interpolate import griddata
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

##Correlation parameter:
### Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Interpolate data:

## Loop over particles:
for it_p in range(nb_part):
    
    ### Display:
    print('particle: '+'%03d'%it_p)
    
    ### Load data:
    #### Cell positions:
    I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
    J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
    #### Cell displacements and correlation:
    mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
    mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
    mat_corr0=np.loadtxt('result/'+'%03d'%it_p+'/correlation.txt')
    #### Outlier matrix:
    mat_outl=np.loadtxt('result/'+'%03d'%it_p+'/outlier.txt')
    
    ### Coordinate vector:
    Iu0=np.sort(np.unique(I0))
    Ju0=np.sort(np.unique(I0))
    
    ### Make regular grid for coordinates:
    vec_i=range(int(1.1*Iu0[0]),int(1.1*Iu0[-1]),int(dd_0/2.))
    vec_j=range(int(1.1*Ju0[0]),int(1.1*Ju0[-1]),int(dd_0/2.))
    grid_j,grid_i=np.meshgrid(vec_j,vec_i)
    
    ### Make storage folder and save coordinates grids:
    os.system('mkdir result/'+'%03d'%it_p+'/interpolated')
    os.system('mkdir result/'+'%03d'%it_p+'/interpolated/displacement')
    np.savetxt('result/'+'%03d'%it_p+'/interpolated/grid_I.txt',grid_i)
    np.savetxt('result/'+'%03d'%it_p+'/interpolated/grid_J.txt',grid_j)
    
    
    ### Loop over the frames:
    for itImg in list(reversed(range(nb_img))): 
        
        #### Index of valid cells:
        I_val=np.where(mat_outl[:,itImg]==0)[0]
        
        #### Extract current displacements:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        corr_cur=mat_corr0[:,itImg]
        
        #### Extract fields by interpolation:
        grid_di=griddata(np.array([I0[I_val],J0[I_val]]).T,dI_cur[I_val],(grid_i,grid_j),method='linear')
        grid_dj=griddata(np.array([I0[I_val],J0[I_val]]).T,dJ_cur[I_val],(grid_i,grid_j),method='linear')
        grid_corr=griddata(np.array([I0[I_val],J0[I_val]]).T,corr_cur[I_val],(grid_i,grid_j),method='linear')
        
        #### Save grids:
        np.savetxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%itImg+'_grid_displacement_I.txt',grid_di)
        np.savetxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%itImg+'_grid_displacement_J.txt',grid_dj)
        np.savetxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%itImg+'_grid_correlation.txt',grid_corr)

