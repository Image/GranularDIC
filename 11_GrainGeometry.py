# This code load the DIC and tracking results for each grain  and compute observable related to the grain scale deformations:
# main orientations - anisotropy - asphericity - inertia matix and eigen - area - perimeter 


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
from matplotlib.path import Path
import yaml
import math as m

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Particule geometry [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']
### Small:
diam_sm=data_input['general']['particle diameter']['small']
## Height:
height_part=data_input['general']['particle height']

## Correlation parameter:
### Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Parameters for picture handling
### Image reduction factor for edge detection:
ImRat=data_input['picture handling']['medium reduction ratio']

## Parameter for observable computing:
### volume discretisation of matrix of inertia [px]:
dxyz=data_input['analysis']['discrete volume']

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Computation of the inertia matrix, particle area and perimeter:

## Loading tracking data:
A_p=np.loadtxt('particlePicture/A_stp.txt')
I_p=np.loadtxt('particlePicture/I_stp.txt')
J_p=np.loadtxt('particlePicture/J_stp.txt')

## Loop over particles:
for it_p in range(nb_part):
    
    ### Display:
    print('particle: '+'%03d'%it_p)
    
    ### Load data:
    I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
    J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
    
    ### Cell displacements:
    mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
    mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
    
    ### Make storage folders:
    os.system('mkdir result/'+'%03d'%it_p+'/inertia')
    
    ### Make storage variables:
    vec_perimeter=np.zeros(nb_img)
    vec_area=np.zeros(nb_img)
    vec_asphericity=np.zeros(nb_img)
    vec_anisotropy=np.zeros(nb_img)
    vec_major_direction=np.zeros(nb_img)
    vec_minor_direction=np.zeros(nb_img)
    
    ### Loop over the frames:
    for itImg in range(nb_img):
        
        ### Load current positions and orientations:
        A_cur0=-A_p[it_p,itImg]; I_cur0=I_p[it_p,itImg]; J_cur0=J_p[it_p,itImg]
        
        #### Extract current displacements:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        
        #### Correction of the cell positions:
        I_cur=I0+dI_cur
        J_cur=J0+dJ_cur
        
        #### Rotation of the cells:
        J_n=m.cos(A_cur0/180.*m.pi)*J_cur+m.sin(A_cur0/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0/180.*m.pi)*J_cur+m.cos(A_cur0/180.*m.pi)*I_cur
        
        #### Extract improved edges:
        ##### Get current edge positions:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(itImg).zfill(4)+'.txt').astype('int')
        X_dic=J_n; Y_dic=-I_n
        X_cur_edg=X_dic[edge]
        Y_cur_edg=Y_dic[edge]
        ##### Convert in circular coordinates:
        X_c=np.mean(X_cur_edg); Y_c = np.mean(Y_cur_edg)
        r_cur_edg=np.sqrt((X_c-X_cur_edg)**2.+(Y_c-Y_cur_edg)**2.)
        a_cur_edg=np.arctan2((Y_c-Y_cur_edg),(X_c-X_cur_edg))
        ##### Smooth edges and add cell thickness:
        nn=3
        r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
        r_cur_edg_smth[0]=r_cur_edg[0]
        r_cur_edg_smth[-1]=r_cur_edg[-1]
        r_cur_edg_smth+=dd_0/2
        ##### Compute back in Cartesian coordinates:
        X_edg=X_c-r_cur_edg_smth*np.cos(a_cur_edg)
        Y_edg=Y_c-r_cur_edg_smth*np.sin(a_cur_edg)
        
        #!DEBUG!
        # ~ plt.plot(X_dic,Y_dic,'.k')
        # ~ plt.plot(X_cur_edg,Y_cur_edg,'+-b')
        # ~ plt.plot(X_edg,Y_edg,'*-m')
        # ~ plt.axis('equal')
        # ~ plt.show()
        
        #### Computation of the inertia, perimeter, area and asphericity:
        ##### Make x-y-z mesh:
        x_v=np.linspace(np.min(X_edg),np.max(X_edg),int((np.max(X_edg)-np.min(X_edg))/float(dxyz)))
        y_v=np.linspace(np.min(Y_edg),np.max(Y_edg),int((np.max(Y_edg)-np.min(Y_edg))/float(dxyz)))
        z_v=np.linspace(0,height_part,int(height_part/float(dxyz)))
        xv,yv,zv=np.meshgrid(x_v,y_v,z_v,indexing='xy')
        xv=np.reshape(xv,(1,xv.shape[0]*xv.shape[1]*xv.shape[2]))[0]
        yv=np.reshape(yv,(1,yv.shape[0]*yv.shape[1]*yv.shape[2]))[0]
        zv=np.reshape(zv,(1,zv.shape[0]*zv.shape[1]*zv.shape[2]))[0]
        ##### Select points inside:
        path=Path(np.vstack((X_edg,Y_edg)).T)
        I_in=np.where(path.contains_points(np.vstack((xv,yv)).T)>0.5)[0]
        
        # !! Debug !!:
        #~ import matplotlib.pyplot as plt
        #~ from mpl_toolkits.mplot3d import Axes3D
        #~ fig=plt.figure()
        #~ ax=fig.add_subplot(111, projection='3d')
        #~ ax.scatter(xv[I_in],yv[I_in],zv[I_in],zdir='z', s=20, c=None, depthshade=True)
        #~ plt.show()
        #~ plt.close()
        
        ##### Computation of the area:
        Area=len(I_in)*float(dxyz)*float(dxyz)/int(height_part/dxyz)
        vec_area[itImg]=Area
        ##### Computation of the perimeter:
        Perimeter=np.sum(np.sqrt((X_edg[1:]-X_edg[0:-1])*(X_edg[1:]-X_edg[0:-1])+(Y_edg[1:]-Y_edg[0:-1])*(Y_edg[1:]-Y_edg[0:-1])))+m.sqrt((X_edg[0]-X_edg[-1])*(X_edg[0]-X_edg[-1])+(Y_edg[0]-Y_edg[-1])*(Y_edg[0]-Y_edg[-1]))
        vec_perimeter[itImg]=Perimeter
        ##### Computation of the asphericity:
        vec_asphericity[itImg]=Perimeter*Perimeter/(4*m.pi*Area)
        ##### Compute inertia matrix:
        Ixx=np.sum(yv[I_in]*yv[I_in]+zv[I_in]*zv[I_in])*float(dxyz)*dxyz*dxyz
        Iyy=np.sum(xv[I_in]*xv[I_in]+zv[I_in]*zv[I_in])*float(dxyz)*dxyz*dxyz
        Izz=np.sum(xv[I_in]*xv[I_in]+yv[I_in]*yv[I_in])*float(dxyz)*dxyz*dxyz
        Ixy=-np.sum(xv[I_in]*yv[I_in])*float(dxyz)*dxyz*dxyz
        Ixz=-np.sum(xv[I_in]*zv[I_in])*float(dxyz)*dxyz*dxyz
        Iyz=-np.sum(yv[I_in]*zv[I_in])*float(dxyz)*dxyz*dxyz
        I_mat=np.array([[Ixx,Ixy,Ixz],[Ixy,Iyy,Iyz],[Ixz,Iyz,Izz]])
        ##### Compute eigen values and vectors:
        eig_val,eig_vec=np.linalg.eig(I_mat)
        ##### Save it:
        np.savetxt('result/'+'%03d'%it_p+'/inertia/'+'%03d'%itImg+'_I_eigVal_eigVec.txt',np.column_stack((I_mat,eig_val,eig_vec)))
        
        #### Computation of the main orientation:
        ##### Make x-y mesh:
        dxy=dxyz/2.
        x_v=np.linspace(np.min(X_edg),np.max(X_edg),int((np.max(X_edg)-np.min(X_edg))/dxy))
        y_v=np.linspace(np.min(Y_edg),np.max(Y_edg),int((np.max(Y_edg)-np.min(Y_edg))/dxy))
        xv,yv=np.meshgrid(x_v,y_v,indexing='xy')
        xv=xv.flatten()
        yv=yv.flatten()
        ##### Select points inside:
        path=Path(np.vstack((X_edg,Y_edg)).T)
        I_in=np.where(path.contains_points(np.vstack((xv,yv)).T)>0.5)[0]
        xv=xv[I_in]; yv=yv[I_in]
        ##### Pick barycenter:
        x_g=np.mean(xv); y_g=np.mean(yv)
        ##### Convertion to circular coordinates:
        theta_pos=np.arctan2(yv-y_g,xv-x_g)
        rad_pos=np.sqrt((xv-x_g)*(xv-x_g)+(yv-y_g)*(yv-y_g))
        ##### Add periodicity:
        theta_pos=np.hstack((theta_pos-2.*m.pi,theta_pos,theta_pos+2.*m.pi))
        rad_pos=np.hstack((rad_pos,rad_pos,rad_pos))
        ##### Select major and minor directions and compute anisotropy:
        theta0=np.linspace(0,m.pi,100)
        max_diag=np.zeros(len(theta0))
        d_thet=0.05
        for it_thet in range(len(theta0)):
            I1=np.where((theta0[it_thet]-d_thet<=theta_pos)*(theta_pos<=theta0[it_thet]+d_thet))[0]
            I2=np.where((theta0[it_thet]-d_thet-m.pi<=theta_pos)*(theta_pos<=theta0[it_thet]-m.pi+d_thet))[0]
            max_diag[it_thet]=np.max(rad_pos[I1])+np.max(rad_pos[I2])
            
            # !! Debug !!:
            # ~ plt.plot(X_dic,Y_dic,'.k')
            # ~ plt.plot(X_edg,Y_edg,'-b')
            # ~ plt.plot(x_g+rad_pos[I1]*np.cos(theta_pos[I1]),y_g+rad_pos[I1]*np.sin(theta_pos[I1]),'.g')
            # ~ plt.plot(x_g+rad_pos[I2]*np.cos(theta_pos[I2]),y_g+rad_pos[I2]*np.sin(theta_pos[I2]),'.r')
            # ~ plt.plot(x_g,y_g,'.m')
            # ~ plt.axis('equal')
            # ~ plt.title(str(max_diag[it_thet]))
            # ~ plt.show()
            # ~ plt.close()
        
        # !! Debug !!:
        #~ plt.plot(theta0,max_diag,'-b')
        #~ plt.show()
        #~ plt.close()
        
        Im=np.where(max_diag==np.min(max_diag))[0][0]
        IM=np.where(max_diag==np.max(max_diag))[0][0]
        theta_m=theta0[Im]*180./m.pi
        theta_M=theta0[IM]*180./m.pi
        Anisotropy=np.min(max_diag)/np.max(max_diag)
        ##### Store data:
        vec_anisotropy[itImg]=Anisotropy
        vec_major_direction[itImg]=theta_M
        vec_minor_direction[itImg]=theta_m
    
    ### Save data:
    np.savetxt('result/'+'%03d'%it_p+'/perimeter.txt',vec_perimeter)
    np.savetxt('result/'+'%03d'%it_p+'/area.txt',vec_area)
    np.savetxt('result/'+'%03d'%it_p+'/asphericity.txt',vec_asphericity)
    np.savetxt('result/'+'%03d'%it_p+'/major_direction.txt',vec_major_direction)
    np.savetxt('result/'+'%03d'%it_p+'/minor_direction.txt',vec_minor_direction)
    np.savetxt('result/'+'%03d'%it_p+'/anisotropy.txt',vec_anisotropy)


















