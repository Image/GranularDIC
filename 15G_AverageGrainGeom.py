#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import yaml
import math as m
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from scipy.spatial import ConvexHull
import yaml
from PIL import Image

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Global inputs:
### Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

### Numbering:
#### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
#### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

### Particule diameter [px]:
#### Large:
diam_lg=data_input['general']['particle diameter']['large']
#### Small:
diam_sm=data_input['general']['particle diameter']['small']

## Local inputs:
### Colormap for plotting:
cm=plt.cm.get_cmap('jet')
### Packing fraction binarisation:
t_bin=50

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Grain average geometry:

## Make storage folders:
os.system('mkdir video')
os.system('mkdir video/averageGeom')

## Particle orientation:
A_p=np.loadtxt('particlePicture/A_stp.txt')

## Load particle nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Loop over packing fractions:
K_lg=[]
K_sm=[]

## Vector to store image size:
vec_im_siz=np.zeros(nb_part)

for it_stp in range(nb_img):
    ### Current steps:
    II_cur=np.arange(max(0,it_stp-2),min(it_stp+2,nb_img))
    ### Loop over current steps to collect data:
    Q_lg=[]; flag_lg=True
    Q_sm=[]; flag_sm=True
    for it_i in II_cur:
        #### Load current orientations:
        A_cur0=-A_p[:,it_i]
        #### Loop over particles:
        for it_p in range(nb_part):
            if IJSN[it_p,3]==0:
                #### Load data:
                ##### Cell positions:
                I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
                J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
                ##### Cell displacements:
                mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
                mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
                dI_cur=mat_dI0[:,it_stp]
                dJ_cur=mat_dJ0[:,it_stp]
                #### Get image size:
                if it_stp==0:
                    pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(it_stp).zfill(4)+'.png'))
                    vec_im_siz[it_p]=pict1.shape[0]
                
                #### Correction of the cell positions:
                I_cur=I0+dI_cur-vec_im_siz[it_p]/2.
                J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
                #### Rotation of the cells:
                J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
                I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
                
                #### Get improved edges:
                ##### Get current edge positions:
                edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
                I_cur_edg=I_n[edge]
                J_cur_edg=J_n[edge]
                ##### Convert in circular coordinates:
                I_c=np.mean(I_cur_edg); J_c = np.mean(J_cur_edg)
                r_cur_edg=np.sqrt((I_c-I_cur_edg)**2.+(J_c-J_cur_edg)**2.)
                a_cur_edg=np.arctan2((I_c-I_cur_edg),(J_c-J_cur_edg))
                ##### Smooth edges and add cell thickness:
                nn=3
                r_cur_edg_smth=np.convolve(r_cur_edg,1/nn*np.ones(nn),mode='same')
                r_cur_edg_smth[0]=r_cur_edg[0]
                r_cur_edg_smth[-1]=r_cur_edg[-1]
                ##### Compute back in Cartesian coordinates:
                I_edg=I_c-r_cur_edg_smth*np.sin(a_cur_edg)
                J_edg=J_c-r_cur_edg_smth*np.cos(a_cur_edg)
                
                #### Contsruction shape:
                if IJSN[it_p,2]==1:
                    if flag_lg:
                        grid_I_lg=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_I.txt')
                        grid_J_lg=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_J.txt')
                        flag_lg=False
                    
                    vec_VM=np.zeros(grid_I_lg.shape)
                    for it_I in range(grid_I_lg.shape[0]):
                        for it_J in range(grid_I_lg.shape[1]):
                            if (Polygon(np.vstack((I_edg+grid_I_lg.max()/2.*1.1,J_edg+grid_J_lg.max()/2.*1.1)).T).contains(Point(grid_I_lg[it_I,it_J],grid_J_lg[it_I,it_J]))):
                                vec_VM[it_I,it_J]=1.
                    
                    Q_lg.append(vec_VM)
                
                if IJSN[it_p,2]==0:
                    if flag_sm:
                        grid_I_sm=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_I.txt')
                        grid_J_sm=np.loadtxt('result/'+str(it_p).zfill(3)+'/interpolated/grid_J.txt')
                        flag_sm=False
                    
                    vec_VM=np.zeros(grid_I_sm.shape)
                    for it_I in range(grid_I_sm.shape[0]):
                        for it_J in range(grid_I_sm.shape[1]):
                            if (Polygon(np.vstack((I_edg+grid_I_sm.max()/2*1.1,J_edg+grid_J_sm.max()/2*1.1)).T).contains(Point(grid_I_sm[it_I,it_J],grid_J_sm[it_I,it_J]))):
                                vec_VM[it_I,it_J]=1.
                    
                    Q_sm.append(vec_VM)
    
    ### Compute the average shape
    avg_lg=np.mean(np.dstack(Q_lg),axis=2)
    avg_sm=np.mean(np.dstack(Q_sm),axis=2)
    
    ### Plot results:
    plt.imshow(avg_lg,vmin=0.4,vmax=1.,cmap=plt.cm.GnBu)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/averageGeom/large_'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    plt.imshow(avg_sm,vmin=0.4,vmax=1.,cmap=plt.cm.GnBu)
    plt.colorbar()
    plt.axis('equal')
    plt.axis('off')
    plt.savefig('video/averageGeom/small_'+str(it_stp).zfill(3)+'.png',dpi=200)
    plt.close()
    
    ### Get the average profile:
    #### Large:
    mat_I,mat_J=np.where(avg_lg>0.9)
    hull0=ConvexHull(np.array([mat_I,mat_J]).T)
    I_edg_m=mat_I[hull0.vertices]; I_edg_m=np.append(I_edg_m,I_edg_m[0])
    J_edg_m=mat_J[hull0.vertices]; J_edg_m=np.append(J_edg_m,J_edg_m[0])
    K_lg.append(np.vstack((I_edg_m,J_edg_m)).T)
    #### Small:
    mat_I,mat_J=np.where(avg_sm>0.9)
    hull0=ConvexHull(np.array([mat_I,mat_J]).T)
    I_edg_m=mat_I[hull0.vertices]; I_edg_m=np.append(I_edg_m,I_edg_m[0])
    J_edg_m=mat_J[hull0.vertices]; J_edg_m=np.append(J_edg_m,J_edg_m[0])
    K_sm.append(np.vstack((I_edg_m,J_edg_m)).T)


## Make movies:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/averageGeom/large_%03d.png -qscale 1 video/averageGeom/large/geomAverage_large.avi')
null=os.system('ffmpeg -y -r 5 -f image2 -i video/averageGeom/small_%03d.png -qscale 1 video/averageGeom/small/geomAverage_small.avi')


## Plot the evolution of the average grain shape:
### Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')
### Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)
### Loop over packing fractions for large particles:
for it_phi in range(len(vec_pf)-1):
    #### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
    I_cur=int(np.mean(I_cur))
    #### Get the average shape:
    K_cur=K_lg[I_cur]
    ### Plot shape:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(K_cur[:,0],K_cur[:,1],'-',color=cm(flt_col),linewidth=2)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.axis('equal')
plt.axis('off')
plt.savefig('figure/AverageShape_lg.png')
plt.show()
plt.close()

### Loop over packing fractions for small particles:
for it_phi in range(len(vec_pf)-1):
    #### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<vec_pf[it_phi+1]))[0]
    I_cur=int(np.mean(I_cur))
    #### Get the average shape:
    K_cur=K_sm[I_cur]
    ### Plot shape:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(K_cur[:,0],K_cur[:,1],'-',color=cm(flt_col),linewidth=2)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.axis('equal')
plt.axis('off')
plt.savefig('figure/AverageShape_sm.png')
plt.show()
plt.close()










