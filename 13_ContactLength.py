# This code compute the contacts and measure the contact length


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import math as m
from shapely.geometry import Polygon, Point
import yaml
from PIL import Image

##Display one:
from matplotlib import pyplot as plt


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

## Particule geometry [px]:
### Large:
diam_lg=data_input['general']['particle diameter']['large']

## Proximity criterion for potential contact [px]:
ctt_th=data_input['contact']['proximity threshold consider']
## Number of closest cells to consider for VM field measurement to test contact:
ctt_meas_th=data_input['contact']['number close cells']
## Distance threshold value for contact detection of rigid particles:
ctt_d_th=data_input['contact']['proximity threshold rigid']
## VM threshold value for contact detection of soft particles:
ctt_VM_th=data_input['contact']['von Mises threshold']

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the contact between grains

## Loading tracking data:
A_part=np.loadtxt('particlePicture/A_stp.txt')
I_part=np.loadtxt('particlePicture/I_stp.txt')
J_part=np.loadtxt('particlePicture/J_stp.txt')

## Storage folder:
os.system('mkdir result')
os.system('mkdir result/contact')
os.system('mkdir result/box_edge')
os.system('mkdir video')
os.system('mkdir video/contact')

## Initialize storage vectors for box boundaries:
vec_tl=np.zeros((nb_img,2))
vec_tr=np.zeros((nb_img,2))
vec_bl=np.zeros((nb_img,2))
vec_br=np.zeros((nb_img,2))

## Load particle nature and positions:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Vector to store image size:
vec_im_siz=np.zeros(nb_part)

## Loop over the pictures:
for it_stp in range(nb_img):
    
    ### Display state:
    print('frame '+'%03d'%it_stp)
    
    ### Load current positions and orientations:
    A_cur0=-A_part[:,it_stp]; I_cur0=I_part[:,it_stp]; J_cur0=J_part[:,it_stp]
    
    ### Prepare figure:
    plt.figure()
    if it_stp==0:
        Jm=np.min(J_cur0)-diam_lg/2.
        JM=np.max(J_cur0)+diam_lg/2.
        Im=np.min(I_cur0)-diam_lg/2.
        IM=np.max(I_cur0)+diam_lg/2.
    
    plt.xlim([Jm,JM])
    plt.xlim([Im,IM])
    plt.axis('off')
    plt.axis('equal')
    
    ### List for particules edge storage:
    list_edg=[]
    
    ### Get particles edges:
    for it_p in range(nb_part):
        #### Load data:
        ##### Cell positions:
        I0=np.loadtxt('result/'+'%03d'%it_p+'/index_I.txt')
        J0=np.loadtxt('result/'+'%03d'%it_p+'/index_J.txt')
        ##### Cell displacements:
        mat_dI0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_I.txt')
        mat_dJ0=np.loadtxt('result/'+'%03d'%it_p+'/displacement_J.txt')
        dI_cur=mat_dI0[:,it_stp]
        dJ_cur=mat_dJ0[:,it_stp]
        #### Get image size:
        if it_stp==0:
            pict1=np.array(Image.open('particlePicture/'+str(it_p).zfill(3)+'/'+str(it_stp).zfill(4)+'.png'))
            vec_im_siz[it_p]=pict1.shape[0]
        
        #### Correction of the cell positions:
        I_cur=I0+dI_cur-vec_im_siz[it_p]/2
        J_cur=J0+dJ_cur-vec_im_siz[it_p]/2
        #### Rotation of the cells:
        J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
        I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
        #### Translation of the cells:
        J_n=J_n+J_cur0[it_p]
        I_n=I_n+I_cur0[it_p]
        
        #### Get raw edges:
        ##### Edges:
        edge=np.loadtxt('result/'+'%03d'%it_p+'/edge/'+str(it_stp).zfill(4)+'.txt').astype('int')
        I_edg=I_n[edge]; J_edg=J_n[edge]
        #### Interpolate edges:
        ##### Convert to cylindrical coordinates:
        I_m=np.mean(I_edg); J_m=np.mean(J_edg)
        I_edg_n=I_edg-I_m; J_edg_n=J_edg-J_m
        r_edg=np.sqrt(I_edg_n*I_edg_n+J_edg_n*J_edg_n)
        theta_edg=np.arctan2(I_edg_n,J_edg_n)
        II=np.argsort(theta_edg); r_edg=r_edg[II]; theta_edg=theta_edg[II]
        ##### Spline interpolation:
        theta_edg=np.append(theta_edg,2*m.pi+theta_edg[0])
        r_edg=np.append(r_edg,r_edg[0])
        theta_edg_n=np.linspace(-m.pi,m.pi,360)
        r_edg_n=np.interp(theta_edg_n,theta_edg,r_edg)
        ##### Back to cartesian coordinates:
        I_edg_intp=I_m+r_edg_n*np.sin(theta_edg_n)
        J_edg_intp=J_m+r_edg_n*np.cos(theta_edg_n)
        #### Store it:
        list_edg.append(np.vstack((I_edg_intp,J_edg_intp)).T)
        
        # !Debug!:
        # ~ plt.plot(I_edg,J_edg,'ok')
        # ~ plt.plot(I_edg_intp,J_edg_intp,'-b.')
        # ~ plt.axis('equal')
        # ~ plt.show()
        # ~ plt.close()
    
    # !Debug!:
    # ~ for it_p in range(nb_part):
        # ~ plt.plot(list_edg[it_p][:,0],list_edg[it_p][:,1],'-b')
    # ~ plt.axis('equal')
    # ~ plt.show()
    # ~ plt.close()
    
    ### Find box boundaries:
    #### Define global edge vector:
    gbl_edg=np.vstack(list_edg)
    I_edg=gbl_edg[:,0]
    J_edg=gbl_edg[:,1]
    #### Find upper boundary:
    ##### Initialize:
    K=np.where(((0.45*(np.max(I_edg)-np.min(I_edg))+np.min(I_edg))<I_edg)*(I_edg<(0.55*(np.max(I_edg)-np.min(I_edg))+np.min(I_edg))))[0]
    k_l=K[np.argmin(J_edg[K])]
    k_r=K[np.argmax(J_edg[K])]
    J_m=np.mean(J_edg)
    flag0=True
    ##### Loop:
    while flag0:
        I1=I_edg[k_l]; J1=J_edg[k_l] 
        I2=I_edg[k_r]; J2=J_edg[k_r] 
        det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
        K=np.where(I_edg>(a*J_edg+b)+0.1)[0]
        if len(K)>1:
            KK=np.where(J_edg[K]<J_m)[0]
            if len(KK)>0:
                k_l=K[KK[np.argmin(J_edg[K[KK]])]]
            
            KK=np.where(J_edg[K]>J_m)[0]
            if len(KK)>0:
                k_r=K[KK[np.argmax(J_edg[K[KK]])]]
            
        else:
            flag0=False
    
    k_U_l=k_l
    k_U_r=k_r
    
    #### Find lower boundary:
    ##### Initialize:
    K=np.where(((0.45*(np.max(I_edg)-np.min(I_edg))+np.min(I_edg))<I_edg)*
      (I_edg<(0.55*(np.max(I_edg)-np.min(I_edg))+np.min(I_edg))))[0]
    k_l=K[np.argmin(J_edg[K])]
    k_r=K[np.argmax(J_edg[K])]
    J_m=np.mean(J_edg)
    flag0=True
    ##### Loop:
    while flag0:
        I1=I_edg[k_l]; J1=J_edg[k_l] 
        I2=I_edg[k_r]; J2=J_edg[k_r] 
        det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
        K=np.where(I_edg<(a*J_edg+b)-0.1)[0]
        
        # ~ plt.plot(gbl_edg[:,1],gbl_edg[:,0],'.k')
        # ~ plt.plot(J_edg[K],I_edg[K],'oy')
        # ~ plt.plot(J_edg[[k_l,k_r]],I_edg[[k_l,k_r]],'-r+')
        # ~ plt.axis('equal')
        # ~ plt.show()
        # ~ plt.close()
        
        if len(K)>1:
            KK=np.where(J_edg[K]<J_m)[0]
            if len(KK)>0:
                k_l=K[KK[np.argmin(J_edg[K[KK]])]]
            
            KK=np.where(J_edg[K]>J_m)[0]
            if len(KK)>0:
                k_r=K[KK[np.argmax(J_edg[K[KK]])]]
            
        else:
            flag0=False
    
    k_D_l=k_l
    k_D_r=k_r
    
    #### Find left boundary:
    ##### Initialize:
    I_edg_tmp=J_edg; J_edg_tmp=I_edg 
    K=np.where(((0.45*(np.max(I_edg_tmp)-np.min(I_edg_tmp))+np.min(I_edg_tmp))<I_edg_tmp)*
      (I_edg_tmp<(0.55*(np.max(I_edg_tmp)-np.min(I_edg_tmp))+np.min(I_edg_tmp))))[0]
    k_l=K[np.argmin(J_edg_tmp[K])]
    k_r=K[np.argmax(J_edg_tmp[K])]
    J_m=np.mean(J_edg_tmp)
    flag0=True
    ##### Loop:
    while flag0:
        I1=I_edg_tmp[k_l]; J1=J_edg_tmp[k_l] 
        I2=I_edg_tmp[k_r]; J2=J_edg_tmp[k_r] 
        det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
        K=np.where(I_edg_tmp<(a*J_edg_tmp+b)-0.1)[0]
        
        # ~ plt.plot(J_edg_tmp,I_edg_tmp,'.k')
        # ~ plt.plot(J_edg_tmp[K],I_edg_tmp[K],'oy')
        # ~ plt.plot(J_edg_tmp[[k_l,k_r]],I_edg_tmp[[k_l,k_r]],'-r+')
        # ~ plt.axis('equal')
        # ~ plt.show()
        # ~ plt.close()
        
        if len(K)>1:
            KK=np.where(J_edg_tmp[K]<J_m)[0]
            if len(KK)>0:
                k_l=K[KK[np.argmin(J_edg_tmp[K[KK]])]]
            
            KK=np.where(J_edg_tmp[K]>J_m)[0]
            if len(KK)>0:
                k_r=K[KK[np.argmax(J_edg_tmp[K[KK]])]]
            
        else:
            flag0=False
    
    k_L_d=k_l
    k_L_u=k_r
    
    #### Find right boundary:
    ##### Initialize:
    I_edg_tmp=J_edg; J_edg_tmp=I_edg 
    K=np.where(((0.45*(np.max(I_edg_tmp)-np.min(I_edg_tmp))+np.min(I_edg_tmp))<I_edg_tmp)*
      (I_edg_tmp<(0.55*(np.max(I_edg_tmp)-np.min(I_edg_tmp))+np.min(I_edg_tmp))))[0]
    k_l=K[np.argmin(J_edg_tmp[K])]
    k_r=K[np.argmax(J_edg_tmp[K])]
    J_m=np.mean(J_edg_tmp)
    flag0=True
    ##### Loop:
    while flag0:
        I1=I_edg_tmp[k_l]; J1=J_edg_tmp[k_l] 
        I2=I_edg_tmp[k_r]; J2=J_edg_tmp[k_r] 
        det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
        K=np.where(I_edg_tmp>(a*J_edg_tmp+b)+0.1)[0]
        
        # ~ plt.plot(J_edg_tmp,I_edg_tmp,'.k')
        # ~ plt.plot(J_edg_tmp[K],I_edg_tmp[K],'oy')
        # ~ plt.plot(J_edg_tmp[[k_l,k_r]],I_edg_tmp[[k_l,k_r]],'-r+')
        # ~ plt.axis('equal')
        # ~ plt.show()
        # ~ plt.close()
        
        if len(K)>1:
            KK=np.where(J_edg_tmp[K]<J_m)[0]
            if len(KK)>0:
                k_l=K[KK[np.argmin(J_edg_tmp[K[KK]])]]
            
            KK=np.where(J_edg_tmp[K]>J_m)[0]
            if len(KK)>0:
                k_r=K[KK[np.argmax(J_edg_tmp[K[KK]])]]
            
        else:
            flag0=False
    
    k_R_d=k_l
    k_R_u=k_r
    
    ### Find frame edges:
    #### Top left:
    x1=J_edg[k_U_l]; x2=J_edg[k_U_r]; y1=I_edg[k_U_l]; y2=I_edg[k_U_r]
    a=(y1-y2)/(x1-x2); b=y1-a*x1
    x1=J_edg[k_L_u]; x2=J_edg[k_L_d]; y1=I_edg[k_L_u]; y2=I_edg[k_L_d]
    c=(y1-y2)/(x1-x2); d=y1-c*x1
    J_tl=(d-b)/(a-c)
    I_tl=a*J_tl+b
    #### Top right:
    x1=J_edg[k_U_l]; x2=J_edg[k_U_r]; y1=I_edg[k_U_l]; y2=I_edg[k_U_r]
    a=(y1-y2)/(x1-x2); b=y1-a*x1
    x1=J_edg[k_R_u]; x2=J_edg[k_R_d]; y1=I_edg[k_R_u]; y2=I_edg[k_R_d]
    c=(y1-y2)/(x1-x2); d=y1-c*x1
    J_tr=(d-b)/(a-c)
    I_tr=a*J_tr+b
    #### Bottom right:
    x1=J_edg[k_D_l]; x2=J_edg[k_D_r]; y1=I_edg[k_D_l]; y2=I_edg[k_D_r]
    a=(y1-y2)/(x1-x2); b=y1-a*x1
    x1=J_edg[k_R_u]; x2=J_edg[k_R_d]; y1=I_edg[k_R_u]; y2=I_edg[k_R_d]
    c=(y1-y2)/(x1-x2); d=y1-c*x1
    J_br=(d-b)/(a-c)
    I_br=a*J_br+b
    #### Bottom left:
    x1=J_edg[k_D_l]; x2=J_edg[k_D_r]; y1=I_edg[k_D_l]; y2=I_edg[k_D_r]
    a=(y1-y2)/(x1-x2); b=y1-a*x1
    x1=J_edg[k_L_u]; x2=J_edg[k_L_d]; y1=I_edg[k_L_u]; y2=I_edg[k_L_d]
    c=(y1-y2)/(x1-x2); d=y1-c*x1
    J_bl=(d-b)/(a-c)
    I_bl=a*J_bl+b
    
    ### Store edges:
    vec_tl[it_stp,:]=np.array([I_tl,J_tl])
    vec_tr[it_stp,:]=np.array([I_tr,J_tr])
    vec_br[it_stp,:]=np.array([I_br,J_br])
    vec_bl[it_stp,:]=np.array([I_bl,J_bl])
    
    ### Add edges in the edg list:
    #### Get the space between points:
    d_edg=int(np.max(np.abs(np.diff(list_edg[0][:,0]))))
    #### Make vectors for top edge:
    I1=I_tl; J1=J_tl; I2=I_tr; J2=J_tr 
    det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
    vec_J_t=np.arange(J_tl,J_tr,d_edg)
    vec_I_t=a*vec_J_t+b
    list_edg.append(np.vstack((vec_I_t,vec_J_t)).T)
    #### Make vectors for right edge:
    I1=J_tr; J1=I_tr; I2=J_br; J2=I_br 
    det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
    vec_I_r=np.arange(I_br,I_tr,d_edg)
    vec_J_r=a*vec_I_r+b
    list_edg.append(np.vstack((vec_I_r,vec_J_r)).T)
    #### Make vectors for bottom edge:
    I1=I_bl; J1=J_bl; I2=I_br; J2=J_br 
    det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
    vec_J_b=np.arange(J_bl,J_br,d_edg)
    vec_I_b=a*vec_J_b+b
    list_edg.append(np.vstack((vec_I_b,vec_J_b)).T)
    #### Make vectors for left edge:
    I1=J_tl; J1=I_tl; I2=J_bl; J2=I_bl 
    det=J1-J2; a=(I1-I2)/det; b=(J1*I2-J2*I1)/det
    vec_I_l=np.arange(I_bl,I_tl,d_edg)
    vec_J_l=a*vec_I_l+b
    list_edg.append(np.vstack((vec_I_l,vec_J_l)).T)
    
    # !Debug!:
    # ~ plt.plot(gbl_edg[:,1],gbl_edg[:,0],'.k')
    # ~ plt.plot(J_edg[[k_U_l,k_U_r]],I_edg[[k_U_l,k_U_r]],'r+')
    # ~ plt.plot(J_edg[[k_D_l,k_D_r]],I_edg[[k_D_l,k_D_r]],'r+')
    # ~ plt.plot(J_edg[[k_L_d,k_L_u]],I_edg[[k_L_d,k_L_u]],'r+')
    # ~ plt.plot(J_edg[[k_R_d,k_R_u]],I_edg[[k_R_d,k_R_u]],'r+')
    # ~ plt.plot([J_tl,J_tr],[I_tl,I_tr],'-xm')
    # ~ plt.plot([J_bl,J_br],[I_bl,I_br],'-xm')
    # ~ plt.plot([J_bl,J_tl],[I_bl,I_tl],'-xm')
    # ~ plt.plot([J_br,J_tr],[I_br,I_tr],'-xm')
    # ~ plt.plot(vec_J_t,vec_I_t,'.m')
    # ~ plt.plot(vec_J_r,vec_I_r,'.m')
    # ~ plt.plot(vec_J_b,vec_I_b,'.m')
    # ~ plt.plot(vec_J_l,vec_I_l,'.m')
    # ~ plt.axis('equal')
    # ~ plt.show()
    # ~ plt.close()
    
    ### Add particle number in the edge list:
    for it_edg in range(len(list_edg)):
        if it_edg<nb_part:
            list_edg[it_edg]=np.vstack((list_edg[it_edg][:,0],list_edg[it_edg][:,1],it_edg*np.ones(list_edg[it_edg].shape[0]))).T
        else:
            list_edg[it_edg]=np.vstack((list_edg[it_edg][:,0],list_edg[it_edg][:,1],(nb_part-it_edg-1)*np.ones(list_edg[it_edg].shape[0]))).T
    
    ### Detect potential contacts:
    #### Initialize list:
    list_cnt_pot=list()
    #### Loop over particles:
    for it_p in range(nb_part):
        ##### Get particle edges:
        I_p=list_edg[it_p][:,0]
        J_p=list_edg[it_p][:,1]
        ##### Get edges of other particles:
        other_edg=list_edg.copy()
        other_edg.pop(it_p)
        other_edg=np.vstack(other_edg)
        ##### Initialize index list:
        K_cnt=list()
        ##### Loop over particle edges:
        for it_edg in range(len(I_p)):
            ###### Get current coordinates:
            I_c=I_p[it_edg]
            J_c=J_p[it_edg]
            ###### Compute proximity:
            d_c=np.sqrt((other_edg[:,0]-I_c)**2.+(other_edg[:,1]-J_c)**2.)
            ###### Test proximity and store position if necessary:
            if np.min(d_c)<ctt_th:
                K_cnt.append([it_edg,other_edg[np.where(d_c==np.min(d_c))[0][0],2],np.min(d_c)])
        
        ##### Store potential contacts:
        if len(K_cnt)>0:
            list_cnt_pot.append(np.vstack(K_cnt).astype('int'))
        else:
            list_cnt_pot.append(np.array([np.float('nan')]))
    
    # !Debug!:
    # ~ plt.plot(gbl_edg[:,1],gbl_edg[:,0],'.k')
    # ~ plt.plot(J_edg[[k_U_l,k_U_r]],I_edg[[k_U_l,k_U_r]],'r+')
    # ~ plt.plot(J_edg[[k_D_l,k_D_r]],I_edg[[k_D_l,k_D_r]],'r+')
    # ~ plt.plot(J_edg[[k_L_d,k_L_u]],I_edg[[k_L_d,k_L_u]],'r+')
    # ~ plt.plot(J_edg[[k_R_d,k_R_u]],I_edg[[k_R_d,k_R_u]],'r+')
    # ~ plt.plot([J_tl,J_tr],[I_tl,I_tr],'-xm')
    # ~ plt.plot([J_bl,J_br],[I_bl,I_br],'-xm')
    # ~ plt.plot([J_bl,J_tl],[I_bl,I_tl],'-xm')
    # ~ plt.plot([J_br,J_tr],[I_br,I_tr],'-xm')
    # ~ for it in range(len(list_cnt_pot)):
        # ~ K_cnt=list_cnt_pot[it]
        # ~ if K_cnt.ndim>1:
            # ~ I=list_edg[it][K_cnt[:,0],0]
            # ~ J=list_edg[it][K_cnt[:,0],1]
            # ~ plt.plot(J,I,'.r')
    
    # ~ plt.axis('equal')
    # ~ plt.show()
    # ~ plt.close()
    
    #### Initialize list:
    list_cnt=list()
    
    ### Test contact from particle strain and distance:
    for it_p in range(nb_part):
        K_cnt=list_cnt_pot[it_p]
        if K_cnt.ndim>1:
            #### Load grain data:
            ##### Consider particle nature:
            if bool(IJSN[it_p,3]):
                tensor='strain_E'
            else:
                tensor='right_Cauchy_Green_C'
            
            ##### Cell positions:
            I0=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_I.txt').flatten()
            J0=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_J.txt').flatten()
            ##### Cell displacements:
            dI_cur=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_I.txt').flatten()
            dJ_cur=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%it_stp+'_grid_displacement_J.txt').flatten()
            ##### Load elasticity components and compute von Mises strain:
            vec_EgV1=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_eigenvalue_1.txt').flatten()
            vec_EgV2=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/'+tensor+'/'+'%03d'%it_stp+'_eigenvalue_2.txt').flatten()
            vec_VM=np.sqrt(vec_EgV1*vec_EgV1+vec_EgV2*vec_EgV2-vec_EgV1*vec_EgV2)
            ##### Correction of the cell positions:
            I_cur=I0+dI_cur
            J_cur=J0+dJ_cur
            ##### Rotation of the cells:
            J_n=m.cos(A_cur0[it_p]/180.*m.pi)*J_cur+m.sin(A_cur0[it_p]/180.*m.pi)*I_cur
            I_n=-m.sin(A_cur0[it_p]/180.*m.pi)*J_cur+m.cos(A_cur0[it_p]/180.*m.pi)*I_cur
            ##### Translation of the cells:
            vec_J=J_n+J_cur0[it_p]
            vec_I=I_n+I_cur0[it_p]
            ##### Remove nan:
            II=np.where(~np.isnan(vec_VM))
            vec_VM=vec_VM[II]
            vec_I=vec_I[II]
            vec_J=vec_J[II]
            
            #### Load contact information:
            ##### Contact position:
            I_cnt=list_edg[it_p][K_cnt[:,0],0]
            J_cnt=list_edg[it_p][K_cnt[:,0],1]
            ##### Particle in contact:
            Nwith_cnt=K_cnt[:,1].astype(int)
            ##### Contact distance: 
            d_cnt=K_cnt[:,2]
            
            #### Initialize storage vector:
            ##### Strain value around the contact:
            v_cnt=np.ones(d_cnt.shape)
            ##### contact validity:
            bool_cnt=np.zeros(d_cnt.shape)
            
            #### Make strain universal:
            vec_VM=vec_VM/np.nanmean(vec_VM)
            vec_VM=np.abs(vec_VM-np.nanmean(vec_VM))
            
            #### Test contact:
            if bool(IJSN[it_p,3]):
                bool_cnt=d_cnt<ctt_d_th
            else:
                for it_k in range(len(I_cnt)):
                    ##### Get position of the current potential contact:
                    I_c=I_cnt[it_k]
                    J_c=J_cnt[it_k]
                    ##### Get closest points:
                    d_c=np.sqrt((I_c-vec_I)**2.+(J_c-vec_J)**2.)
                    II_c=np.argsort(d_c)
                    ##### Measure local strain:
                    val_c=np.nanmean(vec_VM[II_c[0:ctt_meas_th-1]])
                    v_cnt[it_k]=val_c
                    ##### Test contact:
                    if val_c>ctt_VM_th:
                        bool_cnt[it_k]=1
            
            #### Store results:
            list_cnt.append(np.vstack((I_cnt,J_cnt,Nwith_cnt,d_cnt,v_cnt,bool_cnt)).T)
            
            # !Debug!:
            # ~ cmap=plt.cm.jet
            # ~ Qm=np.nanmin(vec_VM); QM=np.sort(vec_VM[np.where(~np.isnan(vec_VM))])[-20]; vec_Q=np.linspace(Qm,QM,20)
            # ~ for it in range(len(vec_Q)-1):
                # ~ II=np.where((vec_Q[it]<=vec_VM)*(vec_VM<vec_Q[it+1]))
                # ~ plt.plot(vec_J[II],-vec_I[II],'.',color=cmap(float(it)/len(vec_Q)),markersize=6)
            
            # ~ cmap=plt.cm.winter
            # ~ Qm=np.nanmin(v_cnt); QM=np.nanmax(v_cnt); vec_Q=np.linspace(Qm,QM,20)
            # ~ #Qm=np.nanmin(d_cnt); QM=np.nanmax(d_cnt); vec_Q=np.linspace(Qm,QM,20)
            # ~ for it in range(len(vec_Q)-1):
                # ~ II=np.where((vec_Q[it]<=v_cnt)*(v_cnt<vec_Q[it+1]))[0]
                # ~ #II=np.where((vec_Q[it]<=d_cnt)*(d_cnt<vec_Q[it+1]))
                # ~ plt.plot(J_cnt[II],-I_cnt[II],'.',color=cmap(float(it)/len(vec_Q)),markersize=15)
            
            # ~ plt.plot(J_cnt[np.where(bool_cnt)],-I_cnt[np.where(bool_cnt)],'.m')
            # ~ plt.title('step '+str(it_stp)+' - particle '+str(it_p))
            # ~ plt.axis('equal')
            # ~ plt.show()
            # ~ plt.close()
        
        else:
            list_cnt.append(np.array([np.float('nan')]))
    
    # !Debug!:
    # ~ plt.plot(gbl_edg[:,1],gbl_edg[:,0],'.k')
    # ~ plt.plot([J_tl,J_tr],[I_tl,I_tr],'-xm')
    # ~ plt.plot([J_bl,J_br],[I_bl,I_br],'-xm')
    # ~ plt.plot([J_bl,J_tl],[I_bl,I_tl],'-xm')
    # ~ plt.plot([J_br,J_tr],[I_br,I_tr],'-xm')
    # ~ for it in range(len(list_cnt)):
        # ~ K_cnt=list_cnt[it]
        # ~ if K_cnt.ndim>1:
            # ~ I=K_cnt[:,0]
            # ~ J=K_cnt[:,1]
            # ~ II=np.where(K_cnt[:,5])
            # ~ plt.plot(J[II],I[II],'.r')
    
    # ~ plt.axis('equal')
    # ~ plt.show()
    # ~ plt.close()
    
    ### Regularize data:
    list_cnt_final=[]
    for it_p in range(nb_part):
        K_cnt=list_cnt[it_p]
        if K_cnt.ndim>1:
            #### Load data of the current particle:
            ##### Contact bool:
            b_c=K_cnt[:,5]
            ##### Neighboor numbers:
            all_N_c_u=np.unique(K_cnt[np.where(b_c),2][0])
            ##### Loop over contacts:
            for it_n in range(len(all_N_c_u)):
                flag_cnt=True
                N_v=int(all_N_c_u[it_n]) 
                ##### Information about the current contact:
                N_c=it_p
                I_c=K_cnt[np.where(b_c*(K_cnt[:,2]==N_v))[0],0]
                J_c=K_cnt[np.where(b_c*(K_cnt[:,2]==N_v))[0],1]
                if N_v>-1:
                    K_cnt_v=list_cnt[N_v]
                    I_v=K_cnt_v[np.where(K_cnt_v[:,5]*(K_cnt_v[:,2]==N_c))[0],0]
                    J_v=K_cnt_v[np.where(K_cnt_v[:,5]*(K_cnt_v[:,2]==N_c))[0],1]
                    if len(I_v)>0:
                        ##### Get contact information:
                        ###### Contact length:
                        v_I_tmp=list_edg[N_c][:,0]; v_J_tmp=list_edg[N_c][:,1]
                        d_tmp=np.mean(np.sqrt((v_I_tmp[1:]-v_I_tmp[0:-1])**2.+(v_J_tmp[1:]-v_J_tmp[0:-1])**2.))
                        d_a=len(I_c)*d_tmp
                        v_I_tmp=list_edg[N_v][:,0]; v_J_tmp=list_edg[N_v][:,1]
                        d_tmp=np.mean(np.sqrt((v_I_tmp[1:]-v_I_tmp[0:-1])**2.+(v_J_tmp[1:]-v_J_tmp[0:-1])**2.))
                        d_b=len(I_v)*d_tmp
                        d_0=0.5*(d_a+d_b)
                        r_d_0=max(abs(d_a/d_b),abs(d_b/d_a))-1.
                        ###### Contact position and orientation:
                        if d_a>d_b:
                            I_m=np.nanmean(I_c)
                            J_m=np.nanmean(J_c)
                            p0=np.polyfit(J_c,I_c,1)
                            
                            # !Debug!:
                            # ~ plt.plot(J_v,I_v,'.k')
                            # ~ plt.plot(J_c,I_c,'ob')
                            # ~ plt.plot(J_m,I_m,'g+',markersize=30)
                            # ~ plt.plot(J_c,p0[1]+J_c*p0[0],'r')
                            # ~ plt.show()
                            # ~ plt.close()
                        else:
                            I_m=np.nanmean(I_v)
                            J_m=np.nanmean(J_v)
                            p0=np.polyfit(J_v,I_v,1)
                        
                        A_0=m.atan(p0[0])
                        
                        ##### Remove contact
                        II=np.where(K_cnt_v[:,2]==N_c)[0]
                        K_cnt_v_n=np.delete(K_cnt_v,II,0)
                        if K_cnt_v.ndim>1:
                            list_cnt[N_v]=K_cnt_v_n
                        else:
                            list_cnt[N_v]=np.array([0])
                    else:
                        flag_cnt=False
                else:
                    v_I_tmp=list_edg[N_c][:,0]; v_J_tmp=list_edg[N_c][:,1]
                    d_tmp=np.mean(np.sqrt((v_I_tmp[1:]-v_I_tmp[0:-1])**2.+(v_J_tmp[1:]-v_J_tmp[0:-1])**2.))
                    d_0=len(I_c)*d_tmp
                    r_d_0=np.float('nan')
                    I_m=np.nanmean(I_c)
                    J_m=np.nanmean(J_c)
                    p0=np.polyfit(J_c,I_c,1)
                    A_0=m.atan(p0[0])
                
                ##### Store contact information: N_part1 | N_part2 | coord_I | coord_J | length | orient | error
                if flag_cnt:
                    list_cnt_final.append(np.array([N_c,N_v,I_m,J_m,d_0,A_0,r_d_0]))
    
    ### Plot contacts:
    #### Plot particles:
    for it_p in range(len(list_edg)):
        if list_edg[it_p][0,2]>-1:
            v_I=list_edg[it_p][:,0]
            v_J=list_edg[it_p][:,1]
            plt.plot(v_J,v_I,'-k',linewidth=3)
    
    #### Plot edges:
    plt.plot([J_tl,J_tr],[I_tl,I_tr],'-+b',linewidth=2)
    plt.plot([J_bl,J_br],[I_bl,I_br],'-+b',linewidth=2)
    plt.plot([J_bl,J_tl],[I_bl,I_tl],'-+b',linewidth=2)
    plt.plot([J_br,J_tr],[I_br,I_tr],'-+b',linewidth=2)
    #### Plot contacts:
    for it_cnt in range(len(list_cnt_final)):
        I_c=list_cnt_final[it_cnt][2]
        J_c=list_cnt_final[it_cnt][3]
        d_c=list_cnt_final[it_cnt][4]
        A_c=list_cnt_final[it_cnt][5]
        I_1=I_c-d_c/2.*m.sin(A_c)
        I_2=I_c+d_c/2.*m.sin(A_c)
        J_1=J_c-d_c/2.*m.cos(A_c)
        J_2=J_c+d_c/2.*m.cos(A_c)
        plt.plot([J_1,J_2],[I_1,I_2],'-m',linewidth=4)
    
    #### Save plot:
    plt.savefig('video/contact/'+'%03d'%(it_stp)+'.png',dpi=200)
    plt.close()
    
    ### Save contact:
    if len(list_cnt_final)>0:
        np.savetxt('result/contact/ctt_stp_'+'%03d'%(it_stp)+'.txt',np.vstack(list_cnt_final))
    else:
        np.savetxt('result/contact/ctt_stp_'+'%03d'%(it_stp)+'.txt',np.array([0]))


## Save edges:
np.savetxt('result/box_edge/IJ_top_left.txt',vec_tl)
np.savetxt('result/box_edge/IJ_top_right.txt',vec_tr)
np.savetxt('result/box_edge/IJ_bottom_right.txt',vec_br)
np.savetxt('result/box_edge/IJ_bottom_left.txt',vec_bl)

## Make movie:
null=os.system('ffmpeg -y -r 5 -f image2 -i video/contact/%03d.png -qscale 1 video/contact/contact.avi')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Split results for each particle:

## Loop over particle:
for it_p in range(nb_part):
    ### Intialize vectors:
    vec_z=np.zeros(nb_img)
    vec_l=np.zeros(nb_img)
    ### Loop over steps:
    for it_stp in range(nb_img):
        #### Load the data:
        K_cnt=np.loadtxt('result/contact/ctt_stp_'+str(it_stp).zfill(3)+'.txt')
        if K_cnt.ndim>1:
            II=np.where(((K_cnt[:,0]==it_p)+(K_cnt[:,1]==it_p))>0.5)[0]
            vec_z[it_stp]=len(II)
            vec_l[it_stp]=np.sum(K_cnt[II,4])
    
    ### Save results:
    np.savetxt('result/'+str(it_p).zfill(3)+'/contact_length.txt',vec_l)
    np.savetxt('result/'+str(it_p).zfill(3)+'/coordination.txt',vec_z)






















