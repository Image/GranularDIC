#This code performs DIC computation of the displacement field for bidisperse soft grains experiments

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries and global variables:

## Commons ones:
import os
import numpy as np
import random
import math as m
from PIL import Image
Image.MAX_IMAGE_PIXELS = 933120000
from scipy import ndimage as nd
from scipy import signal as sg
from scipy.optimize import minimize
from multiprocessing import Pool, Array
from pylab import ginput
import fnmatch
import yaml

## Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Maximum number of pixel per cells in line and row [px]:  
cc_0_M_r=data_input['granular DIC']['correlation cell size']['rigid'][0]
cc_0_M_s=data_input['granular DIC']['correlation cell size']['soft'][0]

## Minimum number of pixel per cells in line and row [px]:  
cc_0_m_r=data_input['granular DIC']['correlation cell size']['rigid'][1]
cc_0_m_s=data_input['granular DIC']['correlation cell size']['soft'][1]

## Maximum shift optimization (for optimization algorithm) [px<<cc_0_m]:
m_sht=data_input['granular DIC']['maximum shift']

## Number of cell size subdivision :
nc_r=data_input['granular DIC']['number of step']['rigid']
nc_s=data_input['granular DIC']['number of step']['soft']

## Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']

## Image reduction factor for display:
ImRat=data_input['granular DIC']['image reduction']

## Number of processor for parallelization:
nb_proc=data_input['general']['number of processor']

## Correlation maximization inputs:
### Error tolerance: 
tol_0=1e-5,
### Maximum number of iterations:
max_iter_0=300
### Maximum number of function evaluation:
max_fev_0=300
### Verbosity:
verbose=False

## Local input:
### Correlation restriction [0 -> no restiction ; 1 -> only rigid ; 2 -> only soft]:
restrict_correl = 0

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Initialization:

## Load the particle position, size and nature:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Make indice vector for each correlation cell for large particles:
### Get image size:
I_lg=np.where((IJSN[:,2]==1)*(IJSN[:,3]==0))[0][0]
image_tmp=Image.open('particlePicture/'+str(I_lg).zfill(3)+'/'+str(0).zfill(4)+'.png')
S00=image_tmp.size
S0=(int(S00[0]*ImRat),int(S00[1]*ImRat))
image_tmp.thumbnail(S0,Image.ANTIALIAS)
image_tmp=np.array(image_tmp).astype('float')
Si=S00[1]
Sj=S00[0]
### Compute the number of cells:
nbi_0=int(Si/dd_0)
nbj_0=int(Sj/dd_0)
### Make cell size vector:
vec_cc_r=np.linspace(cc_0_M_r,cc_0_m_r,nc_r).astype(int)
vec_cc_s=np.linspace(cc_0_M_s,cc_0_m_s,nc_s).astype(int)
### Make cell position vectors:
vec_i0=np.linspace(0,Si,nbi_0).astype(int)
vec_j0=np.linspace(0,Sj,nbj_0).astype(int)
mat_i, mat_j=np.meshgrid(vec_i0,vec_j0) 
vec_i0=np.reshape(mat_i,[mat_i.size])
vec_j0=np.reshape(mat_j,[mat_j.size])

## Reduction inside the disc for large particles:
### Manually pick the position:
plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
plt.imshow(image_tmp,cmap=plt.cm.Greys)
plt.colorbar()
plt.title('pick 3 points on the edge of the disc')
[i1,j1],[i2,j2],[i3,j3]=plt.ginput(3)
plt.close()
### remove extra zones:
a1=(j1-j3)/(i1-i3); a2=(j2-j3)/(i2-i3); xe=(i1+i3)/2; ye=(j1+j3)/2; b1=ye+xe/a1; xf=(i2+i3)/2; yf=(j2+j3)/2; b2=yf+xf/a2
jc=(b2-b1)/(1/a2-1/a1); ic=-jc/a1+b1; 
rc=m.sqrt((j1-ic)**2.+(i1-jc)**2.)/ImRat
jc=jc/ImRat; ic=ic/ImRat
vec_iN=[]
vec_jN=[]
### Loop over the cell sizes :
for it in range(len(vec_i0)):
    icc=vec_i0[it]
    jcc=vec_j0[it]
    if (m.sqrt((icc-ic)**2.+(jcc-jc)**2.)<=rc):
        vec_iN.append(icc)
        vec_jN.append(jcc)

vec_i0_lg=np.array(vec_iN)
vec_j0_lg=np.array(vec_jN)
del vec_iN, vec_jN, vec_i0, vec_j0

### Correlation parameter checking for large soft particles:
I_lg_s=np.where((IJSN[:,2]==1)*(IJSN[:,3]==0))[0]
if len(I_lg_s)>0:
    I_lg_s=I_lg_s[0]
    image_tmp=np.array(Image.open('particlePicture/'+str(I_lg_s).zfill(3)+'/'+str(0).zfill(4)+'.png'))
    plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
    plt.imshow(image_tmp,cmap=plt.cm.Greys)
    plt.colorbar()
    plt.plot(vec_j0_lg,vec_i0_lg,'b.',markersize=5)
    d=np.abs(np.sqrt((vec_j0_lg-image_tmp.shape[1]/2)**2.+(vec_i0_lg-image_tmp.shape[0]/2)**2.))
    II=np.where(d==np.min(d))[0][0]
    i0=vec_i0_lg[II]; j0=vec_j0_lg[II]
    im=(i0-cc_0_m_s/2.); iM=(i0+cc_0_m_s/2.)
    jm=(j0-cc_0_m_s/2.); jM=(j0+cc_0_m_s/2.)
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    im=(i0-cc_0_M_s/2.); iM=(i0+cc_0_M_s/2.)        
    jm=(j0-cc_0_M_s/2.); jM=(j0+cc_0_M_s/2.)        
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    plt.show()
    plt.close()

### Correlation parameter checking for large rigid particles:
I_lg_r=np.where((IJSN[:,2]==1)*(IJSN[:,3]==1))[0]
if len(I_lg_r)>0:
    I_lg_r=I_lg_r[0]
    image_tmp=np.array(Image.open('particlePicture/'+str(I_lg_r).zfill(3)+'/'+str(0).zfill(4)+'.png'))
    plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
    plt.imshow(image_tmp,cmap=plt.cm.Greys)
    plt.colorbar()
    plt.plot(vec_j0_lg,vec_i0_lg,'b.',markersize=5)
    d=np.abs(np.sqrt((vec_j0_lg-image_tmp.shape[1]/2)**2.+(vec_i0_lg-image_tmp.shape[0]/2)**2.))
    II=np.where(d==np.min(d))[0][0]
    i0=vec_i0_lg[II]; j0=vec_j0_lg[II]
    im=(i0-cc_0_m_r/2.); iM=(i0+cc_0_m_r/2.)
    jm=(j0-cc_0_m_r/2.); jM=(j0+cc_0_m_r/2.)
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    im=(i0-cc_0_M_r/2.); iM=(i0+cc_0_M_r/2.)
    jm=(j0-cc_0_M_r/2.); jM=(j0+cc_0_M_r/2.)
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    plt.show()
    plt.close()

## Make indice vector for each correlation cell for small particles:
### Get image size:
I_sm=np.where((IJSN[:,2]==0)*(IJSN[:,3]==0))[0][0]
image_tmp=Image.open('particlePicture/'+str(I_sm).zfill(3)+'/'+str(0).zfill(4)+'.png')
S00=image_tmp.size
S0=(int(S00[0]*ImRat),int(S00[1]*ImRat))
image_tmp.thumbnail(S0,Image.ANTIALIAS)
image_tmp=np.array(image_tmp).astype('float')
Si=S00[1]
Sj=S00[0]
### Compute the number of cells:
nbi_0=int(Si/dd_0)
nbj_0=int(Sj/dd_0)
### Make cell position vectors:
vec_i0=np.linspace(0,Si,nbi_0).astype(int)
vec_j0=np.linspace(0,Sj,nbj_0).astype(int)
mat_i, mat_j=np.meshgrid(vec_i0,vec_j0) 
vec_i0=np.reshape(mat_i,[mat_i.size])
vec_j0=np.reshape(mat_j,[mat_j.size])

## Reduction inside the disc for large particles:
### Manually pick the position:
plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
plt.imshow(image_tmp,cmap=plt.cm.Greys)
plt.colorbar()
plt.title('pick 3 points on the edge of the disc')
[i1,j1],[i2,j2],[i3,j3]=plt.ginput(3)
plt.close()
### remove extra zones:
a1=(j1-j3)/(i1-i3); a2=(j2-j3)/(i2-i3); xe=(i1+i3)/2; ye=(j1+j3)/2; b1=ye+xe/a1; xf=(i2+i3)/2; yf=(j2+j3)/2; b2=yf+xf/a2
jc=(b2-b1)/(1/a2-1/a1); ic=-jc/a1+b1; 
rc=m.sqrt((j1-ic)**2.+(i1-jc)**2.)/ImRat
jc=jc/ImRat; ic=ic/ImRat
vec_iN=[]
vec_jN=[]
### Loop over the cell sizes :
for it in range(len(vec_i0)):
    icc=vec_i0[it]
    jcc=vec_j0[it]
    if (m.sqrt((icc-ic)**2.+(jcc-jc)**2.)<=rc):
        vec_iN.append(icc)
        vec_jN.append(jcc)

vec_i0_sm=np.array(vec_iN)
vec_j0_sm=np.array(vec_jN)
del vec_iN, vec_jN, vec_i0, vec_j0

### Correlation parameter checking for large soft particles:
I_sm_s=np.where((IJSN[:,2]==0)*(IJSN[:,3]==0))[0]
if len(I_sm_s)>0:
    I_sm_s=I_sm_s[0]
    image_tmp=np.array(Image.open('particlePicture/'+str(I_sm_s).zfill(3)+'/'+str(0).zfill(4)+'.png'))
    plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
    plt.imshow(image_tmp,cmap=plt.cm.Greys)
    plt.colorbar()
    plt.plot(vec_j0_sm,vec_i0_sm,'b.',markersize=5)
    d=np.abs(np.sqrt((vec_j0_sm-image_tmp.shape[1]/2)**2.+(vec_i0_sm-image_tmp.shape[0]/2)**2.))
    II=np.where(d==np.min(d))[0][0]
    i0=vec_i0_sm[II]; j0=vec_j0_sm[II]
    im=(i0-cc_0_m_s/2.); iM=(i0+cc_0_m_s/2.)
    jm=(j0-cc_0_m_s/2.); jM=(j0+cc_0_m_s/2.)
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    im=(i0-cc_0_M_s/2.); iM=(i0+cc_0_M_s/2.)        
    jm=(j0-cc_0_M_s/2.); jM=(j0+cc_0_M_s/2.)        
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    plt.show()
    plt.close()

### Correlation parameter checking for large rigid particles:
I_sm_r=np.where((IJSN[:,2]==0)*(IJSN[:,3]==1))[0]
if len(I_sm_r)>0:
    I_sm_r=I_sm_r[0]
    image_tmp=np.array(Image.open('particlePicture/'+str(I_sm_r).zfill(3)+'/'+str(0).zfill(4)+'.png'))
    plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
    plt.imshow(image_tmp,cmap=plt.cm.Greys)
    plt.colorbar()
    plt.plot(vec_j0_sm,vec_i0_sm,'b.',markersize=5)
    d=np.abs(np.sqrt((vec_j0_sm-image_tmp.shape[1]/2)**2.+(vec_i0_sm-image_tmp.shape[0]/2)**2.))
    II=np.where(d==np.min(d))[0][0]
    i0=vec_i0_sm[II]; j0=vec_j0_sm[II]
    im=(i0-cc_0_m_r/2.); iM=(i0+cc_0_m_r/2.)
    jm=(j0-cc_0_m_r/2.); jM=(j0+cc_0_m_r/2.)
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    im=(i0-cc_0_M_r/2.); iM=(i0+cc_0_M_r/2.)
    jm=(j0-cc_0_M_r/2.); jM=(j0+cc_0_M_r/2.)
    plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-b',linewidth=2)
    plt.show()
    plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Define function to measure image correlation, for subpixel accurate displacement measurement:
# This function measures the goodness of correlation in the area zone between image In and I0 deformed by a X[1] X[2] translation:   

def translation_correlation_weight(X,C0,C1,m_sht0):
    """
    Correlation measurement function for a given translation 
    - inputs:
    -- X: translation vector (line, row)
    -- C0: initial picture
    -- C1: deformed picture
    -- m_sht: maxmum absolute values for X
    - output:
    -- s: goodness of correlation between undeformed and shifted picture 
    """  
    ## Extraction of the variables:
    dI=X[0]
    dJ=X[1]
    ##Condition on the maximum shift:
    if (np.max(X)>m_sht):
        ###Make increasing s:
        s=255.+np.max(X)
    else:
        ## Shift the picture:
        C2=nd.interpolation.shift(C1,(dI,dJ),order=1,mode='constant',cval=0.,prefilter=False)
        ## Reduction to the center of the ROI:
        dd=len(C0)
        C0_0=C0[m_sht:dd-m_sht,m_sht:dd-m_sht]
        C2_0=C2[m_sht:dd-m_sht,m_sht:dd-m_sht]
        ## Correlation:
        s=np.sum(abs(C2_0-C0_0))/(dd**2)
    
    return s


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Define function for parallelisation:

## Define shared variable:
share_vec=Array('d',np.zeros(3*len(vec_i0_lg)))
def main(itCell,def_param=share_vec):
    #### Global variables:
    global itImg, mat_di, mat_dj, mat_corr, vec_cc, I0, I1, verbose, vec_i0, vec_j0, sft_rgd
    
    #### Get current cell position:
    if sft_rgd:
        di_cur=0
        dj_cur=0
    else:
        di_cur=mat_di[itCell,itImg]
        dj_cur=mat_dj[itCell,itImg]
    
    ####Initialize vector:
    vec_corr=np.zeros(len(vec_cc))
    vec_p0=np.zeros((len(vec_cc),2))
    p0=np.zeros(2)
    
    #### Loop over the decreasing correlation cell sizes for FFT shift measurement:
    for itSiz in range(len(vec_cc)):
        ##### Construction of the zone pixel coordinates:
        im=int(vec_i0[itCell]-vec_cc[itSiz]/2.); iM=int(vec_i0[itCell]+vec_cc[itSiz]/2.)
        jm=int(vec_j0[itCell]-vec_cc[itSiz]/2.); jM=int(vec_j0[itCell]+vec_cc[itSiz]/2.)
        ###### Make coordinate vectors: 
        vec_i=range(im,iM)
        vec_j=range(jm,jM)
        ###### Make mesh and store it in a vector:
        mat_i,mat_j=np.meshgrid(vec_i,vec_j) 
        ze_X=np.empty([mat_i.size,2])
        ze_X[:,0]=np.reshape(mat_i,[mat_i.size])
        ze_X[:,1]=np.reshape(mat_j,[mat_j.size])
        ###### Zone for current image:
        if sft_rgd:
            ze_0=ze_X
        else:
            ze_0=ze_X+[di_cur,dj_cur]
        
        ###### Zone for next image:
        ze_1=ze_X+[di_cur,dj_cur]-p0[::-1]
        ##### Load picture cells:
        ###### Current picture (as a vector):
        cell00=nd.map_coordinates(I0,ze_0.T,order=1) 
        ###### Next one with the correction of the previously measured displacement (as a vector):
        cell01=nd.map_coordinates(I1,ze_1.T,order=1) 
        ###### Change it into a 2D picture:
        cell0=np.reshape(cell00,(int(m.sqrt(len(cell00))),int(m.sqrt(len(cell00))))).T
        cell1=np.reshape(cell01,(int(m.sqrt(len(cell00))),int(m.sqrt(len(cell00))))).T
        
        #!DEBUG!
        # ~ plt.imshow(cell0)
        # ~ plt.savefig('0.png')
        # ~ plt.close()
        # ~ plt.imshow(cell1)
        # ~ plt.savefig('1.png')
        # ~ plt.close()
        
        
        ##### Define image size: 
        row,col=cell0.shape
        ##### Measure shift between picture from FFT:
        ###### Compute Fourier transform:
        cell0FFT=np.fft.fft2(cell0)
        cell1FFT=np.conjugate(np.fft.fft2(cell1))
        ###### Convolute:
        pictCCor=np.real(np.fft.ifft2((cell0FFT*cell1FFT)))
        ###### Compute the shift: 
        pictCCorShift=np.fft.fftshift(pictCCor)
        iShift,jShift=np.unravel_index(np.argmax(pictCCorShift),(row,col))
        iShift=iShift-int(row/2)+p0[1]
        jShift=jShift-int(col/2)+p0[0]
        ##### Store results:
        p0[0]=jShift
        p0[1]=iShift
        vec_p0[itSiz,0]=p0[0]
        vec_p0[itSiz,1]=p0[1]
        
        ##### Compute the correlation goodness:
        ###### Compute the shifted next picture:
        ze_1=ze_X+[di_cur,di_cur]-p0[::-1]
        cell01=nd.map_coordinates(I1,ze_1.T,order=1)
        cell1=np.reshape(cell01,(int(m.sqrt(len(cell01))),int(m.sqrt(len(cell01))))).T
        ###### Compute the correlation and store it:
        vec_corr[itSiz]=np.sum(np.abs(cell0-cell1))/vec_cc[itSiz]**2.
    
    #### Subpixel improvement of the displacement:
    ##### Look for the best correlation:
    itSiz_0=np.where(vec_corr==np.min(vec_corr))[0][0]
    ##### Extract current and shifted next pictures:
    im=int(vec_i0[itCell]-vec_cc[itSiz_0]/2.); iM=int(vec_i0[itCell]+vec_cc[itSiz_0]/2.)
    jm=int(vec_j0[itCell]-vec_cc[itSiz_0]/2.); jM=int(vec_j0[itCell]+vec_cc[itSiz_0]/2.)
    vec_i=range(im,iM)
    vec_j=range(jm,jM)
    mat_i,mat_j=np.meshgrid(vec_i,vec_j) 
    ze_X=np.empty([mat_i.size,2])
    ze_X[:,0]=np.reshape(mat_i,[mat_i.size])
    ze_X[:,1]=np.reshape(mat_j,[mat_j.size])
    if sft_rgd:
        ze_0=ze_X
    else:
        ze_0=ze_X+[di_cur,dj_cur]
    
    ze_1=ze_X+[di_cur,dj_cur]-vec_p0[itSiz_0,::-1]
    cell00=nd.map_coordinates(I0,ze_0.T,order=1) 
    cell01=nd.map_coordinates(I1,ze_1.T,order=1) 
    cell0=np.reshape(cell00,(int(m.sqrt(len(cell00))),int(m.sqrt(len(cell00))))).T
    cell1=np.reshape(cell01,(int(m.sqrt(len(cell00))),int(m.sqrt(len(cell00))))).T
    ##### Optimization of the measurement:
    ext_res=minimize(translation_correlation_weight,np.array([0.1,0.1]),args=(cell0,cell1,m_sht),method='Nelder-Mead',tol=tol_0,options={'maxiter':max_iter_0,'maxfev':max_fev_0,'disp':verbose},) 
    
    ####Storage of the results:
    share_vec[itCell]=di_cur-ext_res.x[0]-vec_p0[itSiz_0,1]
    share_vec[len(vec_i0)+itCell]=dj_cur-ext_res.x[1]-vec_p0[itSiz_0,0]
    share_vec[2*len(vec_i0)+itCell]=ext_res.fun
    
    return 0


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Main displacement computation for each grain:

## Initialisation:
### Number of images to treat for each grain:
nb_img=len(fnmatch.filter(os.listdir('particlePicture/000'), '*.png'))
### Make Folder to save data:
os.system('mkdir result')

## Computation of the displacement field for each particle: 
for it in range(IJSN.shape[0]):
    
    if ((restrict_correl==0) or ((IJSN[it,3]==1) and (restrict_correl==1)) or ((IJSN[it,3]==0) and (restrict_correl==3)) ):
    
        ### Decide if small or large:
        if int(IJSN[it,2]):
            vec_i0=vec_i0_lg
            vec_j0=vec_j0_lg
        else:
            vec_i0=vec_i0_sm
            vec_j0=vec_j0_sm
        
        ### Decide if soft or rigid:
        if int(IJSN[it,3]):
            sft_rgd=1
            vec_cc=vec_cc_r
        else:
            sft_rgd=0
            vec_cc=vec_cc_s
        
        ### To store displacements (line<->cell ; column<->picture):
        mat_di=np.zeros([len(vec_i0),nb_img])
        mat_dj=np.zeros([len(vec_i0),nb_img])
        ### To store correlation goodness (line<->cell ; column<->picture) [average error {0..255}]:
        mat_corr=np.zeros([len(vec_i0),nb_img])
        ### Load the initial picture:
        I0=np.array(Image.open('particlePicture/'+str(it).zfill(3)+'/'+str(0).zfill(4)+'.png')).astype('float')
        ### Make Folder to save data:
        os.system('mkdir result/'+str(it).zfill(3))
        
        ## Loop over the pictures: 
        for itImg in range(nb_img-1):
            
            ### Display:
            print(str(it).zfill(3)+' - '+str(itImg)+'/'+str(nb_img-2))
            
            ### Load current picture:
            I1=np.array(Image.open('particlePicture/'+str(it).zfill(3)+'/'+str(itImg+1).zfill(4)+'.png')).astype('float')
            
            ### Parallelisation over the correlation cells:
            p=Pool(nb_proc)
            p.map(main,range(len(vec_i0)))
            p.close()
            
            print('done')
            
            ### Storage of data:
            mat_di[:,itImg+1]=share_vec[0:len(vec_i0)]
            mat_dj[:,itImg+1]=share_vec[len(vec_i0):2*len(vec_i0)]
            mat_corr[:,itImg+1]=share_vec[2*len(vec_i0):3*len(vec_i0)]
            
            # !DEBUG!
            # ~ plt.imshow(I1)
            # ~ plt.plot(vec_j0+mat_dj[:,itImg+1],vec_i0+mat_di[:,itImg+1],'or',markersize=2)
            # ~ plt.axis('equal')
            # ~ plt.savefig(str(itImg).zfill(4)+'.png')
            # ~ plt.show()
            # ~ plt.close()
            
            ### Save data:
            np.savetxt('result/'+str(it).zfill(3)+'/index_I.txt',vec_i0)
            np.savetxt('result/'+str(it).zfill(3)+'/index_J.txt',vec_j0)
            np.savetxt('result/'+str(it).zfill(3)+'/displacement_I.txt',mat_di)
            np.savetxt('result/'+str(it).zfill(3)+'/displacement_J.txt',mat_dj)
            np.savetxt('result/'+str(it).zfill(3)+'/correlation.txt',mat_corr)
            
            ### Switch picture order:
            if not sft_rgd:
                I0=I1.copy()
            
            del I1









