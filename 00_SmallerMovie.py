# This code permit to make small movie from the data.
# No imput is needed

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries and global variables:

## Commons ones:
import os
import numpy as np
import fnmatch

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Sort picture names:

## Extract picture names: 
pict_name=fnmatch.filter(os.listdir('picture'), '*.png')

## Extract number of pitures:
nb_pict=len(pict_name)

## Extract picture numbers:
pict_num=np.zeros(nb_pict)
for it in range(nb_pict):
    pict_num[it]=int(pict_name[it][0:4])

## Sort picture names:
pict_name=[pict_name for (pict_name,pict_num) in sorted(zip(pict_name,pict_num))]
del pict_num

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Make qualitative evolution movie:

## Rescale pictures:
os.system('mkdir picture/reduce')
for it in range(nb_pict):
    print(str(it)+'/'+str(nb_pict-1))
    null=os.system('convert picture/'+'%04d'%it+'.png  -scale 5% picture/reduce/'+'%04d'%it+'.png')

## Make movie:
os.system('ffmpeg -r 5 -f image2 -i picture/reduce/%04d.png -qscale 1 picture/reduce/vid.avi')















