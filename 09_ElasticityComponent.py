# This code load the DIC (after outlier removing and grid interpolation) for each grain of multigrain experiments and compute elasticity component on a regular grid:
# F, C, B, C eigen, C invariant for soft particles
# \epsilon, \epsilon eigen for rigid particles


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Load inputs:

## Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

## Numbering:
### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

##Correlation parameter:
### Space between cell centers in line and row [px]:
dd_0=data_input['granular DIC']['inter-cell space']

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Compute the elasticity components:

## Load particle nature vector:
IJSN=np.loadtxt('initialParticlePosition/IJSN_position.txt')

## Loop over particles:
for it_p in range(nb_part):
    
    ### Display:
    print('particle '+'%03d'%it_p)
    
    ### Load data:
    grid_I=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_I.txt')
    grid_J=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/grid_J.txt')
    dIJ=dd_0/2.
    
    ### Prepare storage folders:
    if bool(IJSN[it_p,3]):
        os.system('mkdir result/'+'%03d'%it_p+'/interpolated/strain_E')
    else:
        os.system('mkdir result/'+'%03d'%it_p+'/interpolated/deformation_gradient_F')
        os.system('mkdir result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C')
        os.system('mkdir result/'+'%03d'%it_p+'/interpolated/left_Cauchy_Green_B')
        os.system('mkdir result/'+'%03d'%it_p+'/interpolated/lagrangian_strain_E')
    
    ### Loop over the frames:
    for itImg in range(nb_img):
        
        #### Load fields:
        grid_ui=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%itImg+'_grid_displacement_I.txt')
        grid_uj=np.loadtxt('result/'+'%03d'%it_p+'/interpolated/displacement/'+'%03d'%itImg+'_grid_displacement_J.txt')
        
        if bool(IJSN[it_p,3]):
            #### Compute E components:
            grid_Eii=(grid_ui[1:grid_ui.shape[0],:]-grid_ui[0:grid_ui.shape[0]-1,:])/dIJ
            grid_Eii=np.append(grid_Eii,np.ones((1,grid_Eii.shape[1])),axis=0)
            grid_Eii[grid_Eii.shape[0]-1,:]=np.nan
            grid_Ejj=(grid_uj[:,1:grid_uj.shape[1]]-grid_uj[:,0:grid_uj.shape[1]-1])/dIJ
            grid_Ejj=np.append(grid_Ejj,np.ones((grid_Ejj.shape[0],1)),axis=1)
            grid_Ejj[:,grid_Ejj.shape[1]-1]=np.nan
            grid_Eij_a=(grid_ui[:,1:grid_ui.shape[1]]-grid_ui[:,0:grid_ui.shape[1]-1])/dIJ
            grid_Eij_a=np.append(grid_Eij_a,np.ones((grid_Eij_a.shape[0],1)),axis=1)
            grid_Eij_a[:,grid_Eij_a.shape[1]-1]=np.nan
            grid_Eij_b=(grid_uj[1:grid_uj.shape[0],:]-grid_uj[0:grid_uj.shape[0]-1,:])/dIJ
            grid_Eij_b=np.append(grid_Eij_b,np.ones((1,grid_Eij_b.shape[1])),axis=0)
            grid_Eij_b[grid_Eij_b.shape[0]-1,:]=np.nan
            grid_Eij=0.5*(grid_Eij_a+grid_Eij_b)
            
            #### Compute eigenvectors, eigenvalues and invariant of E:
            ##### Trace:
            grid_T=grid_Ejj+grid_Eii
            ##### Determinant:
            grid_D=grid_Ejj*grid_Eii-grid_Eij**2
            ##### Eigenvalues:
            grid_EgV1=0.5*grid_T+np.sqrt(0.25*(grid_T**2)-grid_D)
            grid_EgV2=0.5*grid_T-np.sqrt(0.25*(grid_T**2)-grid_D)
            ##### Eigenvectors:
            grid_EgVec1_J=grid_EgV1-grid_Eii
            grid_EgVec1_I=grid_Eij
            A=np.sqrt(1/(grid_EgVec1_J**2.+grid_EgVec1_I**2.))
            grid_EgVec1_J=A*grid_EgVec1_J
            grid_EgVec1_I=A*grid_EgVec1_I
            grid_EgVec2_J=grid_EgV2-grid_Eii
            grid_EgVec2_I=grid_Eij  
            A=np.sqrt(1/(grid_EgVec2_J**2.+grid_EgVec2_I**2.))
            grid_EgVec2_J=A*grid_EgVec2_J
            grid_EgVec2_I=A*grid_EgVec2_I
            ##### Invariants:
            grid_I1=grid_T
            grid_I2=grid_D
            
            #### Storage of the grids:
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_Ejj.txt',grid_Ejj)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_Eij.txt',grid_Eij)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_Eii.txt',grid_Eii)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_eigenvalue_1.txt',grid_EgV1)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_eigenvalue_2.txt',grid_EgV2)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_eigenvector_1_J.txt',grid_EgVec1_J)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_eigenvector_1_I.txt',grid_EgVec1_I)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_eigenvector_2_J.txt',grid_EgVec2_J)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_eigenvector_2_I.txt',grid_EgVec2_I)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_invariant_1.txt',grid_I1)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/strain_E/'+'%03d'%itImg+'_invariant_2.txt',grid_I2)
            
        else:
            #### Compute F components:
            grid_Fjj=1+((grid_uj[:,1:grid_uj.shape[1]]-grid_uj[:,0:grid_uj.shape[1]-1])/dIJ)
            grid_Fjj=np.append(grid_Fjj,np.ones((grid_Fjj.shape[0],1)),axis=1)
            grid_Fjj[:,grid_Fjj.shape[1]-1]=np.nan
            grid_Fji=(grid_uj[1:grid_uj.shape[0],:]-grid_uj[0:grid_uj.shape[0]-1,:])/dIJ
            grid_Fji=np.append(grid_Fji,np.ones((1,grid_Fji.shape[1])),axis=0)
            grid_Fji[grid_Fji.shape[0]-1,:]=np.nan
            grid_Fii=1+((grid_ui[1:grid_ui.shape[0],:]-grid_ui[0:grid_ui.shape[0]-1,:])/dIJ)
            grid_Fii=np.append(grid_Fii,np.ones((1,grid_Fii.shape[1])),axis=0)
            grid_Fii[grid_Fii.shape[0]-1,:]=np.nan
            grid_Fij=(grid_ui[:,1:grid_ui.shape[1]]-grid_ui[:,0:grid_ui.shape[1]-1])/dIJ
            grid_Fij=np.append(grid_Fij,np.ones((grid_Fij.shape[0],1)),axis=1)
            grid_Fij[:,grid_Fij.shape[1]-1]=np.nan
            
            #### Compute C components:
            grid_Cjj=grid_Fjj**2+grid_Fij**2
            grid_Cji=grid_Fjj*grid_Fji+grid_Fij*grid_Fii
            grid_Cii=grid_Fji**2+grid_Fii**2
            
            #### Compute B components:
            grid_Bjj=grid_Fjj**2+grid_Fji**2
            grid_Bji=grid_Fjj*grid_Fij+grid_Fji*grid_Fii
            grid_Bii=grid_Fij**2+grid_Fii**2
            
            #### Compute E components:
            grid_Ejj=0.5*(grid_Cjj-1)
            grid_Eji=0.5*grid_Cji
            grid_Eii=0.5*(grid_Cii-1)
            
            #### Compute eigenvectors, eigenvalues and invariant of C:
            ##### Trace:
            grid_T=grid_Cjj+grid_Cii
            ##### Determinant:
            grid_D=grid_Cjj*grid_Cii-grid_Cji**2
            ##### Eigenvalues:
            grid_EgV1=0.5*grid_T+np.sqrt(0.25*(grid_T**2)-grid_D)
            grid_EgV2=0.5*grid_T-np.sqrt(0.25*(grid_T**2)-grid_D)
            ##### Eigenvectors:
            grid_EgVec1_J=grid_EgV1-grid_Cii
            grid_EgVec1_I=grid_Cji
            A=np.sqrt(1/(grid_EgVec1_J**2.+grid_EgVec1_I**2.))
            grid_EgVec1_J=A*grid_EgVec1_J
            grid_EgVec1_I=A*grid_EgVec1_I
            grid_EgVec2_J=grid_EgV2-grid_Cii
            grid_EgVec2_I=grid_Cji  
            A=np.sqrt(1/(grid_EgVec2_J**2.+grid_EgVec2_I**2.))
            grid_EgVec2_J=A*grid_EgVec2_J
            grid_EgVec2_I=A*grid_EgVec2_I
            ##### Invariants:
            grid_I1=grid_T
            grid_I2=grid_Cjj*grid_Cii-grid_Cji*grid_Cji
            
            #### Storage of the grids:
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/deformation_gradient_F/'+'%03d'%itImg+'_Fjj.txt',grid_Fjj)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/deformation_gradient_F/'+'%03d'%itImg+'_Fji.txt',grid_Fji)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/deformation_gradient_F/'+'%03d'%itImg+'_Fii.txt',grid_Fii)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/deformation_gradient_F/'+'%03d'%itImg+'_Fij.txt',grid_Fij)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_Cjj.txt',grid_Cjj)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_Cji.txt',grid_Cji)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_Cii.txt',grid_Cii)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_eigenvalue_1.txt',grid_EgV1)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_eigenvalue_2.txt',grid_EgV2)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_eigenvector_1_J.txt',grid_EgVec1_J)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_eigenvector_1_I.txt',grid_EgVec1_I)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_eigenvector_2_J.txt',grid_EgVec2_J)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_eigenvector_2_I.txt',grid_EgVec2_I)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_invariant_1.txt',grid_I1)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/right_Cauchy_Green_C/'+'%03d'%itImg+'_invariant_2.txt',grid_I2)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/left_Cauchy_Green_B/'+'%03d'%itImg+'_Bjj.txt',grid_Bjj)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/left_Cauchy_Green_B/'+'%03d'%itImg+'_Bji.txt',grid_Bji)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/left_Cauchy_Green_B/'+'%03d'%itImg+'_Bii.txt',grid_Bii)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/lagrangian_strain_E/'+'%03d'%itImg+'_Ejj.txt',grid_Ejj)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/lagrangian_strain_E/'+'%03d'%itImg+'_Eji.txt',grid_Eji)
            np.savetxt('result/'+'%03d'%it_p+'/interpolated/lagrangian_strain_E/'+'%03d'%itImg+'_Eii.txt',grid_Eii)





