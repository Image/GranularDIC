# This code permit to analyse voids


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

##Commons ones:
import os
import numpy as np
import fnmatch
import math as m
import yaml

##Display one:
from matplotlib import pyplot as plt

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Inputs:

## Global inputs:
### Load data:
with open('input_parameters.yaml', 'r') as stream:
    data_input=yaml.safe_load(stream)

### Numbering:
#### Number of step to treat for each grain:
nb_img=data_input['general']['number of picture']
#### Number of particles:
nb_part=np.loadtxt('initialParticlePosition/IJSN_position.txt').shape[0]

### Particule diameter [px]:
#### Large:
diam_lg=data_input['general']['particle diameter']['large']

## Local inputs:
### Colormap for plotting:
cm=plt.cm.get_cmap('jet')
### Binarisation number for PDF:
n_bin=10
### Packing fraction binarisation:
t_bin=10

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Function to compute linear pdfs:

def hist_pdf_lin(Q,X):
    P=np.zeros(len(X)-1)
    rP=np.zeros(len(X)-1)
    Q_len=float(len(Q))
    for it in range(len(X)-1):
        I=np.where((X[it]<=Q)*(Q<X[it+1]))[0]
        P[it]=float(len(I))/Q_len/(X[it+1]-X[it])
        rP[it]=np.std(Q[I])/m.sqrt(len(I))/(X[it+1]-X[it])/(X[it+1]-X[it])
    
    X=0.5*(X[0:len(X)-1]+X[1:len(X)])
    return [X,P,rP]


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Void area PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

## Loop over packing fractions:
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<=vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        if os.path.isfile('result/void/void_stp_'+str(it_i).zfill(3)+'.txt'): 
            Q=np.loadtxt('result/void/void_stp_'+str(it_i).zfill(3)+'.txt')
            if Q.ndim==1:
                lst_Q.append(Q[1])
            else:
                lst_Q.append(Q[:,1])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    ### Scale with large particle area:
    vec_Q/=(m.pi*(0.5*diam_lg)**2.)
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.xlim(0.,1.2)
# ~ plt.ylim(0.,10.)
plt.xlabel('void area / large particle area')
plt.ylabel('PDF')
plt.savefig('figure/PDFvoidArea.png')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Void asphericity PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

## Loop over packing fractions:
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<=vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        if os.path.isfile('result/void/void_stp_'+str(it_i).zfill(3)+'.txt'): 
            Q=np.loadtxt('result/void/void_stp_'+str(it_i).zfill(3)+'.txt')
            if Q.ndim==1:
                lst_Q.append(Q[3])
            else:
                lst_Q.append(Q[:,3])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.xlim(2.,16.5)
# ~ plt.ylim(0.,0.6)
plt.xlabel('void asphericity')
plt.ylabel('PDF')
plt.savefig('figure/voidAsphericity.png')
plt.show()
plt.close()


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Void solidity PDF:

## Load packing fraction:
phi0=np.loadtxt('result/global/packing_fraction_rough.txt')

## Make packing fraction discretisation vector: 
vec_pf=np.linspace(np.min(phi0),np.max(phi0),t_bin)

## Loop over packing fractions:
for it_phi in range(len(vec_pf)-1):
    ### Initialize data list:
    lst_Q=[]
    ### Current steps:
    I_cur=np.where((vec_pf[it_phi]<=phi0)*(phi0<=vec_pf[it_phi+1]))[0]
    ### Loop over current steps to collect data:
    for it_i in I_cur:
        if os.path.isfile('result/void/void_stp_'+'%03d'%(it_i)+'.txt'): 
            Q=np.loadtxt('result/void/void_stp_'+str(it_i).zfill(3)+'.txt')
            if Q.ndim==1:
                lst_Q.append(Q[4])
            else:
                lst_Q.append(Q[:,4])
    
    ### Convert into array:
    vec_Q=np.hstack(lst_Q)
    ### Compute the PDF:
    X,P,rP=hist_pdf_lin(vec_Q,np.linspace(np.min(vec_Q),np.max(vec_Q),n_bin))
    ### Clean PDF:
    I=np.where(~np.isnan(P))[0]; X=X[I]; P=P[I]; rP=rP[I]
    ### Plot PDF:
    flt_col=(0.5*(vec_pf[it_phi]+vec_pf[it_phi+1])-np.min(vec_pf))/(np.max(vec_pf)-np.min(vec_pf))
    plt.plot(X,P,'o-',color=cm(flt_col),linewidth=1,markersize=3)

sc=plt.scatter(np.zeros(len(vec_pf)),np.zeros(len(vec_pf)),c=vec_pf,s=0,cmap=cm)
plt.colorbar(sc)
plt.xlim(0.,1.)
plt.ylim(0.,6.)
plt.xlabel('void solidity')
plt.ylabel('PDF')
plt.savefig('figure/voidSolidity.png')
plt.show()
plt.close()






